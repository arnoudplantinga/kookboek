
## Ingrediënten
- 1 stengel citroengras
- 250 g champignons
- 2 cm verse gemberwortel
- 1 rode peper
- 300 g kipfilet
- 2 el olie
- 400 ml kokosmelk (blik)
- 1 liter kippenbouillon (van tablet)
- 15 g verse basilicum (zakje)

## Bereiding
1. Snijd het citroengras in stukken. Maak de champingnons schoon met keukenpapier en snijd in grove stukken. Snijd de gemberwortel in plakken. Snijd het steeltje van de rode peper. Halveer de peper in de lengte en verwijder met een scherp mesje de zaadlijsten. Snijd de peper in ringetjes. Snijd de kipfilet in blokjes.
2. Verhit de olie in een soeppan. Bak hierin de kipfilet totdat deze lichtbruin ziet. Bak al omscheppend op hoog vuur het citroengras, de peper, gember en champignons mee, totdat ze een sterke geur afgeven. Schenk de kokosmelk en de kippenbouillon erbij, breng het geheel aan de kook en laat 15 min. zachtjes doorkoken.
3. Breng de soep op smaak met peper en zout. Snijd het basilicum fijn en bestrooi de soep voor het serveren met het basilicum.
