## Ingrediënten

Voor 3 tot 4 personen:

- 500 gram kipfilet, gesneden
- 200 gram glasnoedels
- 4 lente-uien, in ringetjes
- 100 gram taugé
- 2 rode chilipepers, gehakt
- 3 handen munt, (Thaise) basilicum, koriander, grof gehakt
- Taugé
- Broccoli

### Voor de bouillon:

- 1,5 liter bouillon
- ½ liter water
- 3 cm verse gember, in plakken
- 3 teentjes look, gepeld
- 1 ui, in vieren
- 3 steranijs
- 1 kaneelstokje
- 50 ml vissaus
- Peper

## Bereiding

Doe alle ingrediënten voor de bouillon in een grote soeppan en breng aan de kook. Laat 20 minuten zachtjes pruttelen met het deksel op de pan. Zeef of schep alle smaakmakers uit de bouillon. Kruid met peper naar smaak.

Kook de noedels zoals aangegeven op de verpakking, giet af en spoel direct af met koud water, zodat ze niet doorgaren.

Verdeel de noedels over 3 of 4 kommen met de biefstuk, sojascheuten en lente-ui. Schep er een grote pollepel hete bouillon over.

Serveer de chilipeper en kruiden er apart bij en roer naar smaak door de soep.
