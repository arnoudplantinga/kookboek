## Ingrediënten 4 personen

- 2 rode uien
- 2 teentjes knoflook
- 400 gr flespompoen
- 4 el zonnebloemolie
- 2 el Verstegen Mix voor Kip Krokant - Cajun
- 800 gr tomatenblokjes
- 800 gr zwarte bonen uit blik/pot
- 500 ml groentebouillon
- 1 afbakstokbrood
- 4 takjes peterselie
- 4 el Griekse yoghurt 


Bereidingstijd 15 min

Wachttijd 30 min

Totale tijd 45 min

## Bereiding

1. Pel en snipper de rode ui. Pel de knoflook en hak grof. Snijd de pompoen in blokjes van 1 cm.
2. Verhit de zonnebloemolie in een grote soeppan op middelhoog vuur. Fruit de rode ui, knoflook en Verstegen Mix voor Kip Krokant - Cajun 2 min. aan. Voeg de tomatenblokjes, de zwarte bonen (houd een handje zwarte bonen achter), pompoenblokjes en de groentebouillon toe, breng aan de kook en laat 15 min. doorkoken.
3. Pureer de soep met een staafmixer. Voeg eventueel extra water toe wanneer de soep te dik is. Laat nog 15 min. rustig doorkoken op laag vuur.
4. Bak intussen het stokbrood volgens aanwijzingen op de verpakking. Pluk de peterselie en hak grof.
5. Schenk de soep in kommen en garneer deze met een toefje Griekse yoghurt. Maak af met wat zwarte bonen en peterselie. Serveer met stokbrood. 
