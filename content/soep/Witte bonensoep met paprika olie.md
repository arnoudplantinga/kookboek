## Ingredients 4 to 6 servings (8 cups)

For the Soup

- ¼cup extra-virgin olive oil
- 3large shallots, minced (about ⅔ cup)
- 4large celery stalks, peeled and thinly sliced
- 5garlic cloves, minced
- 3teaspoons dried Italian seasoning or herbes de Provence
- 1½teaspoon fennel seeds (optional)
- ¾teaspoon finely chopped fresh rosemary (optional)
- Kosher salt and black pepper
- 6cups chicken or vegetable stock
- 3(14-ounce) cans cannellini beans (or chickpeas), rinsed
- 2cups half-and-half or almond milk

For the Paprika Oil

- ⅓cup extra-virgin olive oil
- 1½teaspoons smoked (hot or sweet) paprika
- ½teaspoon red-pepper flakes

## Preparation

**Step 1**

Prepare the soup: In a large pot, heat ¼ cup oil over medium. Add the shallots, celery, garlic, Italian seasoning, fennel seeds (if using) and rosemary (if using); season with salt and pepper. Cook, stirring occasionally, until tender, about 10 minutes, reducing the heat to medium-low if needed to avoid browning the vegetables.

**Step 2**

Add the stock and rinsed beans, and bring to a boil over high. Once the mixture comes to a boil, cook over medium-high until flavors meld and stock thickens, about 15 minutes.

**Step 3**

While the soup cooks, prepare the paprika oil: Heat ⅓ cup oil in a small skillet over the lowest heat on your smallest burner. Add the paprika and red-pepper flakes and cook, stirring frequently, just until toasted and flavors bloom, 1 to 2 minutes. Strain, discarding flakes, then set paprika oil aside.

**Step 4**

Working in batches if needed, transfer the soup to a blender and purée until smooth, adding half-and-half or almond milk to thin to desired consistency. Season to taste with salt and pepper.

**Step 5**

Divide among bowls and drizzle with strained paprika oil to serve.
