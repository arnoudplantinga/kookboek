## Ingrediënten Voor 4 personen:

- 700 gram pompoen, geschild, ontpit, in stukken
- 1 kleine winterwortel, in stukjes
- 2 à 3 eetlepels plantaardige olie
- Stuk gember van 4 cm, geschild en fijngesneden
- Teentje knoflook, fijngesneden
- 1 theelepel cayennepeper (of meer, naar smaak)
- 1 eetlepel kerriepoeder
- 1 theelepel gemalen komijn
- 1/2 bosje koriander, fijngehakt
- 1 liter bouillon (iets minder als je de soep dik wilt)
- 1 blikje ongezoete kokosmelk

## Bereiding

Verhit de olie in een grote pan en fruit hierin op een laag vuur de stukken pompoen, wortel, gember en knoflook gedurende 10 minuten. Voeg dan het kerriepoeder, gemalen komijn en cayennepeper toe, schep alles goed om en laat het nog vijf minuten zacht fruiten.

Voeg dan de koriander toe, met de bouillon. Breng aan de kook en laat de soep ongeveer vijftien minuten koken tot de groenten zacht zijn.

Pureer de soep met een staafmixer of in de keukenmachine tot een egaal geheel. Voeg de kokosmelk toe en meng dit goed door de soep. Maal er nog wat zwarte peper door en voeg – indien nodig – wat zout toe.

Serveer de soep in voorverwarmde kommen. Garneer de Oosterse pompoensoep met een drizzle extra vergine olijfolie, de dukkah en amandelen.
