#### Ingrediënten

-   2 courgettes, in blokjes
-   1 liter kippenbouillon (van tablet)
-   125 ml slagroom
-   100 g gerookte zalm, in snippers
-   2 el olie
-   1 ui, gesnipperd[]( "zet ingrediënt op mijn lijst")

###### Keukenspullen

-   staafmixer of keukenmachine

#### Bereiden

1. Verhit de olie in een grote (soep)pan en fruit de ui in de olie tot deze glazig ziet. Roer er de courgetteblokjes door en bak die nog 3 minuten mee op hoog vuur. Blijf de blokjes omroeren.

2. Schenk de kippenbouillon erbij, breng het geheel aan de kook en laat de courgette 10 minuten zachtjes doorkoken. Pureer de soep met de staafmixer of in de keukenmachine. Roer de slagroom erdoor en breng de soep op smaak met zout en peper.

3. Schep de soep in vier diepe borden en strooi in ieder bord een kwart van de zalmsnippers.

 
