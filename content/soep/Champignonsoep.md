Champignonsoep met creme fraiche
================================


- 500 gram champignons
- 40 gram boter                                                        
- 1 ui
- 1 eetlepel bloem
- 1 liter vlees bouillon
- Zout, en peper
- 4 eetlepels creme fraiche
- Peterselie


-   Maak de ui schoon en snijd hem in halve ringen.   

    ![](http://plzcdn.com/resize/500-500/upload/d1ac549dbfdec4a0d49baec903648bb4Y2hhbXBpZ25vbnNvZXAgZ2VzbmVkZW4gdWkuanBn.jpg)

     

-   Maak de champignons schoon en snijd ze in plakjes. 

    ![](http://plzcdn.com/resize/500-500/upload/32b683d9d8e73d3eeb6bf08fe0817402Y2hhbXBpZ25vbi5qcGc=.jpg)


1. Verhit de boter in de soeppan en fruit de ui tot dat hij licht bruin
is. 
2. Voeg dan de champignons bij de ui en fruit alles ongeveer 5 minuten en roer af en toe in de pan.
3. Roer dan de bloem door de champignons, voeg de bouillon toe, breng de soep op smaak met peper en zout en breng de soep aan de koek.
4. Laat de soep 10 minuten koken voeg dan de creme fraiche en naar smaak peterselie toe.                

-   Haal de soep van het gas en serveer direct.  
-   Eet smakelijk

 ![](http://plzcdn.com/resize/500-500/upload/c9f029a6a1b20a8408f372351b321dd8Y2hhbXBpZ25vbnNvZXAgbWV0ICBjcmVtZSBmcmFpY2hlLmpwZWc=.jpeg)
 
