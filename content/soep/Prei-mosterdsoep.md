
## Ingrediënten

- 500 g preien
- 2 el olie
- 2 aardappelen
- 1 liter kippenbouillon (van tablet)
- 2 el Zaanse mosterd
- 125 ml crème fraîche (halfvet)

## Bereiding
1. Was de prei en snijd in ringen. Verhit de olie in een soeppan. Leg enkele lichtgroene, dunne ringen prei apart voor de garnering en bak de rest op middelhoog vuur 3 min. in de olie.
2. Snijd de aardappelen in blokjes. Voeg de aardappelen en bouillon toe en breng het geheel aan de kook. Laat de soep 10-15 min. zachtjes doorkoken.
2. Schep de mosterd en de crème fraîche in de soep en pureer het geheel met de staafmixer. Breng op smaak met peper en zout.
3. Verwarm de soep nog even kort en schep hem in kommen of diepe borden. Strooi er de achtergehouden ringen prei over.
