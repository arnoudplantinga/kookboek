## Ingrediënten voor 4 personen

- 500 gram zalmfilet (zonder huid)
- 2 eetlepels miso
- 1 limoen (zeste en sap)
- 1 eetlepel sojasaus
- 2 teentjes knoflook, geperst
- 1 eetlepel verse koriander
- 50 gram panko

Voor de gembermayonaise
- 2 grote eetlepels mayonaise
- 1 theelepel gemberpasta (gehakte gember)

En verder:
- 4 hamburgerbroodjes
- 8 blaadjes sla
- 125 gram zeewiersalade (wakame)
- 1 kleine rode ui, in dunne ringen

## Bereiding

Hak de zalm in grove stukken. Doe de helft in een blender en mix tot een pasta. Voeg er nadien de miso, limoen, sojasaus, knoflook en de rest van de zalm aan toe en mix tot je een massa hebt met hier en daar nog wat stukjes. Doe de inhoud van de blender in een kom en roer er de panko door. Voelt het mengsel nog wat te nat aan om er burgers van te maken? Roer er dan wat extra panko door. Maak van dit mengsel 4 hamburgers en leg ze even apart.

Meng de gemberpasta met de mayonaise in een kommetje.

Verhit een klontje boter in een pan. Snijd de hamburgerbroodjes doormidden en toast de binnenkanten in de pan tot ze lichtjes bruin kleuren. Let op: dit kan snel gaan! Leg ze apart.

Voeg eventueel wat extra boter toe aan de pan en bak de zalmburgers op een laag vuurtje tot ze gaar zijn.

Besmeer beide kanten van de broodjes met de gembermayonaise en leg op de onderste helft een blaadje sla. Leg daarop een zalmburger en vervolgens wat van de zeewiersalade en een paar ringen rode ui. Leg er de bovenste helft van het broodje op en wacht vooral niet te lang met je tanden erin te zetten…
