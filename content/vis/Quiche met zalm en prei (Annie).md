
Voor 4 personen

- 1 kruimeldeegrol
- 3 preistelen
- 2 el olijfolie
- 200 gr. gerookte zalm
- handvol verse bieslook
- 3 eieren
- 2 dl rool
- peper en zout
- Geraspte kaas


1. Ontrol het deel en leg het samen met bakpapier in een taartvorm. Snij het overtollige bakpapier weg. Doorprik de bodem met een vorm.
2. Snij de preistelen in fijne ringen en stoof ze 2 minuten in olijfolie. Laat afkoelen.
2. Verdeel de prei in de vorm samen met reepjes gerookte zalm en verse bieslooksnippers.
2. Klop de eieren los samen met de room, kruid met peper en zout en giet over de prei en zalm.
2. Bestrooi met gemalen kaas.
2. Bak de quiche 30 minuten op 200 graden. Garneer met verse bieslook.
