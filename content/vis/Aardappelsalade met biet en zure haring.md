
## Ingrediënten (4 personen)

- 1 kg vastkokende aardappel
- 355 g zure haringen
- 500 g gekookte bieten
- 2 friszure appels
- 15 g verse dille (bakje 15 g)
- 125 g crème fraîche
- 2 el mierikswortel
- 3 bosuitjes

## Bereiding

1. Schil de aardappelen en snijd ze in blokjes van 1 x 1 cm (brunoise). Kook in water met zout in 15 min. gaar. Giet af en laat afkoelen.
2. Laat ondertussen de haring uitlekken in een vergiet en vang het vocht op. Snijd in lange repen. Schil de bietjes en snijd in blokjes van 1 x 1 cm.Snijd ook de ongeschilde appel in blokjes van 1 x 1 cm. Ris de dille van de takjes en snijd grof.
3. Meng voor de dressing de crème fraîche, mierikswortel, de helft van de dille en 3 el van het haringvocht. Breng op smaak met (versgemalen) peper en eventueel zout.
4. Snijd de bosui in dunne ringetjes. Meng de aardappel, biet, appel en de helft van de bosui met de dressing en schep op een schaal. Verdeel de haring erover en bestrooi met de rest van de bosui en dille.

**Bewaartip:**
Je kunt de salade 1 dag van tevoren maken. Meng de groenten, de appel en dressing vlak voor het serveren door elkaar.
