## Ingrediënten

- 200 g zilvervliesrijst
- 400 ml kokosmelk
- paksoi, wit en groen apart in reepjes
- 150 g diepvriesdoperwten, ontdooid
- 250g kabeljouwfilet, in blokjes
- geraspte schil en sap van 1/2 limoen & peper en zout

### Voor de groene currypasta

- 1 stengel citroengras, fijngehakt
- 2 lente-uiten, in ringetjes
- 1/2 groene peper
- 2 tenen knoflook
- 1/2 bose koriander + extra blaadjes
- 1/2 tl gemalen koriander
- 4 limoenblaadjes
- 1 el sojasaus
- 1 tl vissaus

## Bereiding

Doe alle ingrediënten voor de groene currypasta in een keukenmachine en maal ze tot een gladde pasta. Kook de rijst.

Verhit de zonnebloemolie en fruit de currypasta 2 minuten. Giet de kokosmelk erbij en laat het 5 minuten pruttelen.

Voeg het wit van de paksoi toe en warm het 1 minuut mee. Doe het groen van de paksoi met de doperwten erbij en schep om. Leg de kabeljouw in de saus en roer niet meer. Laat de vis in 3 minuten gaar worden.

Breng de curry op smaak met de limoenrasp en peper en zout.
