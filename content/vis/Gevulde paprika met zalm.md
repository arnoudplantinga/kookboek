**Gevulde paprika met zalm**

- 160 gram gerookte zalm, in kleine stukjes gesneden
- 2 gele paprika’s
- 150 gram roomkaas
- 1/4 komkommer, geschild en in blokjes gesneden
- 4 eetlepels zelfgemaakte mayonaise (zie boven)
- 1 eetlepel dille
- zout en peper


**Bereiding**

1. Meng de mayonaise met de roomkaas in een aparte kom.
2. Voeg de komkommer, dille en de zalm toe, samen met wat peper en zout.
3. Snij de paprika’s in twee en verwijder de zaadlijst. Vul de paprika’s met de zalm.
4. Garneer met dilletakjes en schijfjes citroen.
5. Dien op samen met salade.
