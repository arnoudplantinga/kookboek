
## Ingrediënten

- 300 g fusilli
- 46 g Princes ansjovis in olijfolie in blik
- 2 courgettes
- 2 uien
- 30 g kappertjes
- 370 g Princes tonijnstukken in zonnebloemolie
- 400 g AH BASIC pastasaus basilicum
- 4 el zwarteolijvenplakjes (potje 240 g)

## Bereiding

1. Kook de pasta volgens de aanwijzingen op de verpakking beetgaar. Snijd ondertussen de ansjovisfilets fijn en vang de olie op in een hapjespan.
2. Snijd de courgettes in de lengte in vieren. Verwijder met een scherp mes de zaadlijsten en snijd het vruchtvlees in plakken van 1 cm dik.
3. Snijd de ui in halve ringen.
4. Verhit de opgevangen olie. Voeg de ansjovis en kappertjes toe en bak 2 min. mee op middelhoog vuur. Voeg de courgette en ui toe en bak 3 min.
5. Laat de tonijn uitlekken. Voeg de pastasaus en de helft van de tonijn toe aan het courgettemengsel. Schep om en laat 2 min. koken.
6. Schep de pasta door de saus en verdeel de rest van de tonijn en de olijven erover.
