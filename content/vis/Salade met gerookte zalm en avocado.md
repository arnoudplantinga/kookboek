Een heerlijke salade. Je hebt ‘m in vijf minuten op tafel staan en de
smaak is super. Hij is licht, maar door de avocado en zalm toch vullend,
en past ook prima als bijgerecht bij een gezonde BBQ. Maak er
bijvoorbeeld nog [zalmpakketjes met
dille](http://www.focusonfoodies.nl/zalm-van-de-barbecue-met-citroen-en-dille/ "Zalm van de barbecue met citroen en dille") bij,
of ga voor [kipspiesjes met
pindasaus](http://www.focusonfoodies.nl/bbq-kip-spies-met-homemade-pindasaus/ "BBQ kip spies met homemade pindasaus").

 

**Wat heb je nodig?**

-   200 gram gerookte zalm
-   150 gram veldsla
-   halve komkommer
-   1 avocado
-   10 zongedroogde tomaatjes
-   2-3 eetlepels olijfolie
-   sap van een halve citroen
-   1 theelepel dille
-   zout en peper

 

**En hoe maak je het?**

1.  Snijd of scheur de gerookte zalm in stukjes. Snijd de avocado in
    blokjes, de zongedroogde tomaat in stukjes en de komkommer in
    halve plakjes.
2.  Doe de veldsla in een grote kom, decoreer met de zalm, avocado,
    tomaatjes en komkommer.
3.  Maak een dressing van de olijfolie, citroensap, dille en zout en
    peper naar smaak.

 

Bron:  
