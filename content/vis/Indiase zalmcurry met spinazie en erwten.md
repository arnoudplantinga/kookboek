#### Ingrediënten

4
personen

-   150 g zilvervliesrijst
-   2 uien
-   4 cm verse gember
-   2 tomaten
-   1 el 
-   2 el kruidenpasta mild curry (potje 160 g, Patak's)
-   200 ml kokosmelk (blikje
    270 ml)
-   400 g kikkererwten OF GEWONE ERWTEN
-   300 g zalmfilet
-   400 g wilde spinazie
-   1 limoen

 

**Bereiden**

1.  Breng een ruime pan water aan de kook, voeg de rijst toe en kook in
    15 min. gaar. Giet af en laat met de deksel op de pan staan
    tot gebruik. Snijd ondertussen de uien in halve ringen. Schil de
    gember en snijd fijn. Snijd de tomaten in blokjes. Verhit de olie in
    een wok en fruit de ui, fijngesneden gember en currypasta 3 min. op
    laag vuur.
2.  Voeg de kokosmelk, tomatenblokjes en kikkererwten toe en breng al
    roerend aan de kook. Laat 5 min. op middelhoog vuur koken. Breng op
    smaak met peper en eventueel zout.
3.  Snijd ondertussen de zalm in blokjes van 2 x 2 cm. Snijd de
    spinazie grof. Voeg toe en schep voorzichtig om tot de spinazie
    geslonken is. Breng eventueel op smaak met zout. Serveer met
    partjes limoen.

 

 
