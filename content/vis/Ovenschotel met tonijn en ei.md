
## Ingrediënten

- 320 g tonijnmoot in olijfolie
- 1 rode peper
- 100 g cheddar (kaas)
- 600 g verse spinazie
- 800 g aardappelpuree met boter
- 4 middelgrote eieren

## Bereiden

1. Verwarm de oven voor op 200 ºC. Laat de tonijn uitlekken en vang de olie op. Snijd het steeltje van de peper en halveer in de lengte. Verwijder de zaadlijsten en snijd het vruchtvlees fijn. Rasp de kaas.
2. Snijd de spinazie grof. Verhit 2 el van de opgevangen olie van de tonijn in een wok. Fruit de helft van de peper 1 min. Voeg de spinazie in delen toe en laat slinken. Doe over in een zeef en druk het vocht eruit. Meng de spinazie met de puree en verdeel over de ovenschaaltjes. Verdeel de tonijn erover. Breek er voorzichtig een ei boven en bestrooi met de kaas.
3. Bak 15 min. in het midden van de oven tot de eiwitten gestold zijn. Bestrooi met de rest van de rode peper en serveer.
