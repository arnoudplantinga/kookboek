#### Ingrediënten

-   [1/2 pak couscous (a 500 g)]( "zet ingrediënt op mijn lijst")
-   [3 el olijfolie]( "zet ingrediënt op mijn lijst")
-   [2 uien (in ringen)]( "zet ingrediënt op mijn lijst")
-   [2 tenen knoflook (in plakjes)]( "zet ingrediënt op mijn lijst")
-   [1 courgette (in blokjes)]( "zet ingrediënt op mijn lijst")
-   [1 bakje cherrytomaten ((250
    g), gehalveerd)]( "zet ingrediënt op mijn lijst")
-   [1 pak pangasiusfilet ((400 g,
    diepvries), ontdooid)]( "zet ingrediënt op mijn lijst")
-   [1 bakje platte peterselie ((20 g),
    grof gesneden)]( "zet ingrediënt op mijn lijst")

 

#### Bereiden

1.  Bereid de couscous
    volgens de aanwijzingen op de verpakking met 1 el olie.
2.  Verhit de rest van de olie in een hapjespan en fruit de ui en
    knoflook 3 min. Voeg de courgette toe en bak 2 min. mee. Schep de
    tomaten erdoor en bak nog 4 min. Schenk 50 ml water in de pan. Snijd
    de vis in stukken en leg tussen de groenten. Bestrooi met peper en
    zout en stoof met de deksel op de pan in 10 min. op laag vuur gaar.
3.  Voeg van de peterselie en peper en zout toe aan de couscous. Verdeel
    de visstoof over 4 diepe borden of kommen. Strooi er de rest van de
    peterselie over en serveer met de couscous.

#### Tip:

Bestrooi de pangasiusfilet met 1 tl gemalen komijnzaad voor een extra
kruidige smaak.

 
