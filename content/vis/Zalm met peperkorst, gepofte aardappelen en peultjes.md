\
4 personen

565
kcal [voedingswaarden](http://www.ah.nl/allerhande/recept/R-R775770/zalm-met-peperkorst#)

-   25 min. bereiden 

#### Ingrediënten

-   [4 grote iets
    kruimige aardappelen (schoongeboend)]( "zet ingrediënt op mijn lijst")
-   [2 pakken zalm (Atlantisch (a 250
    g), ontdooid)]( "zet ingrediënt op mijn lijst")
-   [2 el zwarte peperkorrels]( "zet ingrediënt op mijn lijst")
-   [25 g boter]( "zet ingrediënt op mijn lijst")
-   [3 schaaltjes peultjes (a 200 g)]( "zet ingrediënt op mijn lijst")
-   [1 bekertje zure room (125 ml)]( "zet ingrediënt op mijn lijst")

 

###### **Keukenspullen**

-   magnetronfolie, vijzel

#### Bereiden







 







1.  Prik met een vork gaatjes in de aardappelen en verpak ze
    afzonderlijk in magnetronfolie. Pof de aardappelen 10 min. in de
    magnetron op 800 watt. Laat in de magnetron nog 2 min. in de
    folie nagaren.
2.  Bestrooi ondertussen de zalm met zout naar smaak. Plet de
    peperkorrels grof in de vijzel of met de bolle kant van een lepel.
    Strooi op een bord en duw 1 kant van de zalmfilets erin. Verhit de
    boter en bak de zalm 6 min., eerst op het peperkorstje.
    Keer halverwege.
3.  Kook ondertussen de peultjes in 4 min. beetgaar in water met zout.
    Haal de aardappelen uit de folie, halveer en verdeel over 4 borden.
    Schep de zure room op de aardappelen en leg de zalm en
    peultjes erbij.

 
