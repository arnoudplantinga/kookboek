
## Ingrediënten
- 1 rode paprika
- 2 uien
- 2 venkelknollen
- 300 g schelpjespasta
- 1 el traditionele olijfolie
- 250 g zalmreepjes
- 125 ml kookroom

## Bereiden

1. Snijd de paprika in reepjes, snipper de uien en snijd de venkelknollen in dunne plakken. Kook de pastaschelpjes volgens de aanwijzingen op de verpakking.
2. Bak in een koekenpan de ui, voeg de paprika en de venkel toe en bak deze goudbruin. Voeg de zalmreepjes toe en bak deze al omscheppend gaar volgens de aanwijzingen op de verpakking. Voeg de kookroom toe en breng aan de kook. Breng op smaak met peper.
3. Giet de pasta af en verdeel deze over de borden. Schep de venkel met zalm in roomsaus erop.
