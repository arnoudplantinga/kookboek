
## Ingrediënten (4 personen)

- 1 rode paprika
- 1 gele paprika
- 3 tomaten
- 1 ui
- 2 teentjes knoflook
- 3 eetlepels olie
- 1 eetlepe tomatenpuree
- peper
- bosje peterselie
- 500 gram kabeljauw

## Bereiden

1. Snij de paprika's in repen en snij de ui en knoflook fijn. Snij de tomaten in vieren. Snij de kabeljauw in 4 stukken.
2. Verhit de olie in een braadpan en fruit de ui.
3. Voeg de knoflook, tomaten, en de paprika toe en bak die ongeveer 5 minuten.
4. Voeg de tomatenpuree toe en laat het geel 5 minuten zachtjes pruttelen.
Voeg d estukken kabeljauw toe, doe de deksel op de pan en haat het gerecht in ongeveer 10 minuten gaar worden.
5. Garneer het gerecht met gehakte peterselie.

**Tip:** Serveer de kabeljauw met gekookte krieltjes.
