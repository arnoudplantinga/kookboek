
## Ingrediënten 4 personen  

- 4 pasteibakjes
- 2 el olie
- 300 g gesneden prei
- 500 ml volle melk
- 50 g besciamella sausmix
- 300 g zalmfilet
- 250 g cherrytomaten
- ½ komkommer
- 100 g botersla
- 3 el sladressing naturel

## Bereiden

1. Verwarm de oven voor op 220 °C. Zet tijdens het voorverwarmen de pasteibakjes ca. 15 min. in de oven.
2. Verhit ondertussen de olie in een hapjespan en bak de prei 2 min. op hoog vuur. Schenk de melk erbij en voeg al roerend de sausmix toe. Breng al roerend aan de kook en laat de ragout 5 min. op laag vuur koken.
3. Snijd ondertussen de zalm in blokjes van 2 cm, halveer de tomaten en snijd de komkommer in dunne plakjes.
4. Voeg na 4 min. kooktijd de zalmblokjes toe aan de ragout. Laat de ragout nog 1 min. zachtjes koken tot hij de gewenste dikte heeft. Breng op smaak met (versgemalen) peper en eventueel zout.
5. Meng de sla, tomaat, komkommer en dressing. Verdeel de ragout over de pasteibakjes en serveer met de salade.
