
## Ingrediënten (4 personen)

- 300 g couscous
- 2 citroenen
- 1 ui
- 2 aubergines
- 5 el olijfolie
- 3 el rozijnen (zak)
- 50 ml kraanwater
- 370 g tonijnstukken in olie (blik)
- 1 bakje munt (bakje)

## Bereiden

1. Bereid de couscous volgens de aanwijzingen op de verpakking. Rasp de schil van 1 citroen en halveer de vrucht. Snijd de andere citroen in partjes. Snipper de ui en snijd de aubergine in blokjes.
2. Verhit olie in een hapjespan en fruit de ui 1 min en voeg de aubergine toe. Voeg het citroenrasp en de rozijnen toe. Knijp de citroenhelften boven de pan uit; houd een hand onder de citroenen om de pitjes op te vangen. Voeg water toe en stoof de aubergine met de deksel op de pan in 6 min. gaar.
3. Schep de couscous en de tonijn door de aubergine. Verwarm nog 1 min. Snijd de munt in reepjes en meng met de rest van de olie door de couscous. Breng op smaak met peper en zout.
4. Serveer de couscous met de partjes citroen.
