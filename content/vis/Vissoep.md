
## Ingrediënten

- 1 voorgebakken boulogne stokbrood
- 3 visbouillontabletten
- 1/2 theelepel gedroogde tijm
- 1 zak minikrieltjes (450 g)
- 1 zakje fijne soepgroenten (150 g)
- 1 bakje voorgesneden snijbonen (150 g)
- 400 g kabeljauwfilet (in reepjes)
- 125 ml slagroom


1. Oven voorverwarmen op 200 °C. Stokbrood in 6-8 min. afbakken.
2. In pan 1 1/2 liter water met verkruimelde bouillontabletten en tijm aan de kook brengen. Krieltjes door bouillon roeren en soep 5 min. zachtjes laten koken.
3. Soepgroenten, snijbonen en reepjes kabeljauw toevoegen en soep nog 4 min. zachtjes laten koken.
4. Slagroom erdoor roeren en 1 min. meewarmen. Op smaak brengen met zout en peper.
5. Soep serveren met stokbrood.
