### Informatie

-   2 personen
-   15-30 minuten

###  

### Ingrediënten

-   1 ui
-   500 gram winterwortel
-   1 eetlepel olie
-   2 theelepels komijn (djinten)
-   1 theelepel kaneel
-   2 eetlepels bloem
-   2 pangasius- of tilapiafilets
-   1&frac12; eetlepel vloeibare margarine

 

### Bereiding

1.  Maak de ui en de wortels schoon. Snijd de ui in snippers en de
    wortels in plakken.
2.  Fruit de ui met 1 theelepel komijn en &frac12; theelepel kaneel in
    de olie.
3.  Voeg de wortels toe met wat water en stoof de groente in 15
    minuten gaar.
4.  Stamp de groente tot een smeuïge puree en maak het op smaak met een
    beetje zout.
5.  Meng de bloem met de rest van de komijn en de kaneel en doe dit in
    een bord.
6.  Bestrooi de visfilets met peper en een beetje zout en wentel de vis
    door het bloemmengsel.
7.  Verwarm de margarine in een koekenpan met een anti aanbakbodem en
    bak hierin de vis in 8 minuten gaar en bruin.
8.  Geef er de wortelpuree bij.

###  

### Menusuggestie

Lekker met röstirondjes uit de oven.

 

### Voedingswaarden per persoon

                  Recept     Menu
  --------------- ---------- ----------
  Energie         370 kcal   595 kcal
  Eiwit           25 gram    30 gram
  Koolhydraten    25 gram    60 gram
  Vet             15 gram    25 gram
  Verzadigd vet   2 gram     3 gram
  Vezels          9 gram     10 gram

 
