## Ingrediënten 4 personen
- 250 g sushirijst
- 120 ml Saitaku rijstazijn
- 300 g verse zalmfilet
- ½ rode peper
- 2 el Kikkoman sojasaus
- 1 el sesamolie
- 200 g bospeen
- 1 komkommer
- 3 bosuien
- 2 eetrijpe avocado's
- 2 tl zwart sesamzaad

## Bereiding

1. Kook de rijst volgens de aanwijzingen op de verpakking. Roer ⅔ van de rijstazijn door de rijst als deze nog warm is. Laat de rijst in 1 uur afkoelen op een grote platte schaal.
1. Snijd ondertussen de zalm in blokjes van 1 cm (brunoise). Snijd het steeltje van de halve rode peper en snijd het vruchtvlees fijn. Meng de zalm met de peper, sojasaus en olie. Zet tot gebruik afgedekt in de koelkast.
1. Schaaf de komkommer en bospeen met een kaasschaaf of dunschiller in lange linten. Besprenkel met de rest van de rijstazijn. Snijd de bosuien in schuine ringetjes. Snijd de avocado’s overlangs doormidden en verwijder de pit. Schep het vruchtvlees uit de schil en snijd in plakjes.
1. Verdeel de rijst over kommen. Verdeel de zalm, komkommer, wortel, bosui en avocado erover. Bestrooi de zalm met het sesamzaad.

Combinatietip: Lekker als extra ingrediënten voor deze pokébowl: sushigember en zeewiersalade (wakame).

Algemeen: Verwerk rauwe vis zo vers mogelijk en houd hem gekoeld tot gebruik.

Variatietip: Vervang de zalm ook eens door tonijnfilet.
