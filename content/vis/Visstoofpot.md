
## Ingrediënten (2 personen)

- 1 fijngesneden ui
- 1 teen fijngesneden knoflook
- Provencaalse kruiden
- 1 aubergine
- 1 courgette
- 1 paprika
- 200 ml gezeefde tomaten
- 250g kabeljauw
- 1 blik reuzenbonen (uitlekgewicht 255g)

## Bereiding

1. Verhit olie in een pan met een dikke bodem en voeg de ui en knoflook en provencaalse kruiden toe.
2. Snijd de aubergine in blokjes, voeg deze toe en bak ze kort. Snijd ondertussen de courgette en paprika en bak deze ook mee.
3. Voeg de tomatenp en kabeljauw in blokjes toe. Voeg tot slot de reuzenbonen toe en laat dit nog ongeveer 10 minuten sudderen, tot de kabeljauw gaar is. Breng op smaak met peper.
