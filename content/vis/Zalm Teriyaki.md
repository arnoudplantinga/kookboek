
## Ingrediënten

- 7 el Japanse sojasaus (flesje 250 ml)
- 2 el vloeibare honing
- 1 el wittewijnazijn
- 1 el sesamzaad (bakje 100 g)
- 250 g eiernoedels (pakje)
- 1 rode ui
- 400 g broccoli
- 4 el zonnebloemolie
- 200 g verse peultjes
- 300 g diepvries wilde zalm

## Bereiden

1. Verwarm de sojasaus, de honing en de azijn in een steelpan. Neem deze teriyaki-marinade van het vuur. Rooster het sesamzaad in een koekenpan zonder olie of boter in 3 min. goudbruin. Laat afkoelen op een bord. Kook de eiernoedels volgens de aanwijzingen op de verpakking.
2. Snijd de ui in halve ringen en verdeel de broccoli in roosjes. Schil eventueel de steel en snijd deze in plakjes. Verhit 1/2 van de olie in een wok en roerbak de ui, broccoli en peultjes in 5 min. beetgaar.
3. Verdeel de ontdooide zalm in porties en bestrijk elk stuk aan beide kanten dun met de rest van de olie. Verhit de grillpan. Bestrijk de stukken zalm met de marinade en gril ze in 4 min. rosé. Keer regelmatig en bestrijk steeds met de marinade.
4. Schep de groenten door de noedels. Breng op smaak met de rest van de sojasaus en peper. Verdeel over kommen en leg de zalm teriyaki erop. Bestrooi met het sesamzaad. Bon appétit!
