Uit de voedselzandloper.

 

**Wat heb je nodig?**

*Als voorgerecht voor 4 personen, hoofdgerecht voor 2 personen*

-   2 avocado’s
-   2 lente uitjes
-   stuk komkommer (+/- 7 cm)
-   100 gram gepelde Hollandse garnalen
-   1 eetlepel (suikervrije) mayonaise
-   3 takjes verse dille
-   sap van een halve citroen
-   snuf peper en zout

 

**En hoe maak je het?**

1.  Snijd de avocado’s doormidden en haal bij beide de pit eruit. Snijd
    de avocado’s dan eerst in de lengte in en daarna in de breedte en
    let hierbij op dat je niet door de schil heen snijdt. Lepel dan
    voorzichtig het avocado vruchtvlees uit de avocado’s en doe dit in
    een apart kommetje. Bewaar de avocado schillen even apart.
2.  Snijd ondertussen de komkommer in blokjes en de lente ui
    in ringetjes. Schep, samen met de garnalen, voorzichtig door de
    avocado blokjes.
3.  Maak de dressing van zout, peper, dille, mayonaise en citroensap en
    doe hier eventueel een theelepeltje water bij. Doe de dressing dan
    op het avocado mengsel en schep heel voorzichtig door. Schep dit dan
    weer in de lege avocado schillen en serveer met nog een takje verse
    dille bovenop.

 

Bron:
  

 
