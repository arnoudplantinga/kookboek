#### Ingrediënten

-   [600 g broccoli]( "zet ingrediënt op mijn lijst")
-   [250 g witte rijst (zak a 1 kg)]( "zet ingrediënt op mijn lijst")
-   [2 uien]( "zet ingrediënt op mijn lijst")
-   [250 g cherrytomaatjes]( "zet ingrediënt op mijn lijst")
-   [1 pakje gestoomde makreel (ca.
    350 g)]( "zet ingrediënt op mijn lijst")
-   [3 eetlepels zonnebloemolie]( "zet ingrediënt op mijn lijst")
-   [1 eetlepel sambal]( "zet ingrediënt op mijn lijst")
-   [2 theelepels kerriepoeder]( "zet ingrediënt op mijn lijst")
-   [2 eetlepels ketjap manis]( "zet ingrediënt op mijn lijst")

 

#### Bereiden

1. Broccoli schoonmaken, in kleine roosjes verdelen en steel in plakjes snijden.
2. Broccoli in ruim kokend water met wat zout in ca. 8 min. beetgaar koken. Afgieten.
2. Rijst bereiden volgens aanwijzingen op verpakking.
2. Uien pellen en in ringen snijden. Tomaatjes schoonmaken en halveren. Vel van makreel verwijderen, vis van graat halen en in stukken verdelen.
2. Olie in pan verhitten, uien toevoegen en 3 min. bakken. Sambal en kerriepoeder toevoegen en 1 min. meebakken. Gehalveerde tomaten en peper naar smaak toevoegen en 2 min. bakken. Ketjap erdoor roeren. Makreel toevoegen en kort verwarmen, zo min mogelijk roeren.
2. Rijst over 4 borden verdelen, pittige makreel erover en broccoli ernaast scheppen.

 
