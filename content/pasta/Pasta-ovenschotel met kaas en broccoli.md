Lekkere pasta-ovenschotel met broccoli
======================================

*Lekker recept voor een pasta gerecht uit de oven. Ik vind het heerlijk
om doordeweeks, als je niet al te veel tijd hebt, een lekkere maar
simpele ovenschotel te maken zoals deze. Deze ovenschotel bestaat uit
broccoli, pasta, een heerlijk kaassausje, ui en rode peper. Als je niet
van broccoli houdt kan je deze ovenschotel ook maken met bijvoorbeeld
bloemkool. Een lekker recept en simpel te bereiden!*

 

*Tijd: 20 min + 25 min. in de oven\
Dit recept is voor: 3 tot 4 personen.*

**Benodigdheden:**

-    350 gram penne
-   300 gram broccoli (ik heb een zakje broccoli gekocht)
-   1 rode ui
-   1 rode peper
-   50 gram boter
-   50 gram bloem
-   500-550 ml melk
-   geraspte kaas
-   kipfilet

[![](http://www.lekkerensimpel.com/wp-content/uploads/2012/05/DSC_1395-1024x685.jpg "DSC_1395"){width="614"
height="411"}](http://www.lekkerensimpel.com/wp-content/uploads/2012/05/DSC_1395.jpg)

**Bereidingswijze:**

Begin met het opzetten van een pan water voor de pasta en begin daarna
met het voorverwarmen van de oven op 200 graden. Ik kook ondertussen de
broccoli in het zakje in de magnetron totdat deze een beetje zacht is.
Ik hou zelf van zachte broccoli dus ik kook hem in de magnetron voor
ongeveer 6 minuten.

Als het water kookt voeg je de pasta toe en kook je deze volgens de
bereidingswijze op het pak. Snijd ondertussen een uitje en de rode peper
fijn. Maak daarna de kaassaus. Smelt eerst de boter in een pannetje op
een laag vuurtje. Voeg dan de bloem toe en roer het goed door. Laat de
bloem met de boter even gaar worden en voeg dan de melk toe. Roer het
geheel goed door en zet het vuur iets hoger. Als het sausje een beetje
warm is geworden voeg je wat geraspte kaas door. Roer dit weer goed door
en voeg weer wat kaas toe als de andere geraspte kaas gesmolten is. Ga
zo door totdat het sausje naar keuze is.

Als de pasta klaar is met koken, giet je deze af en schep je de pasta in
een grote ovenschaal.

[![](http://www.lekkerensimpel.com/wp-content/uploads/2012/05/DSC_1398-1024x685.jpg "DSC_1398"){width="614"
height="411"}](http://www.lekkerensimpel.com/wp-content/uploads/2012/05/DSC_1398.jpg)

Als de broccoli ook klaar is met koken snijd je deze eventueel nog wat
kleiner en roer je deze door de pasta in de ovenschaal.

[![](http://www.lekkerensimpel.com/wp-content/uploads/2012/05/DSC_1400-1024x685.jpg "DSC_1400"){width="614"
height="411"}](http://www.lekkerensimpel.com/wp-content/uploads/2012/05/DSC_1400.jpg)

Als het sausje ook klaar is giet je ongeveer de helft in de ovenschaal
en roer je dit goed door. Voeg nu ook het gesnipperde uitje en het rode
pepertje toe. Roer alles nogmaals goed door en giet daarna de rest van
de saus over de ovenschotel. Als je wil kan je eventueel nog een klein
beetje geraspte kaas over de ovenschotel.

[![](http://www.lekkerensimpel.com/wp-content/uploads/2012/05/DSC_1401-1024x685.jpg "DSC_1401"){width="614"
height="411"}](http://www.lekkerensimpel.com/wp-content/uploads/2012/05/DSC_1401.jpg)

De ovenschotel kan nu voor ongeveer 25 minuten de oven in. Na 25 minuten
is de ovenschotel klaar en serveer je deze lekker met een salade, mmm!

[![](http://www.lekkerensimpel.com/wp-content/uploads/2012/05/DSC_1403-1024x924.jpg "DSC_1403"){width="614"
height="554"}](http://www.lekkerensimpel.com/wp-content/uploads/2012/05/DSC_1403.jpg)

[![](http://www.lekkerensimpel.com/wp-content/uploads/2012/05/DSC_1414-1024x685.jpg "DSC_1414"){width="614"
height="411"}](http://www.lekkerensimpel.com/wp-content/uploads/2012/05/DSC_1414.jpg)

 

bron:
<http://www.lekkerensimpel.com/2012/05/09/lekkere-pasta-ovenschotel-met-broccoli/> 

 
