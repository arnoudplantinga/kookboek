## Ingrediënten 4 personen

- 120 gr salami (boterham beleg)
- 250 gr pasta
- 1 paprika
- 1 courgette
- 1 bol mozzarella
- handje geraspte kaas
- 400 ml gezeefde tomatensaus
- 0.5 eetlepel italiaanse kruiden
- basilicum

## Bereiding

Verwarm de oven op 200 graden. Snijd de paprika en courgette in stukjes. Verhit een beetje olie of margarine in een pan. Voeg de paprika en courgette toe en roerbak een paar minuutjes. Breng de groenten op smaak met een snufje peper, zout en Italiaanse kruiden.

Kook ondertussen ook de pasta gaar in ca 10 minuten en giet daarna af. Bewaar 6 plakken salami en snijd de rest in grove stukken, voeg toe aan de groenten in de pan. Doe dan de gezeefde tomatensaus erbij. Voeg als laatste de pasta toe aan de pan en schep alles goed door elkaar.

Doe de helft van het pastamengsel in een ingevette ovenschaal. Verdeel hier de helft van de mozzarella in stukjes over en een paar basilicumblaadjes. Dek af met de rest van het mengsel. Verdeel hier de rest van de mozzarella in stukjes over en de geraspte kaas. Leg als laatste de overige 6 plakjes salami op de ovenschotel.

Zet de pastaschotel met salami ongeveer 20 minuten in de oven tot de kaas mooi gesmolten is. Garneer de ovenschotel met wat verse basilicum.
