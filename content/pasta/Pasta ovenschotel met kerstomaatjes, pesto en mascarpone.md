
## INGREDIËNTEN 

-   250
    gram [kerstomaatjes](http://www.mijnreceptenboek.nl/ingredienten/groenten/cherrytomaten.html "Ingrediëntinformatie over kerstomaatjes"),
    gehalveerd
-   bertolli extra
    vergine [olijfolie](http://www.mijnreceptenboek.nl/ingredienten/olien-en-vetten/olijfolie.html "Ingrediëntinformatie over olijfolie")
-   400 gram pipe rigate
    (grote [elleboogjes](http://www.mijnreceptenboek.nl/ingredienten/pasta/macaroni.html "Ingrediëntinformatie over elleboogjes") pasta)
-   250
    gram [mascarpone](http://www.mijnreceptenboek.nl/ingredienten/kaas/mascarpone.html "Ingrediëntinformatie over mascarpone")
-   4 eetlepel bertolli [pesto
    rosso](http://www.mijnreceptenboek.nl/ingredienten/sauzen/pesto.html "Ingrediëntinformatie over pesto rosso")
-   1
    stronk [broccoli](http://www.mijnreceptenboek.nl/ingredienten/groenten/broccoli.html "Ingrediëntinformatie over broccoli")
-   150
    gram [sperziebonen](http://www.mijnreceptenboek.nl/ingredienten/peulvruchten/sperziebonen.html "Ingrediëntinformatie over sperziebonen")
-   15 gram [verse
    basilicum](http://www.mijnreceptenboek.nl/ingredienten/kruiden/basilicum.html "Ingrediëntinformatie over verse basilicum"),
    in reepjes gesneden
-   100 gram geraspte [parmezaanse
    kaas](http://www.mijnreceptenboek.nl/ingredienten/kaas/parmezaanse-kaas.html "Ingrediëntinformatie over parmezaanse kaas")
-   [peper](http://www.mijnreceptenboek.nl/ingredienten/specerijen/peper.html "Ingrediëntinformatie over peper") en [zout](http://www.mijnreceptenboek.nl/ingredienten/mineralen/zout.html "Ingrediëntinformatie over zout") uit
    de molen

 

## BEREIDINGSWIJZE

Verwarm de oven voor op 200°C. Halveer de tomaatjes. Besmeer een
bakplaat met olijfolie en leg de tomaatjes er met de snijkant naar boven
op. Bestrooi ze met zout en peper en druppel er nog wat olijfolie op.
Zet de bakplaat in het midden van de oven en bak de tomaatjes 15
minuten. Breng in een pan ruim water aan de kook en blancheer de
sperziebonen 7 minuten en de broccoli 5 minuten in kokend water. Giet af
en spoel na met lauw water. Kook ondertussen in een andere pan de pasta
beetgaar en bewaar bij het afgieten een paar eetlepels van het
kookvocht. Doe de mascarpone in een kom en roer hier de pesto en het
kookvocht door. Meng het mascarponemengsel met de groenten door de pasta
en schep er dan de tomaatjes en de reepjes basilicum voorzichtig
doorheen. Schep dit in een met olijfolie ingevette ovenschaal en
bestrooi het geheel met parmezaanse kaas. Zet de ovenschaal terug in de
oven en bak het gerecht 15 minuten tot de kaas gesmolten en goudbruin
van kleur is.

 
