## Ingredients (SERVES 4)

- 1 small onion quartered
- 1 chopped small carrot peeled and roughly
- 1 chopped stalk celery roughly
- 2 anchovy fillets
- 2 medium cloves garlic
- ½ tsp dried oregano
- Pinch of red pepper flakes
- 250 grams button mushrooms
- 300 grams ground chuck
- 2 tbsp extra-virgin olive oil
- 2 tbsp unsalted butter
- 2 tbsp tomato paste
- 800 grams whole tomatoes packed in juice
- 1 tbsp Asian fish sauce
- ¼ cup grated Parmigiano-Reggiano
- Kosher salt and freshly ground black pepper
- 500 grams spaghetti
- Chopped fresh parsley or basil for serving

## Preparation

1.  Place the onion, carrot, celery, anchovies, if using, garlic, oregano, and pepper flakes in the bowl of a food processor and pulse until finely chopped, 8 to 10 short pulses, scraping down the sides as necessary. Transfer to a bowl. Add the mushrooms to the empty food processor bowl of and pulse until finely chopped, 6 to 8 short pulses. Add the meat to the processor and pulse until the meat and mushrooms are evenly mixed, 6 to 8 short pulses. Set aside.
2.  Heat the olive oil and butter in a Dutch oven over medium-high heat until the butter has melted and the foaming subsides. Add the chopped vegetables and cook, stirring frequently, until softened but not browned, about 5 minutes. Add the tomato paste and stir until homogeneous, about 1 minute. Add the meat/mushroom mixture and cook, stirring occasionally, until the moisture has completely evaporated and the mixture starts to sizzle, about 10 minutes. Add the tomatoes, with their juice, and bring to a boil over high heat, then reduce to a simmer and cook, stirring occasionally, until the sauce has reduced and thickened, about 30 minutes. Stir in the fish sauce and grated cheese and with salt and pepper to taste; keep warm.
3.  Meanwhile, bring a large pot of well-salted water to a boil.
4.  Cook the pasta according in the boiling water until it is fully softened but retains a slight bite in the center. Drain the pasta, reserving 1 cup of cooking liquid, and return to the pot. Add the sauce and stir to combine, adding some of the reserved pasta water as necessary to thin the sauce to the desired consistency. Serve immediately, topped with parsley and more Parmigiano-Reggiano.

## Description

In college, there was nothing easier than throwing a pound of ground beef into a pot, adding a jar of pasta sauce, and simmering them together, then tossing it all with pasta and calling it dinner. Tasty enough to be sure, but knowing what we know now, we can do a little better. For this sauce, I added carrot and celery to the onions to form the base of the marinara, along with the requisite garlic, oregano, and red pepper flakes. A couple of anchovy fillets add richness and meaty depth to the flavor. Anchovies contain both glutamates and inosinates—natural compounds that enhance the inherent meatiness of other ingredients, a factor further supported here by the use of glutamate-rich tomato paste. Chopping all the ingredients in the food processor makes short work of them.
As for the meat, I tried using straight-up ground beef but found that the texture was a little too tough without the benefit of a long, slow simmer (after all, I wanted this on the table within an hour or two). Instead, I decided to incorporate a technique frequently used for meat loaves and meatballs. By adding another element to the meat—mushrooms, in this case—and blending it right in, I was able to break up its texture, preventing it from getting tough while at the same time adding flavor.
