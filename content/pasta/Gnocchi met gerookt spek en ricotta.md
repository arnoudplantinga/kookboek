Weer eens iets anders dan traditionele pasta: Gnocchi zijn deegballetjes
van aardappel en tarwemeel. Je kunt ze net als pasta met diverse sauzen
maken. Ikzelf vind ze het lekkerst met kaassausjes. Daarom dit keer een
recept met ricottasaus, spek, champignons en tomaten.

 

-   *Voorbereidingstijd* <span style="font-size:100%;" title="PT5M">5
    minuten</span>
-   *Kooktijd* <span style="font-size:100%;" title="PT10M">10
    minuten</span>
-   *Aantal porties:* <span style="font-size:100%;">2</span>

#### Ingrediënten:

-   Gnocchi - 250 gram
-   Gerookt spek - een blok van zo'n 150 gram
-   Kastanjechampignons - 150 gram
-   Tomaat - 3 stuks stevige tomaten
-   Ricotta - 125 gram
-   Peterselie - een paar takjes
-   Evt. verse spinazie

#### Instructies:

1.  <span style="font-size:100%;">Kook de Gnocchi volgens de
    aanwijzingen op de verpakking. Tip: als ze boven komen drijven zijn
    ze gaar..</span>
2.  Snij de spek en champignons in blokjes
3.  Bak eerst de spekjes lichtbruin en voeg dan de champignons toe
4.  Bak gedurende 3 minuten op middelhoog vuur
5.  Voeg de tomaten toe en bak deze 2 minuten mee
6.  Doe nu ook de ricotta erbij en roer goed door
7.  Voeg nu de gare gnocchi toe en snij de peterselie fijn, roer deze
    ook erdoor
8.  Eet smakelijk!

 
