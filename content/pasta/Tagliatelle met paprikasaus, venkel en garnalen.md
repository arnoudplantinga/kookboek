## Ingrediënten 4 personen

- 300 g verse tagliatelle
- 2 venkelknollen
- 250 g cherrytomaten
- 2 tenen knoflook
- 2 el (olijf)olie
- 1 fles AH basis groentesaus paprika
- 100 g AH grote garnalen

## Bereiding

1. Kook de pasta volgens de aanwijzingen op de verpakking beetgaar. Snijd ondertussen de stelen van de venkelknollen en een stukje van de onderkant. Bewaar het venkelgroen. Halveer de venkel in de lengte, verwijder de harde kern en snijd in dunne partjes. Halveer de tomaten en snijd de knoflook fijn.

2. Verhit de helft van de olie in een hapjespan en bak de venkel 7 min. op hoog vuur. Voeg de tomaat en knoflook toe en bak nog 3 min. Voeg de saus toe en laat 2 min. op laag vuur zachtjes koken.

3. Verhit ondertussen de rest van de olie in koekenpan en bak de garnalen met peper en eventueel zout 5 min. op hoog vuur. Meng de pasta door de saus en verdeel over vier diepe borden. Verdeel de garnalen en het achtergehouden venkelgroen erover.

**Variatietip** Vervang de venkel door 400 g spinazie, laat de garnalen achterwege en bestrooi het gerecht met Parmezaanse kaas.

**Combinatietip** Lekker met citroenpartjes of verkruimelde feta.
