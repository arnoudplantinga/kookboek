Soms heb je maar 6 ingrediënten nodig om een lekker doordeweeks gerecht te maken.

- hoofdgerecht
- 4 personen
- 845 kcal voedingswaarden
-   15 min. bereiden

#### Ingrediënten

-   250 g champignons 
-   2 preien
-   2 el olijfolie
-   250 g hamblokjes 
-   200 ml crème fraîche 
-   45 g pesto alla Genovese (potje à
    90 g)
-   500 g tortellini 

#### Bereiden


1.  Snijd de champignons in plakjes. Was en snijd de preien in schuine ringen.
2. Verhit de olie in een hapjespan en bak de champignons en prei 7 min. op middelhoog vuur. Voeg de hamblokjes, crème fraîche en pesto toe. Schep om en warm nog 4 min. door. Breng op smaak met peper en eventueel zout.
2.  Kook ondertussen de pasta volgens de aanwijzingen op de verpakking beetgaar.
2. Giet de pasta af, verdeel over de borden en schep het groentemengsel erover. Serveer direct.

 

#### combinatietip:

Lekker met geraspte Parmezaanse kaas.

 
