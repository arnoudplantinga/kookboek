Vol en romig, met spinazie à la crème en zelfgemaakte bechamelsaus met gruyèrekaas.

hoofdgerecht

4 personen

670 kcal 

25 min. bereiden

30 min. oventijd

## Ingrediënten

- 900 g diepvries bladspinazie deelblokjes
- 50 g ongezouten roomboter
- 50 g tarwebloem
- 600 ml halfvolle melk
- 150 g geraspte gruyère (kaas)
- ½ tl gedroogde nootmuskaat
- 250 g verse lasagnebladen
- 250 g plakjes achterham

## Bereiden
1. Verwarm de oven voor op 180 °C. Ontdooi de spinazie in een ruime pan.
1. Smelt ondertussen de boter in een pan en voeg de bloem toe. Roer glad en laat 3 min. garen op laag vuur (roux). Voeg al roerend de melk toe. Zet het vuur iets hoger en roer tot een gebonden saus (bechamel). Voeg ⅔ van de gruyère en de nootmuskaat toe en roer tot de kaas is gesmolten. Breng op smaak met peper en eventueel zout.
1. Bedek de bodem van de ovenschaal met een laagje spinazie. Leg daarop 4 lasagnevellen. Dek af met weer een laag spinazie, dan een laag ham en schep er een laagje kaassaus op. Dek weer af met lasagnevellen, spinazie, ham en kaassaus tot alle ingrediënten op zijn. Zorg dat je eindigt met kaassaus. Bestrooi met de rest van de gruyère en bak de lasagne in het midden van de oven in ca. 30 min. gaar.

## Variatietip:
Vervang voor een extra hartig accent de ham door uitgebakken spekblokjes.

## Combinatietip:
Lekker met een salade van tomaat en basilicum.
