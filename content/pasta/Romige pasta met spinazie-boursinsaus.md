## INGREDIENTEN 4 PERSONEN
- 400 gram [penne](http://www.smulweb.nl/wiki/80/Penne) of strikjes [pasta](http://www.smulweb.nl/wiki/65/Pasta)
- 1 kleine ui of sjalotje
- 2 teentjes[knoflook](http://www.smulweb.nl/wiki/115/Knoflook)
- 150 gram gerookte spekreepjes
- 300 gram [spinazie](http://www.smulweb.nl/wiki/139/Spinazie) a la creme liefst deelblokjes
- 1 klein pakje boursin of andere roomkaas met kruiden
- 150 gram[champignons](http://www.smulweb.nl/wiki/113/Champignons)
- peper, [zout](http://www.smulweb.nl/wiki/218/Zout)
- eventueel wat [sambal](http://www.smulweb.nl/wiki/186/Sambal) voor de liefhebbers

Deze keer zonder champignons, maar lekker![![](http://images.smulweb.nl/recepten/200610/1148615/low_res/pastaspinazie.JPG)](http://images.smulweb.nl/recepten/200610/1148615/high_res/pastaspinazie.JPG)


## Voorbereiding

1. Ontdooi de spinazie.
2. Pel en snipper de ui en de knoflook.
3. Kook de pasta volgens de gebruiksaanwijzing op het pak.
4. Maak de champignons schoon en snij deze in plakjes.

## Bereidingswijze

1. Bak de spekreepjes in een droge [koekenpan](http://www.smulweb.nl/site/clickout/1064) langzaam uit.
2. Voeg de uisnippers toe en bak deze zachtjes mee tot ze glazig zijn. 
2. Voeg de champignons erbij en bak deze mee tot ze goudbruin zijn.
2. Voeg de knoflook toe en bak even mee.
2. Doe dan de ontdooide spinazie erbij en het pakje boursin.
2. Breng alles aan de kook en roer goed door elkaar.
2. Maak op smaak met peper en zout.

## Serveertips

De saus kan door de pasta geroerd worden of apart worden serveerd.  
Voeg voor een pittige variant een lepeltje sambal toe
