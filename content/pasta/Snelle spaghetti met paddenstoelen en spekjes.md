## Ingrediënten

- 15 g porcini gedroogd eekhoorntjesbrood
- 100 ml kraanwater
- 150 g spaghetti
- 100 g gerookte spekreepjes
- 2 sjalotten
- 400 g paddenstoelen voor pasta
- ½ tl gedroogde rozemarijn
- 3 el crème fraîche
- 60 g Regato 40+ kaas

## Bereiding

1. Breng het water aan de kook. Doe de gedroogde paddenstoelen in een kom en schenk het kokend water erover. Laat 10 min weken. Breng ondertussen een ruime pan water aan de kook. Voeg, als het water kookt, de spaghetti en eventueel zout toe en kook de pasta in 8 min. gaar.
2. Verhit ondertussen een hapjespan zonder olie of boter en bak het spek 5 min. Maak ondertussen de sjalotten schoon en snijd in ringen. Snijd de verse paddenstoelen grof. Voeg de sjalot, verse paddenstoelen en rozemarijn toe aan het spek en bak nog 4 min.
3. Laat ondertussen de geweekte paddenstoelen uitlekken en vang het vocht op. Snijd ze fijn en voeg met het opgevangen vocht, de crème fraîche en spaghetti toe aan de hapjespan en meng. Breng op smaak met peper. Verdeel over diepe borden en bestrooi met de kaas.
