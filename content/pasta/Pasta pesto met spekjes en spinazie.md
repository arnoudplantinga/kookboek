**Ingrediënten**

300 g spaghetti

2 el olijfolie

1 duopak gerookte spekreepjes mager (250
g)

1 prei, in dunne ringen

1/2 potje pesto alla genovese (a 190
g)

1 zak spinazie (600 g)

 

**Bereiden**

1. Kook de spaghetti volgens de
aanwijzingen op de verpakking. 

2. Verhit ondertussen de
olie in een koekenpan en bak de spekblokjes 5 min. Voeg de prei toe en
bak 3 min. mee. Voeg de pesto en peper naar smaak toe. Voeg een handvol
spinazie toe en laat slinken. Herhaal tot de spinazie op is. 

3. Giet de spaghetti af
en schep het pesto-spinaziemengsel erdoor. Verdeel de pasta over 4
borden. Lekker met flinters Parmezaanse kaas.

 

 
