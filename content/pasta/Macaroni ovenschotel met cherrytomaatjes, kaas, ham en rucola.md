_We hebben weer een lekker en simpel ovenschotel recept voor jullie! Deze keer hebben we een macaroni-ovenschotel met onder andere hamreepjes en cherrytomaatjes gemaakt. Ook deze ovenschotel is ideaal om van te voren te bereiden zodat je ‘s avonds de ovenschaal alleen nog maar in de oven hoeft te schuiven._

_Tijd: 20 min. + 15 min in de oven_  
_Recept voor 2 personen_

**Benodigdheden:**

* 150 gram macaroni
* 1 ui
* 1 teen knoflook
* circa 10 cherrytomaatjes
* 200 gram hamreepjes
* 12,5 gram bloem
* 12,5 gram boter
* 300 ml melk
* flinke hand geraspte kaas
* serveren met rucola
* **Tip: voeg eventueel andere groenten naar keuze toe zoals courgette, prei en paprika**

[![photo IMG_7226_zps29c83870.jpg](http://i1279.photobucket.com/albums/y537/lekkerensimpel/2014/jan/IMG_7226_zps29c83870.jpg)](http://s1279.photobucket.com/user/lekkerensimpel/media/2014/jan/IMG_7226_zps29c83870.jpg.html)

**Bereidingswijze:**

Begin met het opzetten van een pan water voor de macaroni. Kook de macaroni volgens de bereidingswijze op het pak. Verwarm ondertussen je oven voor op 220 graden. Snipper daarna het uitjes, snijd het teentje knoflook fijn en snijd de cherrytomaatjes in vier of acht stukjes. Zet een klein pannetje op het vuur en smelt de boter. Bak vervolgens de ui en knoflook aan. Na circa 2-3 minuten voeg je de bloem toe en bak je deze ongeveer 1 minuut mee. Voeg vervolgens de melk toe en meng het geheel goed door elkaar. Warm de saus op en voeg vervolgens de geraspte kaas toe.

[![photo IMG_7230_zps1c839383.jpg](http://i1279.photobucket.com/albums/y537/lekkerensimpel/2014/jan/IMG_7230_zps1c839383.jpg)](http://s1279.photobucket.com/user/lekkerensimpel/media/2014/jan/IMG_7230_zps1c839383.jpg.html)

Je kunt zoveel kaas toevoegen als je lekker vindt. Breng de saus eventueel verder op smaak met een snufje zout/peper. Schep de macaroni in een ovenschotel.

[![photo IMG_7227_zps32732f23.jpg](http://i1279.photobucket.com/albums/y537/lekkerensimpel/2014/jan/IMG_7227_zps32732f23.jpg)](http://s1279.photobucket.com/user/lekkerensimpel/media/2014/jan/IMG_7227_zps32732f23.jpg.html)

Meng in een ovenschaal de macaroni met het sausje, de cherrytomaatjes en de hamreepjes. Je kunt eventueel nog een klein beetje geraspte kaas over de ovenschotel strooien.

[![photo IMG_7232_zps9fc38355.jpg](http://i1279.photobucket.com/albums/y537/lekkerensimpel/2014/jan/IMG_7232_zps9fc38355.jpg)](http://s1279.photobucket.com/user/lekkerensimpel/media/2014/jan/IMG_7232_zps9fc38355.jpg.html)

Zet de macaroni-ovenschotel voor circa 15 minuten in de oven. Serveer de macaroni-ovenschotel met wat rucola.  
**.**

[![photo IMG_7257_zpsa35c9069.jpg](http://i1279.photobucket.com/albums/y537/lekkerensimpel/2014/jan/IMG_7257_zpsa35c9069.jpg)](http://s1279.photobucket.com/user/lekkerensimpel/media/2014/jan/IMG_7257_zpsa35c9069.jpg.html)
