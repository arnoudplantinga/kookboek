hoofdgerecht

4 personen

765
kcal voedingswaarden

-   20 min. bereiden

 

#### Ingrediënten

-   150 g tagliatelle 
-   2 el olijfolie
-   125 g magere spekreepjes 
-   1 teen knoflook
-   125 g kastanjechampignons 
-   1 tl gedroogde oregano 
-   200 g witlof
-   125 ml crème fraîche

###### Lekker erbij

-   peterselie

#### Bereiden

1.  Kook de tagliatelle volgens de aanwijzingen op de
    verpakking beetgaar. Verhit ondertussen de olie in een koekenpan en
    bak de spekreepjes 5 min. Neem ze met een schuimspaan uit de pan.
    Snijd de knoflook fijn en halveer de kastanjechampignons. Bak de
    knoflook, champignons en oregano 4 min. in het
    achtergebleven bakvet.
2.  Snijd ondertussen de onderkant van de stronken witlof en snijd het
    witlof in de lengte in vieren. Verwijder de harde kern. Voeg het
    witlof toe aan de champignons en bak 2 min.
3.  Doe de spekreepjes en crème fraîche bij de groenten en verwarm nog
    2 min. op laag vuur. Breng op smaak met peper. Verdeel de
    tagliatelle over borden en schep de champignons met witlof erover.

 
