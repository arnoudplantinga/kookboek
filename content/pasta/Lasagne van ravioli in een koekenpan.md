Voorbereiding 5 minuten

Kooktijd 5 minuten

Calorieën 564 kcal

## Ingrediënten voor 4 personen

- 400 ml kerstomaten uit blik
- 400 ml peperoncino pastasaus (of andere pastasaus)
- 80 ml water
- 500 gram van je favoriete ravioli (of een mix)
- 200 gram ricotta
- 2 bollen mozzarella, in stukjes gescheurd
- Handje verse basilicum

## Bereiding

Verwarm de oven op de grillstand.

Verhit een ovenbestendige pan op medium-hoog vuur en schenk er de peperoncinosaus, kerstomaatjes en het water in. Kruid af met zout en peper en roer goed. Als je op dit moment de behoefte voelt om je saus wat te pimpen met bijvoorbeeld een scheutje rode wijn, een beetje citroensap en/of verse oregano: laat je gaan.

Begint het te pruttelen? Voeg er dan ravioli aan toe en roer voorzichtig alles om. Zet het vuur desnoods iets lager. Laat ongeveer 3 minuten koken, tot de ravioli al dente is.

Voeg er de helft van de mozzarella aan toe en roer alles nog eens om. Schep er ook wat hoopjes van de ricotta bij. werk af met de andere helft van de mozzarella.

Zet dit 3 à 5 minuten onder de grill, tot de mozzarella bruine plekjes krijgt. Laat nog even 5 minuten afkoelen en werk af met verse basilicum.
