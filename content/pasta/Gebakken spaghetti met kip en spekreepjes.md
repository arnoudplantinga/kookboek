## Ingrediënten

Voor 2 vierkante ovenschalen:

- 450 gram champignons, in plakjes
- 450 gram spaghetti of linguine
- 500 gram gare kipreepjes (of kip + spekreepjes)
- 285 gram doperwten (diepvries)
- 6 eetlepels boter
- 60 gram bloem
- 400 ml kippenbouillon
- 180 ml droge witte wijn
- 700 ml melk
- 250 gram geraspte kaas, geraspt
- 1 theelepel gedroogde tijm
- Zout en peper
- Evt. cashewnoten en edelgistvlokken

Het originele recept van Martha Stewart vraagt om wat kant-en-klare kip. Daarvoor kun je bijvoorbeeld een gebraden rotisseriekip nemen. Wij namen een halve en vulden die aan met wat uitgebakken spekreepjes. Je kunt ook kant-en-klare (gerookte) kipreepjes nemen of zelf wat reepjes kipdijfilet bakken. En heb je nog restjes kip over, dan is dit een fijn recept om die in te verwerken.

In plaats van spaghetti kun je ook linguine nemen. En wat nou handig is: mocht je een keer overgare pasta hebben, dan kun je die ook in dit recept gebruiken. Hoef je die niet weg te gooien!

## Zo maak je deze gebakken spaghetti

Verwarm de oven voor op 200 graden.

Kook eerst de spaghetti tot ‘ie al dente (beetgaar) is. Giet af en zet opzij.

Doe twee eetlepels boter in een grote koekenpan. Bak de champignons tot ze lekker bruin zijn en voeg zout en peper toe. Haal ze uit de pan.

Doe de rest van de boter in de pan en voeg de bloem toe en roer goed door met een garde. Laat ongeveer een minuutje bakken: zo verliest de bloem de typische ‘bloemsmaak’ en dat willen we. Voeg dan, onder voortdurend roeren, voorzichtig de bouillon, wijn en melk toe.

Breng de saus aan de kook en laat lekker weg bubbelen. Als je een mooie dikke saus hebt, voeg je 2/3e van de Parmezaanse kaas toe en roer je die door de saus. Doe ook de gedroogde tijm erbij en roer de doperwten erdoorheen. Breng op smaak met peper en eventueel zout.

Nu kun je de spaghetti, de saus, de kip en de champignons samenvoegen. Roer door elkaar in een grote pan.

Verdeel de spaghetti over twee ovenschalen en bestrooi met de rest van de Parmezaanse kaas.

Zet de schalen ongeveer 30 minuten in de hete oven, tot de bovenkant een goudbruin korstje heeft gekregen.
