### Voor twee grote of drie kleine(re) eters

* 250 gram pasta in je favoriete vorm
* een bakje vegetarische spekreepjes
* een flinke scheut olijfolie
* 2 eieren
* 50 gram geraspte parmezaanse kaas, plus extra voor het serveren.

### Bereiden

1.  Breng een grote pan water aan de kook, doe de pasta erin en een lepel zout, roer even en laat de pasta gaar koken.
2.  Verhit ondertussen de olie in een koekenpan. Doe als de olie heet is de spekjes erbij en laat ze in een minuut of vijf knapperig bakken.
3.  Kluts ondertussen de eieren en meng de geraspte kaas erdoor, en flink wat zwarte peper.
4.  Als de pasta klaar is, giet je hem snel af, doet de pasta terug in de pan en doet de spekjes (inclusief bakvet) en het eimengsel erdoor. Roer goed, tot de pasta romig is. Hou het vuur aan maar niet te hoog: het moet een romige saus worden, geen roerei.
5.  Eet de pasta met meer parmezaanse kaas

Ik hou ook erg van een versie waar Italianen zich vast een rolberoerte van zouden schrikken:

* Snijd een flinke ui (of twee kleinere) in halve ringen en een een doosje champignons in plakjes.
* Bak de spekjes een minuut of drie in hun eentje en doe daarna de ui en de champignons erbij, laat het samen ook nog een minuut of drie bakken, tot de ui en champignons mooi bruin en gaar zijn.
