Bereidingstijd 30 min

## Ingrediënten 4 personen

- 2 witte uien
- 4 teentjes knoflook
- 200 gr zongedroogde tomaten
- 4 el olijfolie
- 4 zalmfilets
- 200 ml groentebouillon
- 200 ml kookroom
- 200 gr mascarpone
- 200 ml tomatensaus
- 320 gr tagliatelle
- 100 gr Parmezaanse kaas
- 400 gr babyspinazie 

## Bereiding

1. Pel en snipper de ui. Pel de knoflook en hak fijn. Snijd de grote zongedroogde tomaten fijn, laat de kleine heel.

2. Verhit de olijfolie in een koekenpan op middelhoog vuur. Bak de zalmfilet 2 tot 3 min. aan op de bovenkant van de filet. Schep uit de pan.

3. Fruit in dezelfde pan de ui en knoflook 2 min. aan. Voeg de zongedroogde tomaten toe en bak 2 min. mee. Voeg de groentebouillon toe en laat 2 min. doorkoken. Voeg de kookroom, mascarpone, tomatensaus en zalmfilets toe, draai het vuur laag en laat 10 min. rustig doorkoken.

4. Bereid ondertussen de tagliatelle volgens aanwijzingen op de verpakking. Rasp de Parmezaanse kaas grof.

5. Neem de zalmfilets uit de saus en voeg de spinazie toe. Laat in 1 min. slinken. Voeg de helft van de Parmezaanse kaas en de tagliatelle toe en roer goed door.

6. Verdeel de tagliatelle over de borden. Verdeel de zalmfilets op de tagliatelle en serveer met de rest van de Parmezaanse kaas. 
