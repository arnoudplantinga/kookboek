## Ingrediënten (5 personen)
- 2 uien
- 8 eetlepels olijfolie
- 4 teentjes knoflook
- 2 eetlepels tomatenpuree
- 2 blikken tomatenblokjes (a 400 g)
- 1 theelepel gedroogde oregano
- 1 courgette
- 1 aubergine
- 300 g spinazie
- 465 g geroosterde rode paprikas (pot)
- 250 g gedroogde lasagnevellen
- 250 g ricotta
- 2 bollen buffelmozzarella (a 125 g)
- 25 g Parmezaanse kaas
- Eventueel 1 bakje pistachenoten

## Bereiding
1. Snipper de uien. Verhit 2 eetlepels olie en bak de uien op míddelhoog vuur 10 minuten. Roer af en toe en draai het vuurlaag als de ui te donker dreigt te worden. Snijd de knoflook fijn en bak even mee. Voeg de tomatenpuree, tomatenblokjes en oregano toe en laat 10 minuten zacht koken.
