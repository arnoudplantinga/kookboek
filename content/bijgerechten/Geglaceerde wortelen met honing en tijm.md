
Glaceren betekent niets meer en minder dan rustig laten garen totdat het product zacht is en glanst aan de buitenkant. Hiervoor gebruik je een combinatie van vet en vloeistof. Het is een kooktechniek die je goed kunt toepassen op wortelen. Met de toevoeging van honing, tijm en een gekneusd teentje knoflook heb je binnen 15 minuten een heerlijk bijgerecht.

## Ingrediënten (bijgerecht voor 4 personen)

– 800gr oranje of gekleurde wortelen
– klontje boter
– 100ml groentebouillon of water
– flinke scheut honing
– 6 takjes tijm
– 2 teentjes knoflook, gekneusd
– peper
– zout

## Bereiding
Bereidingstijd: 15 minuten

Boen de wortelen schoon of rasp ze met een dunschiller. Snijd vervolgens in de lengte doormidden. Grote exemplaren snijd je in vieren.

Zet een grote hapjes- of koekenpan op het vuur en smelt de boter op medium vuur. Schep de wortelen door de boter en laat 1 minuut bakken. Schenk daarna de bouillon (of water) erbij samen met een flinke scheut honing. Schep ook de tijm en gekneusde knoflook erdoor. Laat circa 8 minuten op laag vuur garen totdat de wortelen beetgaar zijn. Als er nog teveel vocht in de pan zit, kun je de wortelen eruit scheppen en op hoog vuur laten inkoken. Schep daarna de wortelen er weer door en verwarm. Breng eventueel extra op smaak met peper en zout.
