## Ingrediënten

- 5 el sesamzaad
- 2 tenen knoflook
- 200 g kikkererwten (blik)
- 1 el Griekse yoghurt
- 2 el olijfolie
- 3 el citroensap
- 1 mespunt suiker
- 1 mespunt cayennepeper
- 1 mespunt paprikapoeder
- 2 el peterselie

## Bereiding

1. Rooster het sesamzaad in een koekenpan zonder olie of boter in 2 min. goudbruin. Halveer de knoflook. Pureer in de keukenmachine de kikkererwten met 2-3 el vocht uit het blikje, het sesamzaad, de knoflook, yoghurt en 1 el olie tot een gladde crème.
2. Voeg het citroensap toe en pureer nogmaals kort. Breng de hummus op smaak met de suiker, cayennepeper, peper en zout.
3. Schep over in een ondiepe kom en vorm met een theelepel een spiraal. Bedruppel met de rest van de olie en bestrooi met de paprikapoeder. Snijd de peterselie fijn en strooi over de hummus.
