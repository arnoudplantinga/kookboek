## Ingrediënten

- 4 personen  
- 1 komkommer
- 2 el olijfolie extra vierge
- 150 g volle yoghurt (Grieks)
- 1 teen knoflook
- 1 tl gedroogde munt
- 1 tl citroensap

## Bereiden

1. Halveer de komkommer in de lengte en schraap met een theelepel de zaadjes eruit.
2. Rasp de komkommer grof, bestrooi met een mespunt zout en laat 5 min. in een vergiet uitlekken.
3. Dep de komkommer droog en schep om met de olie en de yoghurt. Pers de knoflook erboven. Breng op smaak met de munt, citroensap, peper en zout.
