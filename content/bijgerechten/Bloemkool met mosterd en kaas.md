
## Ingrediënten

- 1 grote bloemkool, in roosjes van ca. 4 cm verdeeld (700 g)
- 30 g boter
- 1 kleine ui, in fijne blokjes (120 g)
- 1½ theelepel komijnzaad
- 1 theelepel middelscherp kerriepoeder
- 1 theelepel mosterdpoeder
- 2 groene chilipepers, zaadjes verwijderd, in fijne blokjes
- ¾ theelepel zwart mosterdzaad
- 200 ml slagroom
- 120 g rijpe cheddar of belegen goudse, grof geraspt
- 15 g vers wittebroodkruim
- 5 g peterselie, fijngesneden

## Bereiden

Verhit de oven tot 180 °C.

Stoom de bloemkoolroosjes 5 minuten boven kokend water tot ze beetgaar zijn. Haal de bloemkool uit de pan en laat hem iets afkoelen.

Doe de boter in een ovenvaste braadpan van 24 cm doorsnee of een vuurvaste ovenschaal van eenzelfde formaat en zet hem op halfhoog vuur. Laat de ui hierin in 8 minuten gaar en goudbruin worden. Voeg komijnzaad, kerriepoeder, mosterdpoeder en chilipepers toe en laat alles af en toe roerend 4 minuten smoren. Warm het mosterdzaad een minuut mee en giet de room erbij. Doe er 100 gram kaas en ½ theelepel zout bij en laat de saus 2-3 minuten pruttelen tot hij iets gebonden is. Roer heel rustig de bloemkool door de saus, laat nog een minuut staan en haal de pan van het vuur.

Doe de overgebleven 20 gram kaas in een kom en meng het broodkruim en de peterselie erdoor. Strooi het mengsel over de bloemkoolschotel. Veeg de bovenrand van de pan aan de binnenkant met een spatel of doek schoon - zodat eventuele restanten niet gaan verbranden - en zet hem in de oven. Bak het gerecht 8 minuten tot de saus borrelt en de bloemkool heel heet is. Schakel de ovengrill op de hoogste stand en zet de pan er 4 minuten onder tot de bovenlaag goudbruin en krokant is. Let goed op, hij mag niet verbranden. Haal de pan uit de oven en laat de inhoud iets afkoelen - een minuut of vijf - voor u de bloemkool opdient.

## Opmerking

'Dit is het ultieme comfortfood, dat heerlijk is met gebraden kip, gebakken worstjes of een biefstuk uit de koekenpan. Vegetarische opties zijn ook erg lekker: geef er wat voedzame zilvervliesrijst bij, of een salade met een schep yoghurt of een partje limoen. U kunt het een dag van tevoren voorbereiden tot het klaar is om gebakken te worden en het in de koelkast bewaren.' - Yotam Ottolenghi

