
## Ingrediënten
- 75 ml water
- 75 ml sojasaus
- 1 eetlepel donkere basterdsuiker
- 1 teentje knoflook
- 1 theelepel maïzena
- Scheutje sesamolie

## Bereiding
 Verwarm de olie in een pannetje. Voeg het water en de sojasaus toe en de basterdsuiker. Laat deze oplossen. Snijd het teentje knoflook doormidden en voeg toe aan de saus. Breng het mengsel aan de kook. Verwijder de knoflook. Los de maizena op in een paar druppels water en giet bij de saus en roer direct met een garde goed door elkaar. Laat nog een paar minuten indikken en klaar is je teriyakisaus. Bestrooi eventueel met wat sesamzaadjes.

