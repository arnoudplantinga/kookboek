
## Ingrediënten

- 7 el milde olijfolie
- 800 g zoete-aardappelblokjes
- 1 tl cajunkruiden
- 2 eetrijpe avocado's
- 15 g koriander
- 1 el azijn
- 2 el water
- 4 eieren
- 300 g maiskorrels crispy
- 400 g zwarte bonen
- 300 g gesneden rodekool

### Salsa

- Tomaten
- Limoen
- Ui
- Knoflook
- Koriander
- (Evt. rode peper)

## Bereiden

1. Verhit 4/7 van de olie in een hapjespan of grote koekenpan en bak de aardappelblokjes, cajunkruiden en eventueel zout in 15 min. op middelhoog vuur goudbruin en gaar. Schep regelmatig om.
2. Snijd ondertussen de avocado’s overlangs doormidden, verwijder de pit en schep met een lepel het vruchtvlees uit de schil. Snijd de koriander fijn. Doe de helft van de avocado met de helft van de koriander, 2/7 van de olie, de azijn en het water in een hoge beker en pureer met de staafmixer tot een gladde dressing. Breng op smaak met peper en eventueel zout. Snijd de andere helft van de avocado in de lengte in plakken.
3. Verhit ondertussen de olie in een koekenpan met antiaanbaklaag en bak de eieren.
4. Laat ondertussen de mais en bonen uitlekken in een vergiet. Spoel onder koud stromend water en meng de mais, bonen en de rest van de koriander met elkaar. Verdeel de zoete-aardappelblokjes, rodekool, avocado, mais en zwarte bonen in componenten over 4 diepe borden. Verdeel de gebakken eieren erover en serveer de avocadodressing erbij.
