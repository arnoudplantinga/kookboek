ingrediënten
------------

-   500 g spruitjes
-   30 g boter
-   3 theelepels grove mosterd
-   2 theelepels honing

bereiden
--------

Snijd de onderkant van de spruitjes en verwijder losse blaadjes. Maak
een kleine insnede aan de onderkant, doe de spruitjes in een ruime
stoompan en leg het deksel erop. Stoom de spruitjes 15 minuten, of tot
ze gaar zijn. Laat ze schrikken onder koud water om het kookproces te
stoppen.\
\
Doe boter, mosterd en honing in een steelpan op laag vuur en roer om de
boter te laten smelten. Voeg de spruitjes toe en schep om tot alles goed
is vermengd en doorgewarmd. Schep ze in een schaal en serveer meteen.

 
