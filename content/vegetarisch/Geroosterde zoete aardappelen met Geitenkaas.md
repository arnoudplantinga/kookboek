
## Ingrediënten: 

- 2-3 grote zoete aardappelen (oranje van binnen!), schoon en gewassen 
- 100gr zachte geitenkaas 
- handje vol verse bieslook, bosui of de groene deel (ja!) van de prei, gewassen en fijn gesneden/geknipt 
- 8-10 kleine zoete tomaatjes, gewassen en dun gesneden 
- 4-5 el balsamico azijn 
- 2-3 tl dadelstroop (of honing, of Agavesiroop, of als het echt moet, bruine rietsuiker) 
- zout en gemalen zwarte peper 
- olijfolie om de zoete aardappelen te borstellen

## Bereiding:

1. Verwarm de oven voor op 220°C.
2. Snij de zoete aardappelen in lange stukken, min of meer gelijk in dikte
3. Bestrijk de aardappelenparten in olie gemengd met zout en peper
4. Leg de parten op een ovenplaat, eventueel bedekt met Bakpapier. Schuif hem in het midden van de opgewarmde oven en laat het 20-25 minuten bakken/roosteren. Totdat de zoete aardappelen gaar zijn maar niet te zacht. 
5. ca 5 voor het serveren breng de balsamico met de suikervervanger aan de kook. Laat hem op een laag vuurtje koken totdat het dikker wordt. Het duurt ongeveer 5 minuten. Let op dat zodra het afkoelt wordt het nog dikker. Je moet daarom niet te lang wachten.
6. Leg de geroosterde aardappelen op een mooi serveerbord. Voeg de tomatenstukjes en de verkruimelde geitenkaas toe.
7. Schenk ten slotte de dikke balsamico boven de groente en kaas. Garneer het gerecht met  de verse groene kruiden.
Serveer op kamertemperatuur.
