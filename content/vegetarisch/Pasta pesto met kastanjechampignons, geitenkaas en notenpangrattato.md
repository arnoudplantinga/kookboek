## Ingrediënten 2 personen

- 25 g ongezouten amandelen
- 1 el milde olijfolie
- 50 g panko
- 1 tl gedroogde tijm
- 1 teen knoflook
- 1 biologische ui
- 250 g kastanjechampignon
- 100 g babyspinazie
- 250 g verse pappardelle all'uovo
- 1 salade-uitje
- 62½ g koelverse pestosaus verde
- 75 g geitenkaas blanc 50+
- ½ citroen

## Bereiding

1. Hak de amandelen grof. Verhit de helft van de olie in een koekenpan en bak de panko, tijm en amandelen in 3 min. op middelhoog vuur goudbruin en krokant. Schep regelmatig om. Maak ondertussen de knoflook schoon en snijd fijn. Voeg toe aan de panko en bak nog 2 min. mee (pangrattato). Neem van het vuur.

2. Snipper ondertussen de ui. Veeg de champignons schoon met keukenpapier en snijd in kwarten. Verhit de rest van de olie in de hapjespan en bak de ui en champignons met peper 5 min. op middelhoog vuur. Voeg de spinazie in delen toe en laat slinken.

3. Kook ondertussen de pasta in 4 min. beetgaar. Giet af.

4. Snijd de salade-ui in dunne ringetjes en schep samen met de pestosaus en pasta door de groente. Verdeel over borden, verdeel de geitenkaas in stukken erover en bestrooi met de notenpangrattato. Snijd de halve citroen in parten en serveer erbij.

## Variatietip

Besmeer geroosterd brood met de rest van de pesto en verdeel er wat tomaatjes of mozzarella over.

Pangrattato is een Italiaans mengsel met krokant gebakken broodkruim als basis. Heb je pangrattato over? Strooi het dan de volgende dag over een salade of gekookte groente.
