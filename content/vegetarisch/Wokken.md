## 1. Drop 2 tablespoons oil into a hot wok
- Canola oil
- Coconut oil
- Olive oil
- Toasted sesame oil
- Vegetable oil

## 2.Trim and finely chop your aromatics and add them to the pan
- Garlic
- Ginger
- Red chili
- Shallots
- Scallions

## 3.Trim and finely slice the vegetables and add them to the pan (¾ lb total veggies will serve 4 people)
- Asparagus
- Baby corn
- Bean sprouts
- Bell peppers
- Bok choy
- Broccoli
- Celery
- Mushrooms
- Onion
- Snow peas
- Spinach
- Sugar snap peas
- Zucchini

## 4.Prepare your noodles following the package directions (they might need cooking before they go into the wok) and fold them into the vegetables (12 oz noodles will serve 4 people)
- Glass noodles
- Rice noodles
- Rice vermicelli
- Soba noodles
- Udon noodles
- Whole wheat noodles

## 5.Drizzle your sauce over the vegetables and stir everything together
- Basic Stir-fry
- Orange & Ginger
- Black Pepper
- Sweet & Sour
- Hoisin
- Soy
- Teriyaki

## 6.Season your stir-fry and transfer to plates
- Lemon
- Lime
- Salt

## 7.Finish off your stir-fry with the garnish of your choice
- Cashews
- Cilantro leaves
- Hot sauce
- Peanuts
- Scallions, chopped
- Sesame seeds

# Sauce

## BASIC STIR-FRY

- 3 garlic cloves
- 1 tbsp brown sugar
- 2 tsp cornstarch
- 7 tbsp vegetable stock
- 3 tbsp soy sauce
- 1 tbsp rice vinegar

Peel and finely chop the garlic | Put all the ingredients for your sauce into a measuring cup and mix together with a fork

## SWEET & SOUR

- 1 tbsp brown sugar
- 2 tsp cornstarch
- ½ cup vegetable stock
- 2 tbsp ketchup
- 1 tbsp soy sauce
- 1 tbsp rice vinegar

Put all the ingredients for your sauce into a measuring cup and mix together with a fork

## ORANGE & GINGER

- 1¼-inch piece fresh ginger
- 2 tsp cornstarch
- 3 tbsp soy sauce
- 1 tbsp rice wine vinegar
- juice of 1 large orange

Peel the ginger by scraping off the skin with a spoon and finely chop | Put all the ingredients for your sauce into a measuring cup and mix together with a fork

## BLACK PEPPER

- 7 tbsp vegetable stock
- 1 tbsp cornstarch
- 2 tbsp water
- 1 tsp brown sugar
- 1 tsp black pepper
- 3 tbsp soy sauce
- 2 tsp rice vinegar

Put all the ingredients for your sauce into a measuring cup and mix together with a fork
