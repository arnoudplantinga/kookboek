Ingrediënten
------------

-   50 g boter
-   1 ui, gesnipperd
-   2 appels, geschild en in blokjes
-   800 g rodekool, fijngesneden
-   150 ml sinaasappelsap
-   2 eetlepels bruine basterdsuiker
-   1 eetlepel kaneel
-   2 kruidnagels
-   2 laurierblaadjes
-   100 g rozijnen





Bereiden
--------

Smelt de helft van de boter in een ruime braadpan en fruit de ui en
appel 4 minuten. Schep de rodekool erdoor en schenk het sinaasappelsap
erbij. Voeg de suiker, kaneel, kruidnagels, laurier en rozijnen toe en
laat de rodekool met het deksel op de pan op laag vuur in 30 minuten
gaar stoven. Schep de rodekool tijdens het stoven een paar keer om.\
\
Verwijder de kruidnagels en laurier en roer de rest van de boter door de
rodekool. Serveer het gerecht in een grote voorverwarmde schaal.



 


