---
tags: []
---

- 4 personen
- 250 kcal voedingswaarden
- 20 min. bereiden
- 40 min. oventijd

## Ingrediënten

4 personen:

- 0.05 g saffraandraadjes (zakje)
- 3 el heet water
- 1 teen knoflook
- 150 g Griekse yoghurt
- 5 el olijfolie
- 2 el citroensap
- 2 aubergines
- 2 el geroosterde pijnboompitten (bakje 50 g, koelvers)
- 4 el granaatappelpitjes (bakje 120 g)
- Evt. Za'atar

## Bereiding
1. Laat voor de saus de saffraan 5 min. trekken in een kommetje met het hete water. Snijd de knoflook fijn.
2. Doe de saffraan en knoflook in een schaal met daarin de yoghurt, de helft van de olijfolie, het citroensap en nog wat zout. Klop alles tot een gladde, goudgele saus. Proef of er nog wat zout bij moet en zet de saus afgedekt maximaal 3 dagen in de koelkast.
2. Verwarm de oven voor op 200 °C.
2. Halveer de aubergines in de lengte en leg ze op een met bakpapier beklede bakplaat. Snijd het vruchtvlees kruislings in, maar zorg dat het vel heel blijft. Besprenkel de helften royaal met de rest van de olie en bestrooi ze met peper en eventueel zout. Rooster ze ca. 40 min. in de oven tot ze goudbruin zijn. Laat ze afkoelen.
2. Leg de auberginehelften op een platte schaal. Verdeel de saffraanyoghurt erover.
2. Bestrooi met de pijnboom- en granaatappelpitten en evt. za'atar.

**Combinatietip:**
Wil je er een hoofdgerecht van maken? Serveer er dan couscous bij.

**Bewaartip:**
Je kunt de aubergine tot en met het roosteren maximaal 3 dagen van tevoren maken. Bewaar afgedekt in de koelkast. Laat voor serveren op kamertemperatuur komen en ga verder met het recept.
