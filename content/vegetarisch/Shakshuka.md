Servings: 4-6
Prep Time: 10 Minutes
Cook Time: 40 Minutes
Total Time: 50 Minutes

## INGREDIENTS

- 2 tablespoons extra-virgin olive oil
- 2 small yellow onions (or 1 medium), peeled and diced
- 3 cloves garlic, peeled and roughly chopped
- 1 bell pepper (red, orange or yellow), diced
- 3/4 teaspoon smoked paprika
- 1 teaspoon ground cumin
- 1 teaspoon ground coriander
- 1/4 teaspoon crushed red pepper flakes
- 1-1/2 teaspoons salt, divided
- 1 teaspoon sugar
- 1 28-oz can diced tomatoes, with juices
- 2 cups finely chopped greens, such as Swiss chard, kale, or spinach, tough ribs removed, gently packed
- 1/2 cup heavy cream
- 3 ounces feta cheese, crumbled
- 6 eggs
- Handful chopped cilantro

## INSTRUCTIONS

1. In a large skillet, heat the oil over medium heat. Add the onions, garlic and bell pepper and cook, uncovered, for about 8 minutes, until softened. Do not brown; reduce the heat if necessary.
1. Add the smoked paprika, cumin, coriander, red pepper flakes, 1-1/4 teaspoons of the salt, sugar, and tomatoes. Cook, uncovered, stirring occasionally, for about 10 minutes, or until the sauce is slightly thickened.
1. Add the chopped greens and heavy cream to the sauce, and continue cooking, uncovered, stirring occasionally, until the greens are soft and wilted, about 10 minutes more. While the greens are cooking, set an oven rack in the top position and preheat the broiler.
1. Turn off (or remove the pan from) the heat. Using a spoon, make 6 wells/indentations in the sauce. Carefully crack an egg into each well, then spoon a bit of the sauce over each of the egg whites (this will contain them and also help them cook a bit faster than the yolks), being careful not to disturb the yolks. Sprinkle the eggs with the remaining 1/4 teaspoon salt, then sprinkle the feta around the eggs. Set the pan on the stove over low heat and cover with a lid. Cook for 5-7 minutes, until the egg whites are mostly set but still translucent on top (check frequently towards the end as cook time can vary). Remove the lid, transfer the pan to the oven, and broil until the eggs are cooked to your liking, 1 minute or less for runny yolks (see note below on cooking eggs further). Remove the pan from the oven and sprinkle the cilantro over top. Serve hot with bread.

Note: You can cook the dish entirely on the stovetop for just a few minutes longer (without using the broiler) if you like your egg yolks cooked through. If you like your eggs runny, however, I find that the stovetop-to-broiler method is the only surefire way to cook the egg whites thoroughly without overcooking the yolks.

Make Ahead: The tomato sauce can be made up to 2 days ahead of time and refrigerated, or frozen for up to 2 months. Reheat the sauce on the stovetop and proceed with the recipe when ready to serve.
