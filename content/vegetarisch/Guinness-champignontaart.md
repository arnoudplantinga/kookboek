SERVES 4–6

Oh my goodness, this take on a pub classic is so so good. It’s a hug in a dish! The mushroom is rich and meaty and the Guinness adds a dark umami flavor. It’s one for those winter nights after a long day in the cold (or at work!). Serve with Minted Mushy Peas.

## Ingredients

- 1½ lb cremini mushrooms
- 3 tbsp olive oil
- 4 onions
- 6 garlic cloves
- 3 sprigs fresh rosemary, plus extra to decorate
- 3 sprigs fresh thyme
- 1 tbsp light brown sugar
- 1¼ cups Guinness or other stout or brown ale
- 2½ tbsp all-purpose flour, plus extra for dusting
- 1–2 tbsp Dijon mustard
- 5 tsp dark soy sauce
- 1 lb ready-made dairy-free puff pastry
- 2 tbsp dairy-free margarine
- salt and black pepper

## Instructions

**Preheat oven to 180°C | Line a rimmed baking sheet | Large frying pan over medium heat | 20 - 22 cm deep-dish pie plate | Rolling pin (or use a clean, dry wine bottle) | Pastry brush**

1. Quarter the mushrooms and spread them over the lined baking sheet | Drizzle with 1 tablespoon of the oil, season lightly, and roast in the preheated oven for 15 minutes | When they’re ready, remove and set aside, reserving any juices
1. Meanwhile, add the remaining 2 tablespoons of oil to the frying pan | Peel and slice the onions | Peel and finely chop the garlic | Add to the pan and cook for 10 minutes, stirring occasionally, until softened | Reduce the temperature to medium-low
1. Remove the leaves from the rosemary and thyme by running your thumb and forefinger from the top to the base of the stems (the leaves should easily come away) and finely chop, discarding the stems | Add to the pan along with the sugar and cook for 10 more minutes, until the onions are golden
1. Pour the ale into the pan, bring to a simmer, and cook for 10 more minutes so the liquid reduces | Reduce the heat to low and add the mushrooms and any juices in the baking sheet | Add the flour, mustard, and soy sauce and simmer gently for 15–20 minutes, stirring regularly | Taste and adjust the seasoning, adding more salt, pepper, mustard, or soy sauce if you like | Leave to cool slightly, then spoon the mushroom mixture into the pie plate
1. Lightly dust a work surface with flour and roll out the pastry until it is large enough to cover the top of the pie plate | Brush the rim of the dish with water and lay the pastry over the top | Cut off the excess pastry and crimp the edges of the pastry either by pinching it between your finger and thumb all the way round, or by pressing it against the dish with the back of a fork
1. Melt the dairy-free margarine in the microwave and brush it all over the pastry | Use a small sharp knife to cut a little cross in the center of the pastry so that steam can escape | Top with a few rosemary sprigs to make it look fancy | Bake in the preheated oven for 30–35 minutes, until the pastry is golden brown, remove, and serve hot
