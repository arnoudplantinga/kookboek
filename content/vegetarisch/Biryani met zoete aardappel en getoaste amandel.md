## Ingrediënten

Voor 4 personen

- 400 gram basmatirijst, gekookt en gekoeld
- 1 grote witte ui, fijngesnipperd
- 1 teen knoflook, fijngehakt
- 1 duimlengte gember, fijngehakt
- 3 tomaten, in blokjes

De groenten marineren

- 4 eetlepels Griekse yoghurt
- 1/2 eetlepel gemalen kurkuma
- 1/2 eetlepel paprikapoeder
- 1 courgette, in fijne plakjes
- 1 zoete aardappel, geschild en in blokjes
- 1 rode paprika, in stukjes

Specerijen

- 2 theelepels mosterdzaadjes
- 4 kardemompeulen
- 1 theelepel korianderzaadjes
- 1 theelepel komijnzaad
- 1 eetlepel tomatenconcentraat
- 1 theelepel knoflookpasta
- 1 theelepel gemberpasta
- 2 Thaise bird’s eye chilipepertjes (rawit), in de lengte gehalveerd
- 1 theelepel gemalen koriander

Voor de laatste stap

- 1 kopje warme melk
- 0,05 gram saffraan
- 2 snufjes garam masala
- Handje verse munt, fijn gesnipperd
- Handje verse koriander, fijn gesnipperd

Optioneel:

- Pittige mangochutney (bijv. van Lekker Bekkie)
- Extra Griekse yoghurt
- Extra koriander
- 1/3 komkommer, in schijfjes

## Bereiding

1. Begin met het marineren van de groenten. Doe daarvoor de Griekse yoghurt samen met de 1/2 eetlepel kurkuma en 1/2 eetlepel paprikapoeder in een kom en roer goed.
1. Doe de blokjes zoete aardappel in een diep bord met een beetje water en zet gedurende 1 minuten in de magnetron. Voeg ze daarna samen met de stukjes courgette en de paprika toe aan de kom en laat minstens 20 minuten staan.
1. Verhit een laagje zonnebloemolie op middelhoog vuur en voeg er de mosterdzaadjes, ui, knoflook, gember, kardemompeulen, komijnzaad en de korianderzaadjes aan toe. Laat kort aanfruiten tot de ui glazig wordt en het mengsel begint te geuren. Zet het vuur desnoods lager als dit te snel gaat.
1. Voeg nu de tomatenstukjes toe aan het geheel en vervolgens het tomatenconcentraat, de knoflook- en de gemberpasta en de Thaise bird’s eye chilipepers. Roer goed en geef het geheel op laag vuur wat tijd om de smaken goed te laten intrekken.
1. Voeg er nu de inhoud van de kom met groenten en yoghurt aan toe en roer goed. Laat 5 minuten meebakken en voeg er een scheutje water aan toe. Laat nog een paar minuten meebakken. Kruid af met een snuf zout.
1. Verwarm een kopje melk en voeg er de saffraan aan toe. Zorg dat de saffraan een beetje oplost.
1. Verdeel de helft van de rijst over de pan en druk het aan met een lepel. Zet het vuur uit. Voeg de helft van de saffraanmelk, een eetlepel verse munt, een eetlepel verse koriander en een snuf garam masala toe aan het geheel. Verdeel nu de resterende helft van de rijst over de pan. Voeg de andere helft van de saffraanmelk, een tweede snuf garam masala en nog eens een eetlepel verse munt en verse koriander toe aan de pan en roer niet!
1. Leg het deksel op de pan en laat nog 5 minuten op laag vuur koken. Zet het vuur uit en laat nog 10 minuten afkoelen.
1. Werk af met een flinke hand getoaste amandel en schep nu de rijst voorzichtig door elkaar.
1. Serveer de vega biryani met mangochutney, extra getoaste amandel, een schep yoghurt en wat extra koriander.
