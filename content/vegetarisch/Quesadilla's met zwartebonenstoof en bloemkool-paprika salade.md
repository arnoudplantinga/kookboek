## Ingrediënten 4 personen

- 1½ paprika
- 250 g bloemkool
- 4 el milde olijfolie
- 1 middelgrote ui
- 1 teen knoflook
- ½ el gerookte-paprikapoeder
- ½ el komijnzaad
- 800 g zwarte bonen in blik
- 400 g tomatenblokjes in blik
- 1 limoen
- 100 g yoghurt Griekse stijl 0%
- 4 tortillawraps
- 80 g Zaanse Hoeve geraspte kaas mild 30+

## Bereiding

1. Verwarm de oven voor op 190 °C. Halveer de paprika’s in de lengte, verwijder de steelaanzet en zaadlijsten en snijd het vruchtvlees in repen van ca. 1 cm. Snijd de bloemkool in roosjes van 2-3 cm.

2. Verdeel de bloemkool en paprika over een met bakpapier beklede bakplaat, besprenkel met de helft van de olie en schep om. Rooster de groenten in ca. 15 min. boven in de oven beetgaar. Haal uit de oven en laat op de bakplaat afkoelen tot gebruik.

3. Snipper ondertussen de ui en snijd de knoflook fijn. Verhit de rest van de olie in een hapjespan en fruit de ui en knoflook 1 min. Voeg de gerookte-paprikapoeder en het komijnzaad toe en bak 1 min. mee. Roer regelmatig. Voeg de zwarte bonen met het vocht en de tomatenblokjes toe, breng aan de kook en zet het vuur laag. Laat ca. 20 min. op laag vuur stoven met de deksel op de pan. Neem de deksel van de pan en stoof nog 10 min. Roer regelmatig door. Breng op smaak met peper en eventueel zout.

4. Boen ondertussen de limoen schoon, rasp de groene schil en pers de vrucht uit. Meng het limoenrasp en -sap met de yoghurt in een mengkom. Schep de geroosterde bloemkool en paprika erdoor. Breng op smaak met peper en eventueel zout.

5. Verhit een koekenpan zonder olie of boter op middelhoog vuur. Leg 1 tortillawrap in de pan, beleg de helft van de tortilla met ¼ van de geraspte kaas en vouw de tortilla dubbel. Bak ca. 4 min., keer halverwege. Herhaal met de rest van de tortillawraps en geraspte kaas. Houd de quesadilla’s warm op een bord onder een schone theedoek.

6. Verdeel de stoof over diepe borden en schep de bloemkool-paprikasalade erover. Snijd de quesadilla’s in 4 punten en serveer erbij.

VariatietipDoe voor een pittige kick een ½ tl chilivlokken bij de yoghurtdressing.
