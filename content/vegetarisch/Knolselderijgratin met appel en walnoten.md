
## Ingrediënten

- 800 g kruimige aardappelen
- 1 knolselderij
- 3 bosuien
- 2 jazz-appels
- 200 g roombrie (60+)
- 200 ml kookvocht
- 1 groentebouillontablet
- 125 g crème fraîche
- 100 g ongebrande walnoten


1. Schil de aardappelen en snijd in gelijke stukken. Doe in een ruime pan en kook in water met eventueel zout in 20 min. gaar. Schil ondertussen de knolselderij, snijd in gelijke stukken en voeg de laatste 10 min. toe aan de aardappelen. Verwarm de oven voor op 200 ºC. Snijd de bosui in ringetjes. Snijd de appels met schil en klokhuis (zonder pitjes) in flinterdunne plakken. Snijd de brie in de lengte in dunne plakken.
2. Giet de aardappel en knolselderij af en vang het kookvocht op. Verkruimel het bouillontablet en los op in het vocht. Schenk terug bij de aardappel en knolselderij, voeg de crème fraîche toe en stamp tot een grove puree. Schep de helft van de bosui erdoor. Verdeel het mengsel over de ovenschaal en verdeel de appel, brie en walnoten erover. Bak ca. 15 min. in het midden van de oven. Bestrooi met de rest van de bosui.
