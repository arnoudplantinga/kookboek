[\
4 personen]()

545
kcal [voedingswaarden](http://www.ah.nl/allerhande/recept/R-R584592/vegetarische-risotto-met-romige-groenten#)

-   25 min. bereiden 

 

#### Ingrediënten

4
personen



[]() []()



 

-   [4 el olijfolie]( "zet ingrediënt op mijn lijst")
-   [300 g risottorijst]( "zet ingrediënt op mijn lijst")
-   [1 liter groentebouillon
    (van tablet)]( "zet ingrediënt op mijn lijst")
-   [250 g gebroken sperziebonen]( "zet ingrediënt op mijn lijst")
-   [1 teen knoflook]( "zet ingrediënt op mijn lijst")
-   [400 g champignonroerbakmix]( "zet ingrediënt op mijn lijst")
-   [175 g Tivall gegrilde stukjes]( "zet ingrediënt op mijn lijst")
-   [75 ml kookroom light]( "zet ingrediënt op mijn lijst")
-   [75 g geraspte jong belegen kaas
    (puur&eerlijk biologisch)]( "zet ingrediënt op mijn lijst")

\

#### Bereiden

1.  Verhit de helft van de olie in een pan met dikke bodem. Voeg de
    rijst toe en bak deze op hoog vuur tot de korrels glanzen. Draai het
    vuur laag en schenk er een flinke scheut bouillon bij. Laat de
    risottorijst dit al roerend opnemen. Blijf om de paar minuten
    bouillon toevoegen tot de rijst beetgaar is. Dit duurt ca. 20 min.
2.  Kook de sperzieboontjes in 5 min. Snijd de knoflook fijn. Verhit de
    rest van de olie in een hapjespan, bak hierin de knoflook 1 min.,
    schep de champignonroerbakmix erbij en roerbak 3 min. Schep de
    gegrilde stukjes erbij en roerbak weer 3 min. Giet de
    sperzieboontjes af en schep ze door het groentemengsel.
3.  Schep de kookroom, 2/3 van de kaas en het roerbakmengsel door
    de risotto. Breng op smaak met peper en zout. Serveer de risotto in
    diepe borden, serveer de rest van de kaas erbij.

 
