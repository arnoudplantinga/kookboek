## Ingrediënten 4 personen

- 300 g	knolselderij geschild en in blokjes van ca. 1 cm
- 50 ml	olijfolie
- 1 tl	koriander gemalen
- 4 el	olijfolie
- 1	ui fijngesneden
- tl	tijm
- 1 mespunt	gerookte paprikapoeder
- 250 g	risottorijst
- 125 ml	witte wijn
- 600 ml	groentebouillon
- 200 g	paddestoelen in stukjes. een voorkeur voor oesterzwammen
- 1 snuifje	korianderzaad gemalen
- 1 blik (265 gram uitlekgewicht)	kikkererwten
- 3 el	sesamzaad
- handje	peterselie vers
- citroensap
- zout en peper naar smaak

## Instructies

Verwarm de oven voor tot 200°(gasoven)/180°(elektrische oven). Bekleed een bakplaat met aluminiumfolie. Doe de knolselderij in een kom met 50 milliliter olijfolie, koriander en peper en zout naar smaak. Roer goed door, zodat alle blokjes ingesmeerd zijn. Verspreid de knolselderij over de bakplaat en rooster ze zo’n 30 minuten.

Stoof intussen de ui met wat zout, tijm en paprikapoeder in 2 eetlepels olijfolie, in een grote pan. Voeg de rijst toe en roerbak tot het glazig wordt. Blus met witte wijn en voeg dan pollepel na pollepel bouillon toe, tot de rijst gaar is.

Rooster ondertussen het sesamzaad in een droge pan tot de zaadjes beginnen te springen, laat ze niet te veel verkleuren. Plet ze met een vijzel, samen met een theelepel grof zeezout. Deze mengeling heet gomasio. Als je geen vijzel hebt kun je de zaadjes in een zakje doen en met een lepel pletten.

Gebruik de sesampan voor de paddenstoelen. Verhit nog 2 eetlepels olie en bak de paddenstoelen in zo’n 5 minuten mooi bruin.

Prak de kikkererwten met een schepje bouillon en een scheut olie tot een puree. Roer de kikkererwtenpuree door de risotto. Hij moet mooi smeuïg zijn. Voeg de knolselderij en de paddenstoelen toe. Breng op smaak met peper, zout, citroensap en olijfolie. Strooi er sesamzaadjes en fijngesneden peterselie op.

