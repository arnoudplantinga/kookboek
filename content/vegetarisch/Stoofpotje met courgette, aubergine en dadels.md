
## Ingrediënten

Voor 4 personen:

- 1 aubergine
- 4 sjalotten
- 1 courgette
- 4 el olijfolie
- 1 kaneelstokje
- 2 tl gemalen komijn (djinten)
- 350 g tomatensaus (tomaten frito)
- 150 ml kraanwater
- 200 g dadels
- 250 g cherrytomaten
- 400 kikkererwten
- 200 g parel couscous
- 50 g amandelenten

## Bereiding

1. Snijd de aubergine in de lengte doormidden en snijd plakken van 2 cm dik. Snijd de sjalotten in kwarten. Snijd de courgette in halve plakken.
2. Verhit de olie in een braadpan en bak de aubergine 4 min. op hoog vuur. Bak de sjalotten 2 min. mee. Voeg de courgette, het kaneelstokje, de komijn, tomatensaus en het water toe. Breng op smaak met peper en zout en laat 10 min. stoven.
2. Halveer de dadels en voeg deze toe. Laat de kikkererwten uitlekken en voeg deze samen met de cherrytomaten toe en stoof nog 10 min.
2. Bereid ondertussen de parelcouscous volgens de aanwijzingen op de verpakking. Proef de stoof en breng indien nodig verder op smaak met peper en zout. Schep in een mooie schaal. Hak de amandelen en strooi deze erover. Serveer de couscous er apart bij.
