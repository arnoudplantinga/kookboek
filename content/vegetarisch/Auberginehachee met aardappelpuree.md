## Ingrediënten 4 personen
- 2 aubergines
- 400 gr gemengde paddenstoelen
- 2 uien
- 4 el zonnebloemolie
- 200 ml rode wijn
- 4 el balsamicoazijn
- 500 ml groentebouillon
- 4 takjes tijm
- 2 laurierblaadjes
- 600 gr aardappelpuree
- 100 gr ontbijtkoek
- 4 takjes peterselie 

Bereidingstijd 30 min

Wachttijd 15 min

Totale tijd 45 min

1. Snijd de aubergine in reepjes van ongeveer 1 cm dik en 4 cm lang. Snijd de champignons van de gemende paddenstoelen in kwarten, snijd de rest van de paddenstoelen in reepjes. Pel de ui en snijd in grove stukken.

2. Verhit de zonnebloemolie in een stoofpan op hoog vuur. Bak de aubergine, champignons en paddenstoelen in 5 min. rondom bruin. Zet het vuur laag, voeg de ui toe en bak 2 min. mee.

3. Blus het geheel af met de rode wijn, balsamico azijn en groentebouillon. Voeg de takjes tijm en laurier toe. Sluit de pan met het deksel en stoof het geheel in 20 min. gaar. Verwarm ondertussen de aardappelpuree.

4. Verkruimel de ontbijtkoek en voeg toe aan de hachee. Kook het geheel nog 2 min. op laag vuur en roer goed door. Kook eventueel langer door zonder deksel voor een dikkere saus. Snijd de peterselie grof.

5. Haal de takjes tijm en het laurierblaadje uit de hachee. Serveer de hachee met aardappelpuree. Garneer het gerecht met peterselie. 
