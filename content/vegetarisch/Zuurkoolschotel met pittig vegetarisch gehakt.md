## Ingrediënten (voor 4-5 personen, ovenschaal 33 x 23 cm):

- 500 g zuurkool
- 2 el olijfolie en beetje extra
- 1 grote ui, gesnipperd
- 2 teentjes knoflook, geperst
- 500 g rundergehakt (of vegetarisch gehakt)
- ¼ tl kruidnagelpoeder
- 2-3 el tomatenketchup*
- 2-3 el ketjap manis*
- 1-1½ tl sambal oelek*
- zwarte peper, liefst vers gemalen
- nootmuskaat, liefst vers geraspt

*  Ik gebruik meestal 3 el ketchup en ketjap en 1½ tl sambal, maar je kunt hier gerust van afwijken en afhankelijk van je eigen smaak er minder van toevoegen.

voor de aardappelpuree:

1300 g kruimige aardappelen (met schil gewogen)
150 g volle melk
40 g roomboter
zwarte peper, liefst vers gemalen
nootmuskaat, liefst vers geraspt
zout

## Variatie tips:

* de volle melk en de roomboter geven de aardappelpuree extra smaak, maar voor een slankere versie kun je eventueel de hoeveelheid roomboter halveren en er iets meer (halfvolle) melk aan toevoegen.

* vervang de aardappelen door zoete aardappelen (mogelijk dat er dan minder melk bij hoeft), of vervang de aardappelpuree door deze bloemkoolpuree. Laat dan de melk en boter van dit recept achterwege en volg het bloemkool recept).

## Bereiding

1. Schil de aardappelen en kook ze gaar. Spoel de zuurkool met water en knijp het vocht er goed uit. Spreid de zuurkool uit in een ingevette ovenschaal. Zet het daarna apart en maak ondertussen het pittige gehakt.
1. Verwarm de oven voor op 190°C (hetelucht).
1. Fruit de ui in 2 el olijfolie gedurende 5 minuten zachtjes gaar. Doe er in de laatste minuut de knoflook bij. Voeg daarna het gehakt toe en bak deze op iets hoger vuur rul en gaar. Breng het tot slot op smaak met kruidnagelpoeder, ketchup, ketjap, sambal, zwarte peper en nootmuskaat.
1. Verdeel het pittige gehakt over de zuurkool en zet de schaal 10-15 minuten in de oven terwijl je de aardappelpuree maakt. Deze stap zorgt ervoor dat de zuurkool 10-15 minuten extra tijd in de oven krijgt. Hierdoor wordt hij heerlijk zacht en bovendien kan hierdoor de smaak van het gehakt langer in de zuurkool trekken.
1. Giet de gare aardappelen af en stamp deze met de pureestamper fijn. Doe er de melk en boter bij en breng de puree op smaak met zout, peper en nootmuskaat. Neem de ovenschotel uit de oven en verdeel de aardappelpuree over het gehakt. Als je van een krokant korstje op de aardappelpuree houdt, strijk de puree dan niet glad maar maak hem juist extra grillig door middel van een vork. Bak de zuurkoolschotel 30 minuten in de hete oven tot de puree goudbruin en krokant is.
