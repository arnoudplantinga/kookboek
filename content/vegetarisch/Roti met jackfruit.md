## Ingrediënten 4 personen

- 2 middelgrote uien
- 1 verse rode peper
- 550 g Fairtrade Original Young Jackfruit in blik
- 2 el zonnebloemolie
- 1½ el curry madras specerijenmelange
- 70 g tomatenpuree
- 500 g voorgekookte krieltjes
- 1 groentebouillontablet
- 400 ml heet water
- 200 g verse sperziebonen
- 400 g bloemkool (diepvries)
- 4 witte scharreleieren
- 2 Surinaamse rotivellen

## Bereiding

1. Snijd de uien in dunne halve ringen. Snijd het steeltje van de rode peper. Halveer de peper in de lengte en verwijder met een scherp mesje de zaadlijsten. Spoel het jackfruit af in een zeef onder koud stromend water en laat uitlekken. Trek de grotere stukken jackfruit iets uit elkaar.

2. Verhit de olie in een hapjespan en bak de ui, rode peper, curry madras, tomatenpuree en een snuf zout 2 min. op middelhoog vuur.

3. Voeg de krieltjes en het jackfruit toe en bak 3 min. mee. Voeg het bouillonblokje en het water toe en breng al roerend aan de kook. Stoof de curry met de deksel op de pan op laag vuur in 30 min. gaar, schep regelmatig om.

4. Verwijder ondertussen de steelaanzet van de sperziebonen en halveer de bonen. Verdeel de bloemkool in roosjes van ca. 3 cm. Voeg de bloemkool en sperziebonen na 10 min. toe aan de stoof. Proef na 15 min. hoe pittig de curry is en verwijder eventueel de peper.

5. Kook ondertussen de eieren 6 min., laat schrikken onder koud stromend water, pel en halveer ze. Verwarm de rotivellen volgens de aanwijzingen op de verpakking. Serveer de jackfruit-aardappelcurry met de eieren en rotivellen.
