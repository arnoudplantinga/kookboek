## Ingredienten

- 6 à 7 plakjes hartige taartdeeg (diepvries)
- 4 eieren
- 125 ml. sojaroom
- 1 bol mozzarella
- 100 gram aubergine
- 6 cherrytomaten
- 4 zongedroogde tomaten
- 10 zwarte olijven
- 2 el. rode pesto
- 1 tl. Italiaanse kruiden

## Bereiding

1. Verwarm de oven voor op 200 graden Celsius. Vet een quichevorm in en bekleed deze met de plakjes hartige taartdeeg. Zet nog even apart.
2. Snij de aubergine in kleine blokjes. Verwarm een koekenpan met wat olijfolie op middelhoog vuur. Bak de aubergine enkele minuten totdat deze bruin begint te kleuren. Haal dan van het vuur en laat iets afkoelen.
3. De zongedroogde tomaat snij je in reepjes en de olijven in plakjes. Halveer de cherrytomaten en snij of scheur de mozzarella in repen. 
4. Kluts de eieren in een ruime kom samen met de sojaroom en pesto. Roer de aubergines, zongedroogde tomaat, olijven, Italiaanse kruiden en evt. naar eigen smaak zout en peper erdoorheen. 
5. Giet het eimengsel in de hartige taartvorm. Verdeel de mozzarella en gehalveerde cherrytomaten (met de snijkant naar boven) eroverheen. Plaats voor 30 minuten in het midden van de oven. 
6. Laat de quiche een paar minuten afkoelen voordat je hem aansnijdt. 
