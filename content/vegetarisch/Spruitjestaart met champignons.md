#### Ingrediënten

-   [6 plakjes roomboterbladerdeeg (pak 10
    plakjes, diepvries)]( "zet ingrediënt op mijn lijst")
-   [500 g spruitjes]( "zet ingrediënt op mijn lijst")
-   [1 ui]( "zet ingrediënt op mijn lijst")
-   [250 g kastanjechampignons (bakje)]( "zet ingrediënt op mijn lijst")
-   [25 g boter]( "zet ingrediënt op mijn lijst")
-   [1 tl gedroogde tijm (potje 15 g)]( "zet ingrediënt op mijn lijst")
-   [150 g zuivelspread
    bieslook light (bakje)]( "zet ingrediënt op mijn lijst")
-   [3 eieren]( "zet ingrediënt op mijn lijst")
- Spekjes

###### Keukenspullen

-   ovenschaal (ca. 2 liter)

#### Bereiden

1.  Verwarm de oven voor op 225 °C. Ontdooi het bladerdeeg. Vet de
    ovenschaal in en bekleed met het bladerdeeg. Prik het deeg hier en
    daar met een vork in.
2.  Halveer de spruitjes. Kook de spruitjes in ruim kokend water 5 min.
    Snijd de ui in partjes. Maak de champignons schoon met keukenpapier
    en snijd in plakjes. Bak de spekjes, fruit daarna 
    de ui glazig. Voeg de champignons en de tijm toe en roerbak 5 min.
    Schep de spruitjes erdoor en breng op smaak met peper en zout. Schep
    het spruitjesmengsel op het bladerdeeg.
3.  Roer de zuivelspread los met de eieren en lepel het mengsel over
    de vulling. Bak de taart in het midden van de oven in 45 min.
    goudbruin en gaar. Breng na 30 min. de oventemperatuur terug naar
    175 °C. Serveer direct.

#### Bereidingstip:

Je kunt dit gerecht van tevoren bereiden. Verwarm de taart op de dag
zelf in 20 min. op in een voorverwarmde oven op 180°C.

 
