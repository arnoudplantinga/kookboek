Kooktijd 25 minuten

Calorieën 634 kcal

## Ingrediënten Voor 4 personen:
- 8 eieren
- 4 aardappels, geschild en in stukjes
- 8 eetlepels plantaardige olie (+ extra)
- 2 witte uien
- 2 rode rawit pepers
- 2 groene rawit pepers
- 6 teentjes knoflook
- 2 duimen gember
- 4 tomaten
- 2 eetlepels korianderzaad, gestampt in de vijzel
- 2 eetlepels komijnzaad, gestampt in de vijzel
- 1 theelepel garam masala
- 2 theelepels kurkuma
- 2 theelepels gemalen rode chili
- 500 ml kokosmelk
- 2 eetlepels tomatenpuree
- Zout (naar smaak)
- Suiker (naar smaak)
- Handje koriander (optioneel)

## Bereiding

Breng een pan met ruim water aan de kook en kook de eieren in 8 minuten. Spoel daarna af met koud stromend water en pel de eieren.

Verwarm 8 eetlepels plantaardige olie in een pan en frituur daarin zowel de aardappelstukjes als de eieren, tot ze goudbruin zijn. Dit duurt ongeveer 7 minuten. Schep de eieren en de aardappelstukjes uit de pan en zet apart.

Neem je blender erbij (of ga te werk met je vijzel) en blend de uien samen met de pepers, de knoflook, de gember en de tomaten tot een pasta.

Gebruik de pan waarin je eerder de eieren hebt gebakken om het koriander- en komijnzaad, de garam masala, de kurkuma en de gemalen chili aan te fruiten tot de aroma’s vrijkomen. Voeg daarna de inhoud van je blender toe en roer alles door elkaar. Draai het vuur lager en laat alles gedurende een paar minuten pruttelen. Voeg de kokosmelk en de tomatenpuree toe. Proef en voeg naar smaak extra zout en wat suiker toe.

Voeg de aardappelstukjes en de eieren toe aan de pan en roer alles goed door. Laat nog 5 minuten pruttelen. Maak de eiercurry af met verse koriander en serveer met paratha’s of rijst.

