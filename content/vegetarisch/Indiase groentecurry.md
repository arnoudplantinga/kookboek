 

#### Ingrediënten 4 personen

 

-   [2 rode uien]( "zet ingrediënt op mijn lijst")
-   [2 el zonnebloemolie]( "zet ingrediënt op mijn lijst")
-   [3 el kruidenpasta voor
    tikka massala (Patak's)]( "zet ingrediënt op mijn lijst")
-   [400
    g bloemkoolroosjes (diepvries)]( "zet ingrediënt op mijn lijst")
-   [400 g pompoenstukjes (zak)]( "zet ingrediënt op mijn lijst")
-   [400 g kikkererwten (blik 400 g)]( "zet ingrediënt op mijn lijst")
-   [400 g gepelde tomaten (blik
    400 g)]( "zet ingrediënt op mijn lijst")
-   [2 naanbroden (pak 280 g, Patak's)]( "zet ingrediënt op mijn lijst")
-   [170 g Griekse yoghurt]( "zet ingrediënt op mijn lijst")
-  Maïzena

#### Bereiden

 

1.  Snijd de ui in dunne halve ringen. Verhit de olie in een hapjespan
    en fruit de ui 3 min. op laag vuur.Voeg de kruidenpasta toe en fruit
    2 min. mee. Voeg de bloemkool en pompoen toe en fruit 2 min. mee.
2.  Spoel de kikkererwten in een vergiet af met koud water, laat
    uitlekken en voeg toe. Voeg de tomaten toe, breng aan de kook en
    laat op laag vuur met de deksel op de pan 10 min. garen.
3.  Bereid ondertussen de naans volgens de aanwijzingen op
    de verpakking.
4.  Neem de pan van het vuur en roer de helft van de yoghurt door
    de curry. Serveer met de rest van de yoghurt.

 

#### variatietip:

Vervang voor de variatie de naans eens door 100 g pappadums (Patak's)

 
