## Ingrediënten Voor 4 personen

Voor de groentenstoof:

- 2 eetlepels tomatenpuree
- 60 ml water
- 3 kleine aubergines
- 1 grote witte ui
- 2 grote rijpe vleestomaten
- 3 sivri-pepers (Turkse milde groene pepers)
- 3 tenen geraspte knoflook
- 1 theelepel gemalen komijnzaad
- 1 theelepel korianderzaad
- 1/2 theelepel chilipoeder
- 1 theelepel kurkuma
- 1 theelepel zout
- 1 theelepel suiker
- Zonnebloemolie

Voor de yoghurtdip:

- 150 gram volle Griekse yoghurt
- 3 theelepels gedroogde munt
- Snufje zout
- Snufje peper
- 2 teentjes geraspte knoflook

Extra’s:

- Platbrood voor 4 personen

## Bereidingswijze

1. Mix voor de yoghurtdip de Griekse yoghurt, geraspte knoflook en gedroogde munt goed door elkaar. Breng op smaak met zout en peper.
2. Snijd de aubergine in dunne lange plakken. Bestrooi met zout en zet opzij.
3. Meng de tomatenpuree met het water tot de puree helemaal is opgelost in het water.
4. Snijd de witte ui en tomaten in dunne plakken en halveer de sivri pepers.
5. Knijp de aubergineplakken uit zodat het meeste vocht uit de groente is.
6. Verhit in een pan een flinke laag zonnebloemolie en bak de plakken aubergine tot ze beginnen te kleuren. Laat uitlekken op keukenpapier.
7. Verhit twee eetlepels zonnebloemolie in een braadpan en voeg de geraspte knoflook toe.
8. Voeg zodra de knoflook begint te kleuren het tomatenwater toe en bak al roerend tot deze donker begint te kleuren.
9. Voeg het komijnpoeder, het korianderzaad, de kurkuma de suiker, het zout en het chilipoeder toe. Laat een paar minuten meebakken.
10. Stapel je groenten in lagen dakpansgewijs over elkaar heen. Begin met een laag ui, dan de tomaat, vervolgens de aubergine en top het af met de overgebleven ui.
11. Leg de sivri pepers er bovenop en doe de deksel op de pan. Laat op zacht vuur circa 30 minuten stoven.
12. Pak voor het serveren van de borani banjan een groot plat bord. Verdeel een gedeelte van de yogurthdip over het bord en schep de borani banjan er bovenop en bestrooi met wat peterselie.
13. Serveer met Afghaans platbrood (of ander platbrood).
