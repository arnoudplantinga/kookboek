Bereidingstijd 20 min

Wachttijd 10 min

Totale tijd 30 min

## Ingrediënten 2 personen

- 1 witte ui
- 2 teentjes knoflook
- 2 el zonnebloemolie
- 1 tl kurkuma
- 1 tl gemalen gember
- 1 tl garam masala
- 200 ml kokosmelk
- 200 gr tomatenblokjes
- 120 gr ongekookte rode linzen
- 150 gr witte rijst
- 200 gr babyspinazie
- 2 takjes koriander

## Bereiding

1. Pel en snipper de ui. Pel de knoflook en hak fijn. Verhit de zonnebloemolie in een hapjespan op middelhoog vuur. Fruit de ui en knoflook 2 min. aan. Voeg de kurkuma, gember en garam masala toe en laat 1 min. meebakken.
2. Voeg de kokosmelk en tomatenblokjes toe en breng het geheel aan de kook. Spoel ondertussen de linzen onder stromend water. Voeg de linzen toe en laat het geheel 20 min. doorkoken op laag vuur. Roer regelmatig door.
3. Bereid ondertussen de rijst volgens de aanwijzingen op de verpakking.
4. Serveer de dahl samen met de rijst en spinazie. Pluk de takjes koriander en strooi vlak voor serveren over het gerecht.
