If you’re a fan of sticky, sweet, pan-Asian cuisine you will love this dish (seriously, it’s bangin’!). It’s quick and easy to put together and guaranteed to impress. Serve with freshly cooked rice and chopsticks.

## Ingredients (2p)

- ½ lb shiitake mushrooms
- 3 tbsp cornstarch
- 2 tbsp peanut oil
- 2 garlic cloves
- 1¼-inch piece fresh ginger
- ½ tsp water
- 1 tbsp toasted sesame oil
- 2 tbsp light brown sugar
- ¼ cup dark soy sauce
- 2 tbsp rice vinegar
- 1 tsp sriracha sauce, or to taste
- 1 scallion, to serve
- 2 cups cooked basmati rice (store-bought or see here), to serve
- 1 tsp sesame seeds, to serve

## Instructions

1. Thickly slice the mushrooms and put them in a bowl | Sprinkle 2 tablespoons of the cornstarch over the top and toss everything together with your hands, making sure the mushrooms are well covered | Pour the peanut oil into the wok or pan and get it nice and hot | Tip in the mushrooms and fry for 4–6 minutes, until cooked through and slightly crisp on the outside | Transfer the mushrooms to a bowl and set aside
1. Peel and finely chop the garlic and ginger | Spoon the remaining 1 tablespoon cornstarch into a small dish and mix it together with the water | Wipe out the wok with paper towels and put it back over low heat | Pour in the sesame oil | Add the chopped garlic and ginger and cook until you release the aromas and they’re bubbling in the oil, about 1 minute | Sprinkle in the sugar and stir until caramelized, about 2 minutes more | Increase the heat slightly and pour in the cornstarch mixture, soy sauce, and rice vinegar, then stir for another minute until the sauce has thickened slightly | Add the sriracha and stir it into the sauce | Tip the cooked mushrooms back into the pan and stir to warm through and completely cover in the sauce, 1–2 minutes longer
1. Finely slice the scallion | Serve the chewy mushrooms over hot basmati rice, garnished with the sliced scallion and sprinkled with sesame seeds
