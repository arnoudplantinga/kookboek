## Ingrediënten 4 personen

- 700 g AH Biologisch kikkererwten in pot
- 600 g zoete aardappel
- 1 aubergine
- 1 tl milde paprikapoeder
- 2 tl gedroogde oregano
- 2 tl komijnzaad
- 4 el arachideolie
- 1 mespunt zout
- 3 tenen knoflook
- 450 g halloumi kaas
- 240 g groene olijven met piment
- 150 g yoghurt Griekse stijl

## Bereiding

1. Verwarm de oven voor op 200 °C. Doe de kikkererwten in een vergiet en spoel af onder koud stromend water. Laat goed uitlekken. Boen de zoete aardappel schoon en snijd in stukken van 2 cm. Snijd ook de aubergine in stukken van 2 cm.

2. Meng de paprikapoeder, oregano, het komijnzaad, de olie en zout naar smaak. Meng de kruidenolie met de kikkererwten, zoete aardappel, ongepelde tenen knoflook en aubergine en verdeel over een met bakpapier beklede bakplaat. Rooster ca. 30 min. in het midden van de oven.

3. Snijd ondertussen de halloumi in de breedte in plakjes van een ½ cm. Laat de olijven uitlekken.

4. Neem na 15 min. de bakplaat uit de oven en schep de traybake om met de olijven. Verdeel de halloumi erover en rooster nog 15 min. Neem de tenen knoflook van de bakplaat. Verwijder de velletjes, prak fijn en meng door de yoghurt. Serveer bij de traybake.

Combinatietip: Lekker met platte peterselie
