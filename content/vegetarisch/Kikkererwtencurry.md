## Ingrediënten voor 4-6 personen

- 60 ml olijfolie (+ extra)
- 4 teentjes knoflook, geperst
- 1 grote ui, fijn gehakt
- 1 duim gember, fijn gehakt
- Zout & peper
- 1 1/2 theelepel gemalen kurkuma (+ extra)
- 1 theelepel rode chilivlokken
- 2 grote blikken kikkererwten, uitgelekt
- 2 grote blikken kokosmelk
- 500 ml kippenbouillon
- 200 gram snijbiet (of boerenkool), in hapklare stukjes
- Een paar blaadjes verse munt
- Griekse yoghurt
- Optioneel: pitabrood of flatbread

## Bereiding

Verwarm 60 ml olijfolie in een grote pan op medium vuur. Voeg de knoflook, ui en gember toe en kruid af met zout en peper. Bak een paar minuten, tot de uien beginnen zweten.

Voeg de gemalen kurkuma, de rode chilivlokken, de kikkererwten toe en kruid nogmaals af met zout en peper. Laat 8 à 10 minuten bakken zodat de kikkererwten knapperig worden door de olie. Schep er daarna ongeveer 1/4 van de kikkererwten uit en zet apart – hiermee ga je de curry later garneren.

Gebruik een houten lepel of een pureestamper om de resterende kikkererwten plat te stampen. Hierdoor komt het zetmeel vrij, wat ervoor zal zorgen dat je een mooie, dikke curry krijgt. Voeg de kokosmelk en de bouillon toe en geef het geheel nog eens een snuf zout en peper.

Breng de curry zachtjes aan de kook en schraap regelmatig over de bodem van de pan. Laat zo ongeveer een halfuurtje op een laag vuur staan en roer regelmatig. Je wil een mooie dikke, gladde curry: proef regelmatig en laat nog wat langer pruttelen als je curry nog te lopend is.

Voeg daarna de snijbiet toe en roer goed. Kook nog een paar minuten mee, tot deze zacht is.

Verdeel the stew over kommen en garneer met munt, de kikkererwten die je had bewaard, wat extra rode chilivlokken en een scheutje olijfolie. Serveer met yoghurt en eventueel met flatbread.
