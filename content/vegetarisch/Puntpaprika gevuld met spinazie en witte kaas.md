        4 personen
        350 kcal voedingswaarden
            25 min. bereiden
            20 min. oventijd

-       2 rode uien
-       2 tenen knoflook
-       4 takjes oregano
-       3 el zonnebloemolie
-       &frac12; el kerriepoeder
-       370 ml witte bonen in tomatensaus
-       4 puntpaprika's
-       300 g spinazie
-       200 g feta



1. Verwarm de oven voor op 180 C. Snijd ondertussen de uien in halve ringen. Snijd de knoflook fijn en ris de blaadjes van de takjes oregano.
2. Verhit de olie in een hapjespan en fruit de ui, knoflook, kerrie, peper en eventueel zout 5 min. op laag vuur. Roer regelmatig. Voeg de witte bonen in tomatensaus en oregano toe aan de ui en warm 5 min. mee. Halveer ondertussen de paprika’s in de lengte, maar laat de steelaanzet zitten. Verwijder de zaadlijsten. Leg de paprika’s op een met bakpapier beklede bakplaat.
3. Voeg de spinazie toe aan de bonen en laat al omscheppend iets slinken. Neem de pan van het vuur en breng op smaak met peper. Verdeel het bonenmengsel over de paprika’s en verkruimel de feta erover. Bak ca. 20 min. in het midden van de oven en serveer direct.

![](http://www.ah.nl.kpnis.nl/static/recepten/img_069641_1600x560_JPG.jpg)
