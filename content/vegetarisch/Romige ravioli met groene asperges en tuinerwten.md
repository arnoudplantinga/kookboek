## Ingrediënten voor 4 personen:

- 500 gram ravioli (bijv. met burrata en tomaat)
- 300 gram groene asperges
- 300 gram tuinerwtjes (diepvries), ontdooid
- 250 ml slagroom
- Handje pijnboompitten, geroosterd
- 1 citroen, rasp en sap
- Zout & peper
- Verse basilicum

## Bereiding

Breng ruime pan gezouten water aan de kook. Kook de ravioli al dente en giet af.

Was en snijd de kontjes van de groene asperges. Snijd ze in stukjes van 3 cm.

Verhit in een andere, grote pan een scheut olijfolie en bak de groene asperges op hoog vuur tot ze mooi beetgaar zijn. Draai het vuur lager en voeg de room en de tuinerwten toe. Roer alles door elkaar en laat nog een minuutje bakken op laag vuur.

Voeg de citroenrasp en een flinke snuf zout en peper toe. Roer goed en proef. Breng verder op smaak met zout, peper en/of citroensap. Voeg de pasta toe aan de pan en roer alles door elkaar. Verdeel de getoaste pijnboompitten over de ravioli en maak af met basilicumblaadjes.
