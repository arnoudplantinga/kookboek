## Ingrediënten:  4 personen
-	2 sjalotten
-	70 gram tomatenpuree
-	400 gram kikkererwten uit blik
-	1 theelepel komijn
-	1 theelepel kurkuma
-	 1 theelepel gerookte paprikapoeder
-	1 blik tomaten, in blokjes gesneden
-	200 ml kokosmelk
-	250 gram cherrytomaten
-	200 gram spinazie
-	150 ml kookroom
-	250 gram halloumi
-	1 eetlepel fijngesneden koriander voor de garnering
-	snufje zwarte peper en zout
-	naan-brood en/of zilvervliesrijst

## Bereiding
1.	Snij de sjalotten in dunne ringen. Snij de halloumi vast in blokjes van 1 bij 1 cm. Halveer ook vast de cherrytomaten.
2.	Verwarm de hapjespan of grote koekenpan op middelhoog vuur. Fruit de sjalotten kort aan totdat ze glazig worden.
3.	Voeg de tomatenpuree, de komijn, de kurkuma en het paprikapoeder toe en bak ze een minuut op zacht vuur. 
4.	Voeg dan de tomaten uit blik toe. Vul het blik tot de helft met water en voeg dat toe in de pan. Voeg ook de cherrytomaten en de kokosmelk toe aan het gerecht. Breng het aan de kook, zet vervolgens het vuur lager en laat de tomaten ongeveer 8 minuten pruttelen.
5.	Voeg de blokjes halloumi + kikkererwten toe aan je gerecht en roer het goed door. Roer dan beetje bij beetje de spinazie er doorheen en laat deze steeds slinken.
6.	Voeg de kookroom toe als alle spinazie is geslonken. Laat zachtjes pruttelen en breng de saus op smaak met peper en zout. Wees zuinig met zout vanwege het zout in de halloumi.
7.	Serveer de curry met een stuk warm naan-brood of Libanees brood of zilvervliesrijst (Hetty) en garneer met fijngesneden koriander.     

Recept: Natasja Kilerciyan in Trouw

Eet smakelijk!    

Groeten van mama Hetty      
