## Ingrediënten

Gevulde paprika: 

- 2 paprika
- vega gehakt
- geraspte kaas
- evt. paar champignons
- 1 tomaat

Gevulde portobello:

- 2 portobello's
- brie

Salade:

- Sla
- Tomaten
- Komkommer
- Vinagraitte:
    - 6 eetlepels olijfolie
    - 2 eetlepels witte wijnazijn
    - 1 theelepel mosterd
    - 1 theelepel honing
        
Variatie: een van de twee gevulde dingen met tabluleh (couscous salade) 

## Bereiding

- Verwarm oven op 200 graden
- Verwijder steeltje portobello's
- Vul portobello's met tomaat, tijm, en brie
- 30 minuten in de oven
- Vul paprika met gebakken gehakt, evt. champignons, tomaat, en strooi kaas eroverheen
- Bak paprika 20 minuten
- Maak salade

