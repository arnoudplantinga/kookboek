## Ingrediënten

Voor 4 personen:
- 1 courgette
- 100 gram erwten
- 125 gram spinazie
- 1 ui
- 2 teentjes knoflook
- 750 ml passata
- 250 gram verse lasagnebladen
- 1 bol burrata
- 30 gram Parmezaan
- 3 eetlepels pesto
- Handje verse basilicum
- 2 theelepels oregano
- Peper en zout
- Olijfolie

## Bereiding

Pel en snipper de ui en hak de knoflook fijn. Fruit dit aan in een pan in iets olijfolie.

Snijd de courgette in kleine blokjes en bak deze mee.

Voeg de passata en de erwten toe en breng de saus op smaak met peper, zout en oregano.

Snijd de lasagnebladen in reepjes en voeg ze bij de saus. Blijf af en toe roeren zodat de lasagne niet aan elkaar plakt. Wordt de saus te droog, voeg dan een klein scheutje water toe. Voeg vervolgens beetje bij beetje de spinazie toe, totdat alles geslonken is. Na ongeveer 10 minuten is de lasagne gaar.

Leg de bol burrata op de lasagne. Garneer met pesto, geraspte Parmezaan en verse basilicum.
