## Ingrediënten voor 2 personen

- 1 grote aubergine, in plakken van ongeveer een halve centimeter dik
- 1 ui, in dunne halve ringen
- 4 tenen knoflook, in dunne plakjes
- 800 gram tomaten uit blik (van goede kwaliteit, zoals San Marzano)
- Snufje gedroogde chilivlokken
- 45 gram panko
- 30 gram geraspte Parmezaanse kaas
- 2 à 3 eetlepels kappertjes, grof gehakt
- 2 eetlepels verse oregano, grof gehakt
- Flinke hand verse bladpeterselie, grof gehakt
- 225 gram Mozzarella
- 120 ml olijfolie
- Zout & peper

## Bereiding

Verwarm de oven voor op 230 graden.

Leg de plakken aubergine op een bakplaat of in een braadslee (tip: het werkt heel goed als je ze direct op metaal legt). Bestrooi met peper en zout en de helft van de olijfolie. Draai ze even om op de plaat en laat ze daarna 25 tot 30 minuten bakken in de hete oven. Ze moeten goed zacht en gekarameliseerd zijn, een beetje alsof ze gefrituurd zijn.

Maak ondertussen de tomatensaus. Verhit een grote pan op laag vuur en doe er twee eetlepels olijfolie in. Bak de ui en knoflook samen met wat zout en peper in 8 tot 10 minuten tot ze zacht zijn en beginnen te karameliseren.

Doe de gedroogde chilivlokken erbij en giet het sap uit de blikken tomaten in de pan. Knijp de tomaten vervolgens zachtjes met je handen in stukken en voeg ze toe. Let op: doe dit zo laag mogelijk boven de pan en knijp héél zacht, anders zitten zowel jij als je keuken onder de tomaat.

Voeg nogmaals zout en peper toe en laat 15 tot 30 minuten pruttelen op laag vuur.

Ondertussen kun je de panko roosteren in een koekenpan met twee eetlepels olijfolie. Blijf goed roeren en laat ongeveer 5 minuten roosteren, tot de panko goudbruin is.

Breng de tomatensaus op smaak met zout en peper. Volgens Alison kun je nu de helft van de saus invriezen voor later gebruik (of voor door de pasta), maar wij hebben bijna alle saus opgebruikt. Net wat je lekker vindt!

En dan nu: stapelen. Verdeel de helft van de tomatensaus over de bodem van de ovenschaal. Bedek met de helft van de aubergine. Bestrooi met de helft van de Parmezaanse kaas, de helft van de oregano en peterselie en de helft van de kappertjes. Daarna mag de helft van de panko erover, gevolgd door de helft van de Mozzarella (scheur met je handen in stukjes).

Herhaal dit klusje en eindig met de laag Mozzarella. Rasp er eventueel nog wat extra Parmezaanse kaas overheen (jááá) en zet in de voorverwarmde oven. Na ongeveer 15 minuten zal je melanzane alla parmigiana met bubbelende, goudbruine gesmolten kaas uit de oven komen. Wat een feest.

Maak desgewenst af met nog wat van de oregano en peterselie en eet lekker met brood of een frisse salade.
