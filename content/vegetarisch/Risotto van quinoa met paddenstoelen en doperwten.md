#### Ingrediënten:

-   [Quinoa mix](http://www.voedzaamensnel.nl/tag/quinoa-mix/) - 150 gram + 250 ml water
-   [Bouillonpoeder](http://www.voedzaamensnel.nl/tag/bouillonpoeder/) - 1 theelepel
-   [Kastanje
    champignons](http://www.voedzaamensnel.nl/tag/kastanje-champignons/) - 100 gram
-   [Oesterzwammen](http://www.voedzaamensnel.nl/tag/oesterzwammen/) - 100 gram
-   [Doperwten](http://www.voedzaamensnel.nl/tag/doperwten/) - diepvries, 100 gram
-   [Parmezaanse
    kaas](http://www.voedzaamensnel.nl/tag/parmezaanse-kaas/) - 30 gram
-   [Knoflook](http://www.voedzaamensnel.nl/tag/knoflook/) - 1 teen
-   [Roomboter](http://www.voedzaamensnel.nl/tag/roomboter/) - klontje
-   [Peper](http://www.voedzaamensnel.nl/tag/peper/) 
-   [Zout](http://www.voedzaamensnel.nl/tag/zout/) 

 

#### Bereidingswijze:

1.  Spoel de quinoa goed af met water
2.  Vul de waterkoker en zet hem aan
3.  Pak een pan voor de quinoa en een pan voor de doperwten
4.  Leg de quinoa in de ene pan, schep de bouillonpoeder erbij en giet
    250 ml van het hete water er overheen
5.  Giet voldoende heet water bij de doperwten en laat de erwten en de
    quinoa 8 minuten koken
6.  De quinoa moet het water helemaal hebben opgenomen, proef de quinoa,
    is het te hard voeg dan nog wat water toe en laat het koken totdat
    het water is opgenomen
7.  Snijd ondertussen de champignons en de oesterzwammen in reepjes en
    snijd de knoflook fijn
8.  Bak de knoflook kort aan met een beetje roomboter en voeg de
    champignons en oesterzwammen toe, bak 3 minuten op laag vuur
9.  Rasp ondertussen de Parmezaanse kaas
10. Giet de erwten af en voeg ze samen met de quinoa toe aan de
    champignons en oesterzwammen
11. Roer de Parmezaanse kaas er doorheen totdat je de kaas is gesmolten
    en voeg naar smaak wat peper en zout toe

 
