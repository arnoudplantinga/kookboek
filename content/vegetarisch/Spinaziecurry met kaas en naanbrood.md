hoofdgerecht
4 personen
460 kcal
[voedingswaarden](http://www.ah.nl/allerhande/recept/R-R559646/spinaziecurry-met-kaas-en-naanbrood#)
-   15 min. bereiden

#### Ingrediënten

-   [2 uien]( "zet ingrediënt op mijn lijst")
-   [2 vleestomaten]( "zet ingrediënt op mijn lijst")
-   [1 teen knoflook]( "zet ingrediënt op mijn lijst")
-   [2 el olijfolie]( "zet ingrediënt op mijn lijst")
-   [2 el kerriepoeder]( "zet ingrediënt op mijn lijst")
-   [750 g gesneden bladspinazie à
    la crème (diepvries)]( "zet ingrediënt op mijn lijst")
-   [200 g salade kaasblokjes]( "zet ingrediënt op mijn lijst")
-   [280 g naanbroden knoflook
    & koriander (pak)]( "zet ingrediënt op mijn lijst")

 

#### Bereiden

1.  Snijd de ui in parten en de tomaten in blokjes. Snijd de
    knoflook fijn.
2.  Verhit de olie in een hapjespan en fruit de ui. Voeg de tomaten,
    knoflook en het kerriepoeder toe en roerbak 1 min. Schep de spinazie
    erbij en laat deze al roerend ontdooien. Schep de uitgelekte
    kaasblokjes erdoor en warm ze mee.
3.  Halveer de naanbroden en rooster ze in een broodrooster. Snijd open
    en serveer met de spinaziecurry.



#### bereidingstip:

Je kunt het brood ook roosteren onder de ovengrill.





 
