**Ingrediënten**

1/2 pak bladspinazie deelblokjes
(diepvries à 450 g)

5 plakjes bladerdeeg (diepvries)

5 eieren (M)

nootmuskaat

zout

4-seizoenenpeper uit de molen

125 ml slagroom

ca. 200 g geitenkaas (chèvre de
Bellay)

100 g rookamandelen

1/2 eetlepel boter of margarine om in te
vetten

1 eetlepel bloem om te bestuiven

 

**Materialen**

quiche- of ovenschaal doorsnede 24 à 26
cm

 

**Bereiden**

Spinazie in zeef laten ontdooien.
Bladerdeeg op aanrecht uitspreiden en in ca. 10 minuten laten ontdooien.
Oven voorverwarmen op 200 °C of gasovenstand 4. Vorm invetten. Aanrecht
bestuiven met bloem. Plakjes bladerdeeg op elkaar leggen en uitrollen
tot ronde lap van ca. 30 cm doorsnede. Vorm bekleden met bladerdeeg. In
kom eieren loskloppen met mespunt nootmuskaat, zout, peper en slagroom.
Kaas erboven verkruimelen en erdoor kloppen. Met lepel vocht uit
spinazie drukken. Spinazie door eimengsel roeren. Spinazie-eimengsel in
taart scheppen. Taart in midden van oven ca. 20 minuten bakken.
Amandelen grof hakken en erover verdelen. Taart in oven in nog ca. 15
minuten goudbruin en gaar bakken. Serveren met tomatensalade.

 

 

 

 
