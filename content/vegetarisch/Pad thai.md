In Thailand, pad Thai was a regular lunch for us (and the perfect remedy for a Thai-bucket-induced hangover). It varies everywhere you go, but typically includes the artful placement of fresh lime, peanuts, and scallions around the bowl. We like to replicate this and serve it with chili flakes, Thai sweet chili sauce, and sriracha.

## Ingredients

- 5 oz extra-firm tofu
- 1 tbsp cornstarch
- ¼ cup vegetable oil
- 7 oz flat dried rice noodles
- ½ onion
- 2 garlic cloves
- 1 fresh red chili
- 1 carrot
- splash of water
- 3½ oz bean sprouts
- 3 limes
- ¼ cup soy sauce
- 2 scallions
- ½ cup unsalted peanuts
- 1 tbsp chili flakes, to serve
- Thai sweet chili sauce, to serve, optional
- sriracha sauce, to serve, optional

FOR THE DRESSING

- 1 tbsp palm sugar (or any sugar)
- 2 tbsp tamarind paste
- 1 tbsp sweet chili sauce

- Tofu press or 2 clean kitchen towels and a weight such as a heavy book | Wok

## Instructions

1. Press the tofu using a tofu press or place it between two clean kitchen towels, lay it on a plate, and put a weight on top | Leave for at least 30 minutes to drain any liquid and firm up before you start cooking
1. In a bowl, mix together all the ingredients for the dressing
1. Take half the tofu and cut it into ⅓-inch cubes (save the other half for another time) | Sift over the cornstarch and turn the tofu to coat all over
1. Put the wok over high heat and pour in 2 tablespoons of oil | Add the tofu and immediately reduce the heat to medium | Stir gently, without breaking up the tofu, until lightly browned | Transfer to a plate
1. Bring water to a boil | Put the noodles in a bowl, cover them with the hot water, and leave for about 3 minutes, until they’re flexible but not cooked (check the package directions to make sure you don’t fully cook them) | Drain and run under cold water | Set aside
1. Peel and chop the onion and garlic | Rip the stem from the chili and chop, removing the seeds if you prefer a milder flavor | Trim the carrot and cut into matchsticks
1. Put the wok back over high heat and add the remaining 2 tablespoons of oil | Add the onion, garlic, and chili and cook, stirring regularly, for 1–2 minutes | Add the carrot and cook for another 1–2 minutes | Add the noodles, dressing, and a splash of water | Fry for a few minutes until the vegetables are tender
1. Return the tofu to the wok with the bean sprouts | Cut 1 lime in half and squeeze in the juice, catching any seeds in your other hand | Add the soy sauce | Stir-fry until the vegetables are slightly soft but still crunchy | Remove from the heat | Taste and add soy or chili sauce if needed
1. Slice the green part of the scallions into long, thin strips | Break up the peanuts | Cut the remaining limes into wedges
1. Divide the pad Thai among bowls with piles of sliced scallion, peanuts, lime wedges, and chili flakes | Serve with sweet chili sauce or sriracha on the side, if using
