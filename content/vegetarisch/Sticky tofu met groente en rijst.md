## Ingrediënten 4 personen

- 300 g basmatirijst
- 375 g tofu naturel
- 70 ml zonnebloemolie
- 800 g Japanse wokgroente
- Kroepoek

Saus
- 2 tenen knoflook
- 3 el sojasaus
- 2 el gembersiroop
- 1 el rijstazijn
- 4 el chilisaus
- 2 el water
- 1 el maizena
- 2 el seroendeng

## Bereiding

1. Bereid de rijst volgens de aanwijzingen op de verpakking. Laat de tofu 5 min. uitlekken in een vergiet. Leg er iets zwaars op om er zoveel mogelijk vocht uit te drukken. Verhit 50 ml olie (voor 4 personen) in een koekenpan. Dep de tofu droog en scheur het met je handen in ca. 6 slordige stukken van gelijke grootte. Bak de tofu in ca. 10 min op middelhoog vuur goudbruin en krokant. Schep uit de pan en laat uitlekken op keukenpapier.

2. Verhit de rest van de olie in een wok of hapjespan en roerbak de groente 4-6 min.

3. Snijd ondertussen de knoflook heel fijn. Breng de knoflook, sojasaus, gembersiroop, rijstazijn, chilisaus en water in een steelpan aan de kook. Laat 3 min. zachtjes koken en roer met een garde de maizena erdoor tot de saus begint te binden. Schep de gebakken tofu in de saus, schep goed om en neem van het vuur. Schep de rijst in diepe borden of kommen, verdeel de groente erover en schep de tofu met saus erop. Bestrooi met de seroendeng.
