Ingrediënten
------------

### Porties: 4 

-   25 gr bloem
-   2,5 el water
-   4 dikke plakken belegen kaas (0,5-1 cm dik) van ong. 50 gr/stuk
-   paneermeel
-   30 gr boter

Bereidingswijze
---------------

### Voorbereiding: 10min  ›  Bereiding: 5min  ›  Klaar in:15min 

1.  Een laagje paneermeel op een bord schenken.
2.  Bloem en water vermengen en tot een papje roeren dat als een brede
    band van de lepel afloopt. De plakken kaas door het bloempapje
    halen, goed laten uitlekken en door het paneermeel draaien.
3.  In een koekenpan op middel tot hoog vuur de boter smelten en hierin
    de kaasplakken vlug aan beide kanten goudbruin bakken. Direct
    serveren (anders gaan ze uitlopen).

 
