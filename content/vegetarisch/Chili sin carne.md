This deep, dark, and smoky chili is perhaps the richest we’ve tasted. The flavor comes from the mushroom base, but is boosted by untraditional ingredients like soy sauce, balsamic vinegar, maple syrup, and chocolate. You should absolutely leave it bubbling away if you have the time. It’s so, so good—you’ll be bowled over by the end result. See above for a mouthwatering preview.

## Ingredients

- 400 g mushrooms
- olive oil
- ¼ tsp salt
- ¼ tsp black pepper
- 2 red onions
- 4 garlic cloves
- 2 fresh red chilies
- 14 sprigs fresh cilantro
- 1 celery stalk
- 1 red bell pepper
- 1 tbsp tomato paste
- 250 ml red wine
- 2 tsp soy sauce
- 1 tsp balsamic vinegar
- 2 cans (400 g each) chopped tomatoes
- 1 can (400 g) black beans
- 1 can (400 g) kidney beans
- 1½ tsp maple syrup
- 14 g dark chocolate

### FOR THE SPICE MIX

- tiny bit of chili powder
- 1 tsp ground cumin
- 1 tsp smoked paprika
- ½ tsp ground cinnamon
- ½ tsp dried oregano
- ½ tsp salt
- ½ tsp black pepper
- 1 bay leaf

**Food processor | Frying pan over medium-high heat | Large saucepan over medium heat**

## Instructions

Put the mushrooms in the food processor and pulse until very finely minced (you can chop them if you prefer, but it’s quicker and better with a food processor)

Pour a little oil into the hot frying pan | Once the oil is hot, tip in the mushrooms with the salt and pepper and cook for 5 minutes | Take the pan off the heat, transfer the mushrooms to a bowl, and set aside

Peel and mince the red onions | Peel and mince the garlic | Rip the stems from the chilies, cut them in half lengthwise, and remove the seeds if you prefer a milder sauce, then chop finely | Remove the leaves from the cilantro and set aside | Finely chop the stems | Trim the leaves and root from the celery | Cut the bell pepper in half and cut out the stem and seeds | Cut the celery and pepper into very small chunks

Add a little oil to the large saucepan | Once it is hot, add the minced onions and garlic, the finely chopped cilantro stems, and the chilies and cook gently for 5–10 minutes, making sure you stir constantly | Add the chopped celery and bell pepper chunks to the pan and stir

Add all the spice mix ingredients to the pan and stir so that the spices are well mixed and coat all the vegetables | Stir in the tomato paste to give a rich color and depth of flavor | Pour the red wine, soy sauce, and balsamic vinegar into the pan and turn up the heat to high | Stir constantly until the liquid has reduced by two-thirds and the alcoholic aroma has subsided | Tip the chopped tomatoes into the pan, stir into the chili, and simmer for 5 minutes, until the sauce is noticeably thicker

Drain the black beans and kidney beans and add them to the pan along with the maple syrup, dark chocolate, and the minced mushrooms | Stir everything together really well and then reduce the heat to a very gentle simmer | Leave this bubbling away with the lid off, stirring occasionally until it’s reduced to the right thickness (at least 10 minutes) | You can leave it bubbling for longer to deepen the flavors, adding more water if needed to keep the right consistency

Take the lid off the pan and remove the bay leaf | Stir the cilantro leaves into the chili and serve—or make Big Bad Nachos!
