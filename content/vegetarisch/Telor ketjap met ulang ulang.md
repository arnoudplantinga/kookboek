## Ingrediënten voor 2 à 3 personen

Voor de ketjap telor:
- 4 à 5 eieren, gekookt
- Zonnebloemolie
- 1 rode ui, fijngesnipperd
- 2 teentjes knoflook
- 2 eetlepels tomatenpuree
- 2 theelepels sambal badjak (naar smaak)
- 1 theelepel vegan vissaus
- 4 eetlepels ketjap
- 1/2 eetlepel rietsuiker
- 100 ml water

Voor de ulang ulang:
- ½ spitskool, in reepje gesneden
- 100 gram wortel, julienne gesneden
- 1 komkommer, in halve maantjes gesneden
- 50 gram amandelen, fijngemalen
- ½ eetlepel sambal badjak of rode peper
- 2 eetlepel azijn
- ½ eetlepel rietsuiker
- ½ limoen, uitgeknepen
- Zout

Voor de rijst:
- 175 gram rijst

## Bereiding

Verwarm een flinke scheut zonnebloemolie in een smalle koekenpan en bak de gekookte eieren kort aan alle kanten. Schep uit de olie en laat uitlekken op een papiertje. Voeg ui en knoflook toe aan de pan en bak dit 1 minuut, voeg vervolgens de tomatenpuree toe. Bak dit nog eens 2 minuten.

Voeg de sambal, ketjap, vegan vissaus, suiker en water toe. Meng alles goed door elkaar en voeg de eieren toe. Schep de saus over de eieren en laat deze op heel zacht vuur ongeveer 15 minuten trekken, terwijl je soms roert. Langer mag ook, als je de tijd hebt.

Kook de rijst zoals aangegeven op de verpakking.

Meng alle ingrediënten van de salade door elkaar heen. Zet weg zolang je op de eieren wacht (dit gerecht is ook heel makkelijk van te voren klaar te maken) en serveer bij de eieren en salade.
