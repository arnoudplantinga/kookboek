## Ingrediënten 4 personen

Boemboe

- 3 sjalot
- 2 tenen knoflook
- 4 cm verse gember
- 1 el sambal oelek
- 75 g gezouten cashewnoten
- 2 tl kurkuma
- ½ tl gemalen komijn
- 1 tl gemalen koriander*

Rest

- 4 el arachideolie
- 400 ml kokosmelk
- 50 santen kokoscrème 
- 1,5 vegan bouillonblokje
- 2 stengels koelverse sereh citroengras
- 1 limoen 
- 1 verse spitskool
- 250 g kastanjechampignon
- 100 g shiitake

## Bereiding

1. Pel de sjalotjes, snijd ze in stukken en doe ze in de keukenmachine. Pel de tenen knoflook en voeg ook toe. Schil de gember, snijd in stukjes en doe ook in de keukenmachine samen met de sambal oelek, cashewnoten, kurkuma, komijn en koriander. Maal tot een gladde boemboe. Dit duurt 2-3 min.

2. Verhit de olie in een braadpan en bak de boemboe 3 min. op laag vuur. Voeg de kokosmelk en santen toe en verkruimel de bouillontabletten erboven. Snijd de stengels sereh een paar keer in de lengte in en leg in de saus. Snijd met een dunschiller of scherp mesje zo dun mogelijk een paar grote stukken van de groene schil van de limoen en leg ook in de saus. Laat de rendangsaus een half uur zachtjes koken zonder deksel op de pan. Roer regelmatig. Haal de limoenschillen en de sereh uit de saus.

3. Snijd ondertussen de spitskool in kwarten, snijd de harde kern eruit en snijd de kool in repen. Snijd de kastanjechampignons in plakjes en scheur de shiitake in grove stukken. Verhit de olie in een grote wok en roerbak de kool en paddenstoelen 5 min. Voeg dan toe aan de rendangsaus en laat nog 10 min. zachtjes koken. Breng eventueel op smaak met peper en zout. Snijd de geschilde limoen in partjes en serveer erbij.

## Combinatietip

Lekker met rijst en gebakken uitjes.
