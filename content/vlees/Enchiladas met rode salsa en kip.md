## Ingrediënten

- 250 gram kipfilet
- 2 rode uien
- Blik tomatenpuree (70 gram)
- Gezeefde tomaten
- 5 tortillas
- 1 teentje knoflook
- 100 ml crème fraîche
- 100 gram geraspte mozzarella
- Bosje koriander
- 1 laurierblaadje
- 100 gram gemengde salade
- Knoflook
- Peper en zout
- Evt. jalapeñopeper

## Bereiding

Doe de kip, 1 ui, laurier, knoflook en de helft van koriander in 1 liter koud water en breng aan de kook. Houd 10 min. tegen de kook aan. 

Haal kip uit de pan, trek uit elkaar en verdeel over tortilla’s. 

Roer voor de salsa 130ml kookvocht van kip en de helft van de tomatenpuree door de kruidenpasta. 

Verdeel de helft ervan over kip. Rol de tortilla’s op, leg in ovenschaal. Verdeel rest van salsa, kaas en dungesneden ui erover. 

Zet 10 min. in oven op 180C°. Verdeel koriander en room over de tortilla’s. Serveer met een salade.
