## Ingrediënten voor 4 personen
- 300 gram orzo (of parelcouscous)
- 4 kippendijen
- 2 eetlepels gyroskruiden (zie onder)
- 4 flinke handen kerstomaatjes
- 1 komkommer
- 1 rode paprika
- 1/2 rode ui, gesnipperd
- Olijven
- Feta
- Verse munt
- 2 theelepels gedroogde oregano
- Tzatziki, naar smaak
- Zout & peper
- Olijfolie

Voor de gyroskruiden:
- 2 theelepels paprikapoeder
- 2 theelepels chilipoeder
- 2 theelepels gedroogde oregano
- 2 theelepels gedroogde tijm
- 2 theelepels korianderpoeder
- 1 theelepel komijnpoeder
- 2 theelepels knoflookpoeder
- 1/2 theelepel kaneel
- Peper & zout

## Zo maak je deze Griekse bowl

Meng alle specerijen voor de gyros-kruiden. Gebruik twee eetlepels en wrijf de kippendijen ermee in. Laat even liggen.

Kook de orzo volgens de aanwijzingen op de verpakking en giet af.

Bak de kippendijen krokant in een koekenpan met een laagje olijfolie. Snijd ze daarna in reepjes.

Halveer ondertussen de tomaatjes en snijd de komkommer en paprika in stukjes. Doe ze bij elkaar in een kom. Voeg de gesnipperde rode ui en de verse munt toe. Bestrooi met de gedroogde oregano en peper & zout en hussel door elkaar.

Maak de bowls op: schep er wat orzo in, wat van de frisse groenten en de reepjes kippendij. Maak af met een lekkere schep tzatziki en schenk een glaasje ouzo voor jezelf in.
