1 kg varkensgebraad, 50 gr. boter

saus: 150 gr. champignons, 150 gr. gekookte hesp, een halve liter melk,
45 gr. boter, 45 gr. bloem, 100 gr. gemalen kaas.

Kruiden: peper, zout en nootmuskaat.

 

Vlees op gewone manier braden gedurende ongeveer 50 minuten.

Na enkele minuten afkoelen, snijden.

Intussen de champignons kuisen, snijden en opstoven. De hesp fijn hakken
of snijden.

Een witte saus maken en de hesp en champignons onder mengen.

Tussen elke snede vlees een lepel saus brengen en het gebraad terug in
elkaar schikken in een pyrex schotel.

Overschot van saus rond het vlees gieten. Alles bestrooien met de
gemalen kaas.

Opnieuw in hete over schuiven en gedurende ongeveer 15 minuten laten
verder braden en kleuren.

Warm opdienen. Kan dus op voorhand klaargemaakt worden.

Met kroketjes of puree.
