**Ingrediënten**

- 1 doosje bladerdeeg hartige taart (450 g)
- 1 zak paprikamix
- 1 duopak gerookte kipreepjes (200 g)
- 2 el crema van zongedroogde tomaten (pot 140 g)
- 4 scharreleieren
- 1 bakje hüttenkäse (200 g)
- 1/2 zakje verse bieslook (a 25 g)
- 2 zakken veldsla (a 75 g)
- 2 el yoghurtdressing (fles 450 ml)

**Materialen**

quiche- of ovenschaal doorsnede 24 à 26 cm

**Bereiden**

1. Oven voorverwarmen op 220 °C.
2. Deegrol uitrollen en met bijbehorend bakpapier in bakvorm leggen. Deeg stevig langs wanden van vorm aandrukken en het er iets overheen laten hangen. Bodem van deeg met vork inprikken.
2. Paprika's schoonmaken en in heel dunne reepjes snijden.
2. In kom paprika en kipreepjes mengen met crèma van tomaten, zout en peper naar smaak toevoegen.
2. Kipmengsel over taartbodem verdelen.
2. In kom eieren loskloppen, hüttenkäse erdoor scheppen, bieslook erboven fijnknippen en zout en peper naar smaak toevoegen. Eimengsel over taartvulling verdelen.
2. Hartige taart in midden van oven in ca. 35 min. goudbruin en gaar bakken.
2. Taart iets laten afkoelen. Intussen veldsla mengen met yoghurtdressing.
2. Taart in 8 punten snijden. Salade over 4 grote borden verdelen. Op elk bord 2 punten taart leggen.

 

 
