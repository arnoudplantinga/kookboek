Prei is eigenlijk een ideale groente, en wordt misschien wel wat
ondergewaardeerd in de keuken. Dat is niet terecht, want je kunt hem in
diverse gerechten gebruiken. Als ovenschotel bijvoorbeeld, en daar heb
je helemaal geen pakje van de Maggi voor nodig! Deze schotel maken we
gewoon zelf met een aantal simpele ingrediënten. We 'gratineren' hem
niet met kaas, maar met lekkere amandelsnippers.

 

-   *Voorbereidingstijd* <span style="font-size:100%;" title="PT5M">5
    minuten</span>
-   *Kooktijd* <span style="font-size:100%;" title="PT30M">30
    minuten</span>
-   *Aantal porties:* <span style="font-size:100%;">2</span>
-   *Beoordeling:* <span style="font-size:100%;"><span
    style="font-size:100%;">3.77</span> sterren - gebaseerd op<span
    style="font-size:100%;"> 13</span> review(s)</span>

#### Ingrediënten:

-   [Prei](http://www.lekkerensnel.com/tag/prei/) - <span
    style="font-size:100%;">2 stuks</span>
-   [Voorgekookte
    aardappelen](http://www.lekkerensnel.com/tag/voorgekookte-aardappelen/) - <span
    style="font-size:100%;">500 gram, bijvoorbeeld een zakje
    voorgekookte krieltjes</span>
-   [Rundergehakt](http://www.lekkerensnel.com/tag/rundergehakt/) - <span
    style="font-size:100%;">250 gram</span>
-   [Slagroom](http://www.lekkerensnel.com/tag/slagroom/) - <span
    style="font-size:100%;">125 ml</span>
-   [Ei](http://www.lekkerensnel.com/tag/ei/) - <span
    style="font-size:100%;">1</span>
-   [Kerriepoeder](http://www.lekkerensnel.com/tag/kerriepoeder/) - <span
    style="font-size:100%;">Een eetlepel</span>
-   [Amandelschaafsel](http://www.lekkerensnel.com/tag/amandelschaafsel/) - <span
    style="font-size:100%;">Hoeveelheid naar eigen smaak</span>

#### Instructies:

1.  Verwarm de oven voor op 200 graden
2.  Maak de prei schoon en snij in ringetjes
3.  <span style="font-size:100%;">Zet een koekenpan op hoog vuur en bak
    het gehakt rul, voeg peper en zout toe</span>
4.  Doe er vervolgens de prei bij en bak dit gedurende 3 minuten mee,
    roer het goed door elkaar
5.  Doe de gehakt en de prei in een ovenschaal
6.  Breek het ei en meng met de slagroom en de kerrie
7.  Giet het mengsel over de prei en het gehakt en hussel het even goed
    door
8.  Bedek het mengsel vervolgens met een laag aardappelen
9.  Strooi tot slot het amandelschaafsel over de aardappels heen
10. Zet gedurende 30-35 minuten in de oven op 200 graden

 
