*Ik heb courgettes en paprika's gevuld met mengsel van gehakt, binnenste
van courgette en bovenkant van de paprika's. Geitenkaas erbovenop.*

 

#### Ingrediënten:

-   [Courgette](http://www.voedzaamensnel.nl/tag/courgette/) - 2 kleine
-   [Gehakt](http://www.voedzaamensnel.nl/tag/gehakt/) - 300 gram
-   [Champignons](http://www.voedzaamensnel.nl/tag/champignons/) - 70
    gram
-   [Paprika](http://www.voedzaamensnel.nl/tag/paprika/) - 1
-   [Ui](http://www.voedzaamensnel.nl/tag/ui/) - 1
-   [Knoflook](http://www.voedzaamensnel.nl/tag/knoflook/) - 1 teentje
-   [Sambal oelek](http://www.voedzaamensnel.nl/tag/sambal-oelek/) - 2
    theelepels
-   [Geraspte oude
    kaas](http://www.voedzaamensnel.nl/tag/geraspte-oude-kaas/) - 75
    gram
-   [Peper](http://www.voedzaamensnel.nl/tag/peper/) - 1 theelepel
-   [Zout](http://www.voedzaamensnel.nl/tag/zout/) - snufje

#### Bereidingswijze:

1.  Verwarm de oven voor op 180 graden
2.  Was de courgettes, snij ze in de lengte doormidden
3.  Hol de courgettes voorzichtig uit met een lepel en stop het
    vruchtvlees in een schaal
4.  Snij de paprika en champignons in kleine blokjes
5.  Snij de ui en knoflook fijn
6.  Stop alle groenten in de schaal en voeg sambal, peper en zout toe,
    roer door elkaar
7.  Bak het gehakt rul en voeg vervolgens het groentemengsel toe, bak
    even mee
8.  Leg de courgettes naast elkaar in een ovenschaal en vul ze met het
    mengsel
9.  Strooi de geraspte kaas er overheen en zet voor 15 minuten in de
    oven
10. Zet vervolgens nog 3 minuten de grillstand van je oven aan voor een
    knapperig korstje

