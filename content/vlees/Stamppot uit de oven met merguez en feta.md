## Ingrediënten voor 4 personen

- 500 gram winterpeen, geschild en in stukken
- 1 rode paprika, geschild en in kleine stukjes
- 1 kg kruimige aardappelen, geschild en in stukken
- 4 tenen knoflook, in de schil
- Scheut olijfolie
- Snuf ras en hanout
- Snuf paprikapoeder
- Snuf komijn
- 200 ml volle melk
- Handje rucola
- 100 gram feta, verbrokkeld
- 4 merguezworstjes

## Bereiding

Verwarm de oven voor op 200 °C. Doe de aardappelen, paprika, winterpeen en knoflook in een grote ovenschaal en schep om met een scheut olijfolie, de komijn, paprika, ras el hanout en een royale snuf zout en peper. Zet de ovenschaal ongeveer 45 à 55 minuten in de oven. Schep af en toe om.

Bak de laatste tien minuten de merguezworstjes krokant in een pan en zet apart. Test of de stukjes winterpeen gaar zijn met een mesje. Haal de stamppot uit de oven, pel de knoflook en pers deze uit bij de rest van de groenten.

Schenk er de melk bij en gebruik een pureestamper om er stamppot van te stampen. Dit kan eventueel rechtstreeks in de ovenschaal, maar als die niet groot genoeg is kun je de inhoud ook even overscheppen in een grote kookpan.

Strijk de stamppot met een spatel glad in de ovenschaal en verdeel de rucola, feta en merguezworstjes erover.

