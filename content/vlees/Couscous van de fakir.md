Cous­cous van de fa­kir
=======================

Groenterijke couscous met rundergehakt. Zó gemaakt.
---------------------------------------------------

hoofdgerecht

[4 personen]()

625 kcal
[voedingswaarden](http://www.ah.nl/allerhande/recept/R-R626116/couscous-van-de-fakir#)

-   20 min. bereiden

#### Ingrediënten

-   [250 g sperziebonen]( "zet ingrediënt op mijn lijst")
-   [300 g couscous]( "zet ingrediënt op mijn lijst")
-   [2 el pijnboompitten]( "zet ingrediënt op mijn lijst")
-   [1 ui]( "zet ingrediënt op mijn lijst")
-   [2 el olijfolie]( "zet ingrediënt op mijn lijst")
-   [450 g mager rundergehakt]( "zet ingrediënt op mijn lijst")
-   [2 puntpaprika's]( "zet ingrediënt op mijn lijst")

#### Bereiden

1.  Verwijder de steelaanzet en het puntje van de sperziebonen en kook
    ze in 5 min. beetgaar. Bereid ondertussen de couscous volgens de
    aanwijzingen op de verpakking. Rooster de pijnboompitten 2 min. in
    een koekenpan zonder boter of olie. Laat afkoelen op een bordje.
2.  Snipper de ui. Verhit de olie in een koekenpan en fruit de ui 4 min.
    Voeg het gehakt toe en bak het rul en goudbruin. Snijd de
    puntpaprika in stukjes. Voeg de paprika toe aan het gehakt. Breng op
    smaak met peper en zout. Meng de couscous met de sperziebonen en
    het gehaktmengsel. Verdeel de couscous over borden en bestrooi met
    de pijnboompitten.
