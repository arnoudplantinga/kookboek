(Met frietjes of puree)

 

<span style="text-decoration:underline;">Nodig:</span>

Gehakt (ongeveer 150 gram p.p.), laurierblad, tijm, eventueel
champignons (vers of uit potje), bloem, boter, peper, zout,
tomatenpuree.

 

<span style="text-decoration:underline;">Bereiding:</span>

- Draai van het gehakt balletjes.

- Zet water aan de kook met daarin 1 laurierblad en wat tijm. Als het
water kookt kunnen de balletjes erin.

- (Intussen eventueel champignons bakken.)

- Wanneer de balletjes gaar zijn, ze met een schuimspaan uit het water
scheppen. Het laurierblad weggooien, het kookvocht bewaren in een kan.

- Boter smelten, bloem erbij (tot het bruin is). Daarna traag het vocht
van de balletjes erbij gieten al roerend (zodat het niet klontert).

- Vervolgens 1 blikje tomatenpuree erbij doen, goed roeren en kruiden
met peper en zout.

- Nu de balletjes (en de champignons) in de saus toevoegen. Klaar!
