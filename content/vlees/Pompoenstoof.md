4 personen

535
kcal [voedingswaarden](http://www.ah.nl/allerhande/recept/R-R986108/pompoenstoof#)

-   30 min. bereiden
-   25 min. wachten

 

#### Ingrediënten

<span
style="font-size:115%;font-family:'Futura W01 Heavy', sans-serif;color:#48535b;">4
personen</span>

-   850 g flespompoen
-   250 g chorizo
-   2 rode uien
-   2 tenen knoflook
-   20 g platte peterselie
-   295 g gegrilde rode paprika's
-   500 g krieltjes
-   800 g linzen (of minder gedroogde linzen)
-   2 tl paprikapoeder
-   70 g tomatenpuree
-   350 ml hete
    kippenbouillon
-   150 ml witte port

 

###### **Keukenspullen**

-   gietijzeren pan

 

#### Bereiden

1.  Halveer de pompoen. Verwijder de zaden en de draderige binnenkant
    met een lepel. Snijd in parten en verwijder de schil. Halveer
    de parten. Snijd de chorizo in plakjes en de ui in kwarten. Snijd de
    knoflook fijn en de peterselie grof. Laat de paprika uitlekken en
    snijd in stukjes. Halveer de krieltjes en laat de linzen uitlekken.
2.  Bak de chorizo in een gietijzeren pan zonder olie of boter op
    middelhoog vuur goudbruin en krokant. Schep uit de pan en laat
    uitlekken op keukenpapier. Laat het bakvet in de pan.
3.  Bak in het bakvet de ui met de paprikapoeder 3 min. op
    middelhoog vuur. Bak de knoflook 3 min. mee.
4.  Voeg de tomatenpuree toe en bak 2 min. mee.
5.  Voeg de paprika, pompoen, krieltjes, bouillon en port toe. Roer door
    en breng aan de kook.
6.  Leg de deksel schuin op de pan en laat op laag vuur in 25 min.
    gaar stoven. Schep regelmatig om. Voeg de linzen na 15 min. toe.
    Controleer na 20 min. of de pompoen en de krieltjes gaar zijn.
7.  Schep 2/3 van de chorizo met de helft van de peterselie door
    de stoof. Verdeel de rest van de chorizo en peterselie erover.

 
