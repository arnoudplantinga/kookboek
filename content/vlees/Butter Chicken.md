Prep time:  15 mins

Cook time:  35 mins

Total time:  50 mins

 

Serves: 6 servings

 

This recipe has been updated (see comments below) - enjoy!

Ingredients

For chicken & marinade:

-   2.2 lbs (1 kg) boneless, skinless chicken breast cut in 1" - 2"
    cubes
-   2 tbsp lemon juice
-   2 cloves garlic, minced
-   1 tbsp garam masala
-   1 tsp kosher salt

For sauce:

-   &frac14; cup vegetable oil
-   2 &frac12; cups chopped onion (about 2 medium-large)
-   2 tbsp coarsely chopped garlic
-   2 tbsp garam masala
-   2 tsp paprika
-   &frac14; tsp cinnamon
-   2 tsp kosher salt, or to taste
-   2 cups diced no-salt-added canned tomatoes
-   &frac34; cup cream
-   2 tbsp butter
-   Chopped cilantro, to garnish (optional)

 

Instructions

For marinade:

1.  Combine all ingredients in a zip-top bag or shallow baking dish,
    massaging the marinade into the chicken. Let stand at room
    temperature while you prepare the sauce, or marinate in the
    fridge overnight.

For sauce:

1.  Heat oil over medium heat in a large saucepan or dutch oven. Add
    onions and slowly cook until golden, about 20 minutes, reducing heat
    if they are getting crispy or browning quickly.
2.  Add garlic and cook until fragrant, about 1 minute. Stir in garam
    masala, paprika, cinnamon and salt; cook 1 minute more. Add
    tomatoes; cook 2 minutes, then add cream and carefully puree using
    an immersion blender (or standing blender, but do it in batches or
    the steam will blow the top off).
3.  Return sauce to saucepan and bring to a simmer. Add chicken to the
    sauce, cover, and simmer over medium-low heat until cooked through,
    about 12 minutes (remove a couple of pieces to make sure they are no
    longer pink inside). A gentle simmer is required to gently cook the
    breasts so they don't become tough, and you don't want to
    overcook them.
4.  Stir in butter, taste and add more salt to taste. Serve sprinkled
    with cilantro, if desired.

 
