
![](images/franse_kip_met_ui.jpg)

*serves 4 to 6*

- 3 tablespoons unsalted butter
- 2 pounds onions, sliced into thin half-moons
- Kosher salt and freshly ground black pepper
- 4 garlic cloves, sliced
- 2 small sprigs thyme, leaves only
- 4-inch sprig rosemary
- 2 cups chicken broth, divided
- 1 tablespoon balsamic vinegar
- 2 tablespoons Dijon mustard
- 3 pounds boneless skinless chicken thighs
- 2 ounces Gruyère cheese, finely grated or shaved (about 1 cup)



## 1

Melt the butter in a deep 10-inch sauté pan over medium heat. When the
butter has melted completely and foams up, add the onions. They will
fill the pan to the top, at this point. Stir as you add the onions to
coat them in the butter. Sprinkle lightly with salt and black pepper.
Cook the onions for about 40 minutes over low or medium heat, stirring
occasionally.

## 2

When the onions have developed an evenly light beige color throughout,
add the garlic, thyme leaves, and whole rosemary sprig, and cook for a
few minutes more, stirring frequently. Turn the heat up to high and cook
for a further 5 minutes, stirring frequently. You want dark, slightly
burnt spots to appear on the onions, and for them to develop a rich
mahogany color. When the onions get quite dark, add 1 cup of the beef or
chicken broth. Add it slowly, stirring and scraping the pan vigorously
to scrape up any burnt or stuck-on bits. When the liquid has been added,
bring it back up to a simmer and simmer lightly for 5 minutes, or until
it is somewhat reduced.

## 3

Take the onions off the heat and pour them into a 3-quart oven-safe dish
with a lid. (If you don't have a Dutch oven or another oven-safe dish
with a lid, you can use a 9x13-inch baking dish. Just cover it tightly
with a double layer of foil.)

Heat the oven to 325°F.

## 4

While the onions are cooking, brown the chicken. Heat another 10-inch or
cast iron skillet over medium-high heat. Pat the chicken thighs dry and
season lightly with kosher salt and black pepper. When the skillet is
hot, add the thighs and brown for about 3 minutes on each side, 6
minutes total. When they've developed a golden-brown crust, remove from
the pan and set on top of the caramelized onions in the baking dish.

## 5

Add the remaining 1 cup broth to the pan. Stir vigorously, scraping up
any browned bits on the bottom of the pan. Whisk in the balsamic vinegar
and Dijon mustard. Simmer for about 5 minutes or until reduced by half.
Pour this sauce over the chicken and onions, and put the lid on the
baking dish. The chicken and onions will look quite saucy; there will be
plenty of liquid in the baking dish.

(*At this point you can refrigerate the dish for up to 48 hours. Let it sit at room temperature for at least 15 minutes before baking, or else add about 5 minutes to the bake time.*)

## 6

Bake at 325°F for 30 minutes. Remove the chicken from the oven and turn
the heat up to broil. Take the lid off the baking dish, and sprinkle the
cheese evenly over the top of the chicken. When the broiler has heated
up, return the dish to the oven and broil for 3 to 4 minutes, or until
the cheese is melted and golden on top.

 
