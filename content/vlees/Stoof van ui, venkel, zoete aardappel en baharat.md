

4 personen

515
kcal [voedingswaarden](http://www.ah.nl/allerhande/recept/R-R1186288/stoof-van-ui-venkel-zoete-aardappel-en-baharat#)

-   35 min. bereiden<span
    style="font-size:13px;line-height:1.5em;"> 

 

## Ingrediënten

- 300 g zoete aardappelen
- 1 venkelknol
- 200 g quinoa-bulgurmix
- 50 g rozijnen
- 2 el zonnebloemolie
- 1 ui
- 1 el baharat
- 400 ml gepelde tomaten
- 300 g rundergehaktballetjes of falafel
- 4 el yoghurt

#### Bereiden

1.  Snijd de aardappel in stukjes van 1 x 1 cm. Snijd de stelen en een
    klein stukje van de onderkant van de venkel. Houd het groen apart.
    Halveer de venkel en snijd in dunne plakken. Kook de
    quinoa-bulgurmix volgens de aanwijzingen op de verpakking gaar. Voeg
    de laatste 2 min. de rozijnen toe.
2.  Verhit ondertussen 1 el olie in een ruime pan met dikke bodem en
    fruit de ui en baharat 2 min. Voeg de aardappel toe en bak 5 min. op
    middelhoog vuur. Voeg de tomaten toe, plet ze met de achterkant van
    een lepel en laat met de deksel op de pan op laag vuur 15 min.
    stoven. Schep de laatste 10 min. de venkel erdoor. Breng op smaak
    met peper en eventueel zout. Verhit ondertussen de rest van de olie
    in een koekenpan en bak de gehaktballetjes in 10 min. op middelhoog
    vuur goudbruin en gaar.
3.  Schep de quinoa-bulgurmix op een schaal en verdeel de
    zoeteaardappelstoof erover. Garneer met het
    achtergehouden venkelgroen. Serveer met rundergehaktballetjes en
    besprenkel met de yoghurt.

####  

#### Bewaartip:

Maak voor een extra lunch voor de volgende dag een dubbele portie stoof,
maar laat de gehaktballetjes en yoghurt achterwege. Je kunt de stoof 3
dagen in een afgesloten bakje in de koelkast bewaren. Voeg 200 g
verkruimelde feta en 15 g fijngesneden munt toe, of voor ieder een
gekookt eitje met wat fijngesneden peterselie.
