## Ingrediënten

Voor 4 tot 6 personen:

- 1 ui, gesnipperd
- 1 rode paprika, in blokjes
- 450 gram gehakt
- 1 flinke eetlepel salsa (bijv. van Let’s Salsa)
- 1 blik zwarte bonen
- 1 blik maïskorrels
- 1 blik gehakte tomaten
- 1 jalapenopeper, fijngehakt
- 190 gram rijst
- 1 eetlepel tacokruiden
- 1/2 theelepel chilipoeder
- 500 ml kippenbouillon
- 90 gram geraspte kaas, bijv. cheddar
- Olijfolie
- Zout en peper

Toppings (optioneel):

- 1 jalapeno-peper, in ringetjes
- Zure room of yoghurt
- Guacamole of avocado
- Verse koriander
- Blokjes verse tomaat
- Tortillachips

## Bereiding

Verwarm een grote koekenpan (liefst één met deksel) op medium tot laag vuur met wat olijfolie. Doe de gehakte ui en paprika erin, laat zacht worden en voeg dan ook het gehakt toe. Bak dit lekker bruin.

Roer er dan de rest van de ingrediënten doorheen (alles behalve de kaas): de salsa, zwarte bonen, maïs, tomaten uit blik, jalapeno, rijst, bouillon, taco-kruiden en chilipoeder. Roer goed door, breng aan de kook en zet dan het vuur laag en doe de deksel op de pan. Laat dit 15 tot 20 minuten sudderen, maar roer tussendoor af en toe even zodat de rijst niet aan de bodem van de pan gaat plakken.

Proef even of de rijst goed gaar is en strooi dan de geraspte kaas over het burrito-mengsel heen.

Maak eventueel af met de toppings. Lekker om zó uit de pan te lepelen met tortillachips!
