
## Ingrediënten

- Courgette
- Appel
- Kokosmelk 200 ml
- Wortel
- Gele paprika
- Rode paprika
- Ui
- Citroengras
- Gember
- Bouillonblokje
- Knoflook 2 teentjes
- Kipdijfilet 350g
- Rijst 400g
- Kerriepoeder

## Bereiding

1. Kook de rijst
2. Was alle groenten en fruit
3. Pel de ui en snijd in halve ringen. Snijd de knoflook fijn. Schil de gember en snijd in kleine blokjes. Halveer de paprika's en snijd in reepjes.
4. Snij de appel in blokjes. Snij de courgette en wortel in halve plakjes.
5. Plet de stengel citroengras en vouw er een strik van.
6. Verhit de olie in een wok of hapjespan. Bak de kipstukjes rondom bruin in ongeveer 3 minuten.
7. Voeg de wortel toe en laat deze 2 minuten meebakken.
8. Voeg nu de ui, de paprika's, de citroengras, de gember en knoflook toe en bak dit 2 minuten mee.
9. Voeg de kerripoeder en de courgette toe en bak 1 minuut mee. Voeg vervolgens de kokosmelk en het bouillonblokje er aan toe en breng dit aan de kook.
10. Doe de appel erbij, doe een deksel op de pan en laat het gerecht afgedekt 4 minuten op lage temperatuur garen totdat de groenten de gewenste gaarheid hebben. Af en toe roeren.
11. Verwijder de stukken citroengras en serveer met de rijst.
