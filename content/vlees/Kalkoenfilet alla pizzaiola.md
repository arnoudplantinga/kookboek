#### Ingrediënten<span style="font-size:13px;line-height:1.5em;"> </span>

1 schaal kalkoenfilet à la minute (ca. 300 g)

4 el olijfolie

2 tl gedroogde oregano

3 tenen knoflook (geperst)

1 blik gepelde tomaten (400 g)

1 zakje mozzarella ((125 g), in plakken)

1 zak paprikamix ((3 stuks; groen en rood en geel), in reepjes)

1 ciabatta (in sneetjes)

Kappertjes

#### Bereiden

1.  Verwarm de oven voor op 200 °C. Bestrooi de kalkoenfilet met peper
    en zout. Verhit 2 el olie in een koekenpan. Bak de kalkoenfilet in
    1 min. per kant bruin en leg in de ingevette ovenschaal. Bestrooi
    met 1 tl oregano.
2.  Fruit in dezelfde pan 2 tenen knoflook ca. 1 min. Voeg de
    tomaten toe. Druk ze met de spatel stuk en laat in 5 min. inkoken
    tot een dikke saus. Breng op smaak met de rest van de oregano, peper
    en zout. Schep de saus over het vlees en verdeel de
    mozzarella erover. Bak de kalkoenfilet 15-20 min. in de oven.
3.  Verhit ondertussen in een wok de rest van de olie. Bak de paprika in
    4 min. beetgaar. Bak de laatste teen knoflook 1 min. mee. Breng op
    smaak met peper en zout. Serveer de kalkoenfilet met de paprika en
    de ciabatta.
