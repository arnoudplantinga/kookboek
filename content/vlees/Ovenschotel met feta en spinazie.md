**Ingrediënten**

- 2 x 300 g aardappels
- 100 ml melk
- 25 g roomboter
- 4 eetlepels olijfolie
- 350 g rundergehakt
- 1 teen knoflook, fijngesneden 
- 2 x 300 g spinazie (600 g)
- 100 g semi-zongedroogde tomaten (potje 295 g)
- 200 g feta, in blokjes

**Bereiden**

1. Kook de aardappelen in de schil in 25 minuten gaar.
2. Laat ze iets afkoelen en pel de schil eraf.
2. Verwarm de melk en smelt er de boter in. Schenk bij de aardappelen en stamp tot een puree. Breng op smaak met peper en zout.
2. Verwarm de oven voor op 200 °C.
2. Verhit 2 eetlepels olie en bak het gehakt in 8 minuten rul. Breng op smaak met peper en zout.
2. Verdeel het gehakt over de ovenschaal.
2. Verhit de rest van de olie en bak de knoflook 1 minuut. Voeg de spinazie in delen toe, die je telkens laat slinken. Laat de spinazie uitlekken. Breng op smaak met peper en zout.
2. Verdeel de spinazie en de tomaten over het gehakt, bedek met puree en bestrooi met feta. Verhit 10 minuten in het midden van de oven.

 
