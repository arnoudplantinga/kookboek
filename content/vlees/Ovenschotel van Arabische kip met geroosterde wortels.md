Voorbereiding 10 minuten

Kooktijd 35 minuten

## Ingrediënten voor 4 personen
- 500 gram kippendijen
- 600 gram (regenboog-) wortels
- 2 à 3 grote rode uien, in partjes
- 15 gram pijnboompitten
- 1/2 eetlepel komijnzaad
- 1/4 theelepel kaneel
- 1,5 eetlepel sumak + extra
- Handje verse peterselie of koriander, fijngehakt
- Olijfolie
- Zout & peper
- Griekse yoghurt
- Flatbread, naan of pita

## Bereiding 

Verwarm de oven voor op 190 graden.

Meng de kip met twee eetlepels olijfolie, komijn, sumak en kaneel. Voeg zout & peper toe.

Snijd de wortels in hapklare stukjes en verdeel ze over de bakplaat. Voeg ook de rode ui toe. Hussel er wat olijfolie en zout en peper doorheen. Schik de kip tussen de groenten.

Rooster de kip en groenten ongeveer 30 minuten in de voorverwarmde oven. Zet de temperatuur van de oven daarna naar 220 graden. Strooi de pijnboompitten erover en zet nog 5 minuten terug in de oven, zodat alles een mooi bruin korstje krijgt. Bak ook meteen het brood af in de oven, volgens de aanwijzingen op de verpakking.

Dien de Arabische kip op met yoghurt en flatbread, naan of pita. Bestrooi met de fijngehakte peterselie en extra sumak.
