
## Ingrediënten (4 personen)

- 6 el olijfolie
- 600 g kipfilet
- 450 g spruitjes (diepvries)
- 900 g aardappelpartjes met schil (zak à 450 g, diepvries)
- 4 tenen knoflook
- 50 g gepelde walnoten
- 1 tl gedroogde tijm

## Bereiden

1. Verwarm de oven voor op 200 °C. Vet de ovenschaal in. Breng de kipfilets op smaak met peper en zout en leg ze in het midden van de schaal.
2. Schep in een grote kom de spruitjes, aardappelpartjes, ongeschilde knoflook en de walnoten om met de gedroogde tijm en de olijfolie. Breng op smaak met peper en zout.
3. Schep de groenten om de kipfilets heen in de ovenschaal. Laat de kipfilets met de groenten in het midden van de oven in ca. 30 min. gaar worden.

**Bereidingstip:** Weetje! Door knoflook in de schil te laten garen, wordt hij lekker zacht en mildzoet van smaak. Je kunt de teentjes op je bord uitknijpen, de schil eet je niet.
