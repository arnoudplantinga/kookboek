Level Intermediate

Prep Time 10 minutes

Cook Time 40 minutes

Serves 6-8

## Ingredients
- 8 boneless skinless chicken thighs
- 1 pound chorizo, sliced
- 4 cups chicken stock
- 1/2 teaspoon saffron, ground
- 2 teaspoons olive oil
- 1 white onion, diced
- 4 cloves garlic, minced
- 2 1/2 cups Arborio rice
- 1 cup frozen peas
- 2 teaspoons paprika
- 1 teaspoon salt
- 1 teaspoon pepper

## Directions

1. Preheat oven to 400 degrees Fahrenheit. In a medium saucepan, heat chicken stock and saffron.
1. Heat 14 inch skillet over medium high heat, 4-5 minutes. Season chicken on all sides with salt, pepper, and paprika. Sear chicken for 5-6 minutes per side. Remove from pan and set aside.
1. Cook chorizo for 2-3 minutes, stirring often. Add onion and garlic and continue to cook until onion softens, 5-7 minutes. Stir in rice and add broth. Bring to a boil, stirring often.
1. Add peas and top with chicken. Bake until liquid is absorbed and chicken is cooked through, 20 minutes.
1. Serve and enjoy!
