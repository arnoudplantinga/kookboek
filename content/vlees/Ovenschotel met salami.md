## Ingrediënten

- 225 g mild gekruide salami
- 2 tenen knoflook
- 1 aubergine
- 1 kg krieltjes
- 2 zoete puntpaprika's
- 2 el traditionele olijfolie
- 1½ el gedroogde rozemarijn
- 250 g romaatjes
- 30 g verse peterselie
- Evt rode ui

## Bereiden

1. Verwarm de oven voor op 180 °C. Verwijder het vel van de salami en snijd in halve plakken. Snijd de knoflook fijn. Halveer de krieltjes en snijd de aubergine in stukjes van 1½ cm. Snijd de steelaanzet van de puntpaprika's, verwijder de zaadlijsten en snijd het vruchtvlees in ringen.
2. Meng de aubergine, puntpaprika, aardappel, knoflook, salami, olie, rozemarijn en peper en verdeel over een met bakpapier beklede bakplaat. Rooster 35 min. in het midden van de oven. Schep halverwege om. Halveer ondertussen de romaatjes en snijd de peterselie fijn. Neem de bakplaat uit de oven en schep de tomaat en peterselie erdoor.

Combinatietip:
Lekker met in ringen gesneden rode ui erdoor.
