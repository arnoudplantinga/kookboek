**INGREDIENTEN 5 PERSONEN**

- 600 gram rundergehakt  
- 600 gram sperziebonen, kleingesneden  
- 1 dikke snee wit brood, gekruimeld  
- 2½ dl melk  
- 1 ui, gesnipperd  
- 2 teentjes knoflook, geperst  
- 1 eetlepel kerriepoeder  
- 1 eetlepel [kurkuma](http://www.smulweb.nl/wiki/183/Kurkuma) (koenjit)  
- 1 eetlepel garam masala (Indisch kruidenmengsel)  
- 1 eetlepel kaneel  
- 30 ml azijn  
- 2 eetlepels abrikozenjam  
- 2 eetlepels mangochutney  
- 50-100 gram rozijnen  
- 3 eieren  
- 2 appels, in stukjes gesneden  
- 50 gram amandelen, fijn gehakt  
- olijfolie   
- zout en [peper](http://www.smulweb.nl/wiki/205/Peper)

**Voorbereiding**

1. Fruit de kurkuma, garam masala en kerriepoeder kort in hete olie.
2. Zet het vuur wat lager en voeg de ui toe. Fruit deze goudbruin en voeg dan de knoflook en het gehakt toe.
2. Mix ondertussen in een kom het brood, azijn, chutney, jam, kaneel, appel, amandelen, rozijnen en de helft van de melk (125 ml).
2. Als het gehakt gaar is, kunnen de sperziebonen, de appelstukjes en de mix worden toegevoegd aan het vlees.

**Bereidingswijze**

1. Vul een ovenschaal met het mengsel en dek dit af met een mengsel van 3 eieren en de rest van de melk.
2. Bak alles 35 tot 45 minuten op 175° C in de oven. De bobotie is klaar als het eimengsel goudbruin is geworden.

**Serveertips**

Serveer met gele rijst en een frisse salade. Zet eventueel nog wat mangochutney op tafel.
