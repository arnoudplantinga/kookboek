## Ingrediënten

Voor 2-3 personen:

- 1 ui, fijngehakt
- 1 teen knoflook, fijngehakt
- 250 gram kimchi, fijngehakt (van de toko of zelfgemaakt)
- 3 eetlepels kimchi-vocht
- optioneel: 1 theelepel gochujang (Koreaanse peperpasta)
- 500 gram gekookte rijst, koud
- 2 theelepels sesamolie
- 1 eetlepel koriander, fijngehakt
- 1-2 bosuitjes in ringen
- 1-2 theelepels sesamzaadjes
- Plantaardige olie om in te bakken
- Zout en peper
- 2 of 3 eieren
- 200g (buik)spek

## Bereiding

Zet een wok of grote koekenpan op het vuur met een scheut olie en laat heet worden. Bak het spek krokant. Voeg de ui toe en laat op middelhoog vuur zacht worden. Bak na twee minuten voor heel even de knoflook mee en voeg de fijngehakte kimchi en het kimchi-vocht toe. Voor meer pit doe je er nog een theelepel gochujang bij, maar dat mag je zelf beslissen.

Bak de kimchi vijf minuten en voeg dan de koude rijst toe. Schep goed door elkaar. En bak nog 3-5 minuten op hoog vuur. Breng op smaak met de sesamolie, zout en peper. Draai het vuur uit.

Zet een andere koekenpan met olie op het vuur. Bak de eieren sunny side up.

Schep de kimchi fried rice op borden of in kommen. Bestrooi met sesamzaadjes, bosui en/of korianderblaadjes. Leg de eieren er bovenop.
