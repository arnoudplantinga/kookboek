## Ingrediënten

- Roti 4 stuks
- Kipdijfilet 350g
- 200 ml water
- Tomaten 400g
- (Tomatenpuree 70ml)
- Sperziebonen 150g
- Zoete aardappel 1 stuk
- Aardappels 2 stuks
- Ui 1
- Knoflook 1 á 2 teentjes
- Laurierblad
- Kruidenmix
    - Paprikapoeder
    - Zout
    - Kurkuma
    - Koriander
    - Komijn
    - Peper
    - Gember
    - Kaneel
    - Nootmuskaat
    - Fenegriek
    - Venkelzaad
    - Karwijzaag
    - Chilipeper
    - Piment
    - Kruidnagel
    - Lavasblad
    - Foelie

## Bereidingswijze

1. Maak de groenten schoon. Dop de sperziebonen en snijd in tweeen. Pel de ui en de knoflook en snipper deze fijn. 
2. Schil de aardappelen en de zoete aardappel en snijd deze in blokjes van 1 cm. Snijd de tomaten in blokjes van 1 cm.
3. Verhit intussen de olie in een wok of hapjespan en bak de kip in 3 minuten rondom bruin en voeg vervolgens de ui en de knoflook toe en bak dit ca. 2 minuten mee op matige temperatuur
4. Voeg de tomatenpuree en de kruidenmix toe en fruit deze ca. 1 minuut mee. Voeg de tomatenblokjes, 200 ml water, de blokjes aardappel zoete aardappel en het laurierblad toe en breng de saus aan de kook en laat deze 8 minuten zacht doorkoken.
5. Breng ondertussen in een andere pan water aan de kook. Doe de sperziebonen in de pan en kook in ongeveer 5-minuten beetgaar.
6. Giet de beetgare sperziebonen af, schep deze door de saus en laat het geheel nog 4 minuten zacht stoven tot de aardappel gaar is.
7. Verwarm intussen de rotivellen 30 seconden per zijde in een voorverwarmde koekenpan.
8. Verwijder het laurierblad en serveer de warme roti erbij.

## Tips

- Houd je van pittig doe er dan rode peper door 
- Strooi er wat geroosterde cashewnoten en gewelde rozijnen over
