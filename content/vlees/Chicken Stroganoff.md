**Ingredients**

- 1 chicken breast, cut intro strips
- 1 tbsp olive oil
- knob of butter
- 2 shallots, finely sliced
- 50g mushrooms, quartered
- 200ml chicken stock
- 100ml sour cream
- 2 tsp paprika
- 100g basmati rice
- 2 tbsp parsley, finely chopped
- salt and freshly ground black
- pepper

 

**Method**

1. Cook the rice as per packet instructions and than toss the parsley through.
2. Heat the olive oil and butter in a frying pan. Add the chicken and fry until cooked through. Remove with a slotted spoon and set aside.
3. Add the shallots to the pan, adding more olive oil or butter if needed, and heat gently until softened. Add the mushrooms and cook for a further 2 minutes or so.
4. Pour in the chicken stick and turn the heat up to high. Reduce the stock by half and then turn the heat down to a simmer. Add the chicken to the pan, along with the sour cream and stir through. Season and simmer until the cream has thickened slightly.
5. Serve, accompanied by the rice and sprinkle 1 tsp of the paprika over the stroganoff on each plate.

 

 
