
## Ingrediënten voor 4 personen

- 125 g	roomboter
- 500 g	pompoenhutspot met wortel en ui
- 500 g	stamppotaardappelen
- 500 g	rundergehakt
- 6 takjes	tijm
- 50 g	Parmezaanse kaas
- 100 g	bloem

## Bereidingswijze

1. Verwarm de oven voor tot 220 °C. Breng een pan water voor de aardappelen aan de kook. Verhit in een koekenpan 25 g boter en fruit de ui uit de zak hutspotgroenten in 3 min. glazig.
1. Kook de aardappelen in 12 min. gaar. Voeg intussen het gehakt toe aan de ui en ris de blaadjes van 2 takjes tijm boven de pan. Bak het gehakt in 5 min. bruin en gaar. Breng op smaak met versgemalen peper. Schep het gehakt in de ovenschaal.
1. Verhit opnieuw 25 g boter in de koekenpan en bak de pompoen met de wortel 5 min. Ris de blaadjes van 3 takjes tijm boven de pan en schep om. Stoof de pompoen in 5 min. gaar met het deksel op de pan. Rasp intussen de kaas boven een kom.
1. Meng de geraspte kaas met de bloem en versgemalen peper. Kneed er met de vingertoppen van koude handen de rest van de boter door, tot een kruimeldeeg ontstaat. Zet tot gebruik afgedekt in de koelkast.
1. Schep het pompoen wortelmengsel op het gehakt in de ovenschaal. Giet de aardappelen af en vang een kopje kookvocht op. Stamp de aardappelen met een beetje kookvocht tot een smeuïge puree.
1. Verdeel de aardappelpuree over de groente in de ovenschaal en bestrooi met het kruimeldeeg. Bak de pompoenhutspot crumble in 10-15 min. goudgeel en knapperig in de voorverwarmde oven.
1. Garneer met blaadjes tijm.
