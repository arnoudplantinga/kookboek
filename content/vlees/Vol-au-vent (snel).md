

## Ingrediënten
voor 4 personen

-   3 kipfilets
-   4 bladerdeegbakjes
-   250 g champignons
-   400 ml kippenbouillon
-   100 ml room
-   2 el bladpeterselie
-   boter
-   1½ el bloem
-   20 g sesamzaad
-   peper
-   zout
-   Gehaktballetjes gekookt in water met tijm en een laurierblad
-   Citroensap in de saus**\


## Bereidingswijze

1

Verwarm de oven voor op 150°C.

2

Snij de kip in blokjes. Maak de champignons schoon en snij ze in
plakjes. Bak ze heel even in boter en voeg er dan de kip en de bouillon
aan toe. Laat 10 minuten zachtjes garen.

3

Verwarm ondertussen de bladerdeegbakjes 10 minuten in de oven.

4

Meng 1,5 el boter met 1,5 el bloem en doe dit al roerend bij de kip.
Breng aan de kook en laat al roerend een minuutje doorkoken. Doe er de
room en het citroensap bij.

5

Vul de deegbakjes met de vol-au-vent, bestrooi
metbladpeterselie en sesamzaadjes en serveer meteen.

 

Bron: 

 

 
