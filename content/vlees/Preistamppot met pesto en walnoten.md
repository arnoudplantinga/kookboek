

**Ingrediënten**

-   1/2 zakje walnoten (\` 45 g)
-   100 g cheddar (kaas)
-   1 eetlepel boter of margarine
-   1 zakje panklare prei (300 g)
-   1 zakje aardappelpuree voor stamppot
    met baconsmaak (pak \` 224 g Maggi)
-   1 eetlepel groene pesto

 

 

**Bereiden**

Walnoten grof
hakken. Kaas in kleine blokjes snijden. In pan <span
style="color:#494949;">boter verhitten, prei ca. 4 minuten op hoog vuur
al omscheppend bakken. Puree bereiden volgens gebruiksaanwijzing. Prei,
kaas, walnoten en pesto erdoor scheppen.

 

 
