
## Ingrediënten

- Bladerdeeg
- Courgette
- Aubergine
- Paprika
- Gehakt
- Arabische kruidenmix
- Olijfolie

## Bereiding

1. Ontdooi het bladerdeeg
2. Verwarm de oven voor op 200 graden
3. Verdeel bladerdeeg over een ovenschotel
4. Kruid het gehakt en maak drolletjes
5. Snijd de courgette en aubergine in lange plakken, en de paprika in dunne reepjes
6. Verdeel de groente en gehaktdrolletjes over de ovenschaal, met af en toe wat kruiden en olijfolie er tussen door
7. Bak ca. 35 minuten in de oven
