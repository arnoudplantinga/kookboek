[](http://s1279.photobucket.com/user/lekkerensimpel/media/PicMonkeyCollage_zps1c02bf4d.jpg.html)

_Zin in een lekkere avondmaaltijd maar wil je niet teveel tijd in de keuken doorbrengen? Dit recept voor kip met pesto uit de oven heb je binnen 15 tot 20 minuten gemaakt en dan hoeft het alleen nog maar een halfuurtje in de oven. Serveer er aardappeltjes bij (die je ook in de oven schuift) en een salade en klaar!_

_Recept voor 2-3 personen  
Tijd: 15-20 min. + 35 min. in de oven_

**Benodigdheden:**

* 2 kipfilets
* 1 klein potje pesto (90 gr)
* mozzarella of geraspte kaas
* snufje zout/peper

**Bereidingswijze:**

1. Verwarm je oven voor op 190 graden.
2. Snijd de kipfilets in twee of 3 lange repen en breng je ze op smaak met een snufje zout en peper.
2. Vet een ovenschaal en verdeel de helft van de pesto over de bodem van de ovenschaal. Leg de kipfilets op de pesto en verdeel de rest van de pesto over de kip.
2. Dek de ovenschaal af met aluminiumfolie en zet de kip voor circa 30 minuten in de oven totdat de kip gaar is.
2. Haal dan het aluminiumfolie eraf en verdeel wat geraspte kaas of wat plakjes mozzarella over de kip.
2. Zet de ovenschaal voor circa 5 minuten terug in de oven totdat de kaas gesmolten is.
2. Serveer de kip met bijvoorbeeld [knoflookaardappeltjes](http://www.lekkerensimpel.com/2014/03/17/knoflookaardappeltjes-uit-de-oven/) of rijst.

_Bron: Kalyn’s Kitchen_
