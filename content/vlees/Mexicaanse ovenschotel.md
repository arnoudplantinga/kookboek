#### Ingrediënten

-   [1 blik kidneybonen (400 g)]( "zet ingrediënt op mijn lijst")
-   [1 blik maïskorrels (300 g)]( "zet ingrediënt op mijn lijst")
-   [150 g peppadewpepertjes (pot
    420 g)]( "zet ingrediënt op mijn lijst")
-   [2 eetlepels olijfolie]( "zet ingrediënt op mijn lijst")
-   [300 g rundergehakt mager (schaal
    450 g)]( "zet ingrediënt op mijn lijst")
-   [2 eetlepels cajunkruiden (potje
    50 g)]( "zet ingrediënt op mijn lijst")
-   [1/2 zak tortillachips original (a
    150 g)]( "zet ingrediënt op mijn lijst")
-   [150 g Goudse kaas belegen gemalen]( "zet ingrediënt op mijn lijst")
-   [komkommer]( "zet ingrediënt op mijn lijst")

 

#### Bereiden

1. Grill voorverwarmen.
2. Kidneybonen, mais en peppadew laten uitlekken. Peppadew in vieren snijden.
2. In braadpan 1 el olie verhitten. Gehakt 3 min. rulbakken. Cajunkruiden, kidneybonen en peppadew toevoegen en nog enkele min. verwarmen.
2. Gehaktbonenmengsel in ovenschaal doen en bedekken met laag tortillachips. Bestrooien met gemalen kaas. Ovenschotel enkele min. onder grill zetten, zodat kaas smelt.
2. Intussen komkommer in blokjes snijden en mengen met mais. Besprenkelen met 1 el olijfolie en op smaak brengen met versgemalen peper en zout.
2. Komkommer-maissalade serveren bij Mexicaanse ovenschotel.
 
