![Pizzadeeg zelfzijdend
bakmeel](http://www.pizzarecepten.net/wp-content/uploads/2013/06/zelfrijzendbakmeelpizza.jpg)

-   300 gram zelfrijzend bakmeel
-   1/2 theelepel zout
-   1 ei
-   circa 1 deciliter water

**Bereiding:**

1. Doe het zelfrijzend bakmeel in een kom en maak een kuiltje in het midden.
2. Doe het ei, het zout en 1 deciliter water in het kuiltje.
2. Roer met een vork tot al het vocht is opgenomen.
2. Kneed verder met de handen tot een soepele deegbal, voeg indien nodig nog een klein beetje water toe.
2. Laat het deeg circa 1/2 uur rusten (dan rolt het makkelijker uit) of ga meteen verder:


Pizza
=====
 

- 2 pounds, 3 ounces (1 kilogram) strong bread flour
- 1 ounce (30 grams) sugar
- 1 ounce (30 grams) salt
- 1 ounce dried yeast
- 1 pint (565 millilitres) tepid water
- 2 tablespoons olive oil
- Toppings, recipes follow

Put the flour onto a work surface or use a bowl, if you are short of space. Using your fingers, make a big well in the middle of the flour. Add the sugar, salt and yeast then pour in the tepid water and olive oil. Using a fork, make circular movements from the centre moving outwards, slowly bringing in more of the flour until all the yeast mixture is soaked up. This should be starting to look like dough now so you can start to work and knead it until it is smooth. This should take around 4 minutes. Roll out to a sausage shape and divide into 8 or 10 balls, depending on how large you want the pizzas to be.

Flour the surface and roll each pizza out to about the thickness of 3 beer coasters (1/3 of an inch). They don't have to be perfectly round, they should look home-made. Place each pizza on a lightly oiled and floured piece of tin foil. Flour the top of the pizza, placing another on top of it. Flour that and repeat this until all the pizzas are stacked together. These can be frozen for a couple of months, placed in the fridge for 10 hours, or cooked straight away. Lightly top with your chosen topping (the simpler the better) and bake directly on the oven bars for 10 minutes at your oven's highest temperature.


*Toppings:*

Tomato:
- 8 plum tomatoes
- Sprinkle dried oregano
- Drizzle olive oil*

For enough to cover eight bases, chop and de-seed plum tomatoes, dry with kitchen paper, sprinkle with a little dried oregano and a drizzle of olive oil.

*Various topping ingredients:*
- Sliced mozzarella
- Rocket (Arugula)
- Parmesan
- Olives
- Sun-dried tomatoes
- Artichoke hearts
- Prosciutto
- Panchetta
- Any interesting cheeses - use your imagination*

Yield: 8 (10-inch) pizzas, 16 servings

Prep Time: 10 minutes

Cook Time: 1 hour 10 minutes

Difficulty: Medium

 
