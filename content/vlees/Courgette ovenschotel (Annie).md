
## Benodigdheden:

courgette, gehakt en aardappelen

 

## Werkwijze:

De courgette (schillen en) in blokjes snijden, een beetje stoven in wat
boter en kruiden volgens smaak (peper, zout, knor aromat).

Daarna in een ovenschotel gieten.

Dan het gehakt bakken met een ajuintje en vervolgens over de courgette
verdelen.

Aardappelpuree bereiden (aardappelen koken, sap afgieten, fijn maken met
de stamper, dan een beetje boter, eierdooier en melk bijvoegen, kruiden
met peper, zout en nootmuskaat.)

Wanneer de puree klaar is, over het gehakt aanbrengen en vervolgens
bestrooien met gemalen kaas.

Dan in de oven plaatsen en opwarmen op combinatiestand (Grill en
microgolf) 180 graden, 15 à 20 minuten (kijken als het korstje bruin is)
