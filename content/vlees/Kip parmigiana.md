4 personen

680
kcal [voedingswaarden](http://www.ah.nl/allerhande/recept/R-R900688/chicken-parmigiana#)

-   35 min. bereiden
-   15 min. wachten
-   15 min. oventijd

 

#### Ingrediënten

-   [800
    g aardappelen (vastkokend)]( "Verwijder dit ingrediënt uit selectie")
-   [1 snee witbrood]( "Verwijder dit ingrediënt uit selectie")
-   [100 g Parmezaanse kaas]( "Verwijder dit ingrediënt uit selectie")
-   [1 ei]( "Verwijder dit ingrediënt uit selectie")
-   [2 el bloem]( "Verwijder dit ingrediënt uit selectie")
-   [345 g kipfilethaasjes]( "Verwijder dit ingrediënt uit selectie")
-   [5 el olijfolie]( "Verwijder dit ingrediënt uit selectie")
-   [400 g tomatenblokjes]( "Verwijder dit ingrediënt uit selectie")
-   [350 g tomatensaus (tomato frito, pakje à
    350 g)]( "Verwijder dit ingrediënt uit selectie")
-   [1 bol mozzarella (zakje à
    125 g)]( "Verwijder dit ingrediënt uit selectie")
-   [2 tenen knoflook]( "Verwijder dit ingrediënt uit selectie")
-   [15 g tijm (bakje à 15 g)]( "Verwijder dit ingrediënt uit selectie")

 

#### Bereiden

1.  Verwarm de oven voor op 200 °C. Schil de aardappelen en snijd
    in plakjes. Kook de aardappelen 10 min. in water met zout en
    giet af.
2.  Rooster ondertussen het brood en verkruimel het boven een diep bord.
    Rasp de Parmezaanse kaas en voeg toe aan het brood. Klop in een
    ander diep bord het ei los met een vork. Doe de bloem in een
    derde bord. Bestrooi de kip met peper en zout en wentel
    achtereenvolgens door de bloem en het ei en bedek helemaal met
    het broodmengsel.
3.  Verhit olie in de koekenpan. Bak de kip in 4 min. goudbruin.
    Keer halverwege. Meng de tomatensaus en de tomatenblokjes in een
    ovenschaal en leg de kip erop. Snijd de mozzarella in plakken en leg
    ze op de kip. Bak ca. 15 min. in de oven.
4.  Snijd de knoflook fijn. Ris de blaadjes van de tijm. Verhit de rest
    van de olie in een grote koekenpan, fruit de knoflook 1 min. en voeg
    de tijm en aardappelen toe. Bak in 15 min. goudbruin op
    middelhoog vuur. Schep om.

 

#### Variatietip:

Vervang de kipfilet door filetlapjes à la minute voor een parmigiana met
varkensvlees.

 
