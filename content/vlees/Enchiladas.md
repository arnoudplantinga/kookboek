
## Ingrediënten
- 350 kipfilet of gerookte kip
- 1 ui
- 2 tenen knoflook
- 100 gr geraspte kaas
- 4-5 wraps
- 2 paprika’s (bijv. een rode en een gele)
- 400 gr tomatenblokjes
- 3 tl Mexicaanse kruiden
- gebakken uitjes

## Bereiden

Begin met het voorverwarmen van de oven op 200 graden. Snijd de kipfilet, ui, knoflook en paprika in stukjes. Breng de kip op smaak met Mexicaanse kruiden. Giet een scheutje olie in een (hapjes)pan en bak de kip rondom bruin. Voeg daarna de ui, knoflook en paprika toe. Bak dit ongeveer 5-10 minuten.

Daarna kunnen de tomatenblokjes erbij en wat extra Mexicaanse kruiden. Laat de saus nog 5 minuten zachtjes pruttelen.

Zet het vuur uit. Haal de wraps lichtjes door de saus. Schep 2-3 eetlepels van het kipmengsel op een wrap. Strooi wat geraspte kaas er overheen. Rol de wrap op en leg hem in een ingevette ovenschaal.


Maak op deze manier de rest van de wraps. Strooi wat geraspte kaas over de bovenkant van de wraps. Zet de enchiladas met kip 15 minuten in de oven en serveer met wat gebakken uitjes er overheen.
Enchiladas met kip
