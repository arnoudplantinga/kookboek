
## Ingrediënten

- 4 kipfilets
- zakje kip fajita kruiden
- geraspte belegen 48+ kaas
- 1 ui
- 3 kleuren paprika
- 2 teentjes knoflook
- 250 ml gezeefde tomaten
- 200 ml crème fraiche
- zout en versgemalen peper
- verse koriander
- eventueel rijst


## Stap 1 Voorbereiding
1. Snijd de paprika’s door de helft en maak ze schoon door de zaadlijsten te verwijderen. Snijd de paprika’s in repen.
1. Snipper de ui en de knoflook.
1. Neem een kom en voeg hierin drie eetlepels olijfolie samen met een half zakje fajita kruiden en roer het goed door elkaar.
1. Maak de kipfilet schoon en meng deze met de fajita marinade. Dek af en zet minimaal 2 uur in de koeling.

## Stap 2 Bereiding Kip Fajita
1. Maak de barbecue aan en zorg voor een temperatuur zones om direct te kunnen grillen met een keteltemperatuur van rond de 200 graden.
1. Verhit een skillet op hoog vuur en bak de kipfilet rondom bruin in een scheut olijfolie.
1. Haal de kipfilet uit de pan en leg deze op een bord.
1. Breng de temperatuur van de barbecue iets omlaag zodat de onderstaande stappen niet te snel gaan. Je kunt dit doen door de deksel te sluiten van de barbecue en all luchttoevoer even dicht te zetten. Je kunt ook de kolen iets meer verdelen zodat er niet teveel onder de skillet liggen.
1. Fruit nu de ui aan op middel heet vuur en voeg na een minuut ongeveer de knoflook toe.
1. Wanneer je er rijst bij wil serveren is het nu tijd om de rijst te koken.
1. Voeg de paprika’s toe en bak het geheel tot de paprika’s beet gaar zijn (dit duurt een aantal minuten)
1. Schenk de gezeefde tomaten toe samen met de resterende fajita kruiden en meng alles goed door elkaar.

## Stap 3
1. Roer nu de crème fraiche door het mengsel en leg de kipfilet terug in de pan. Bedek de kipfilet onder het mengsel en bak tot de kipfilet gaar is.
1. Strooi de geraspte kaas over de kip fajita en zet deze vervolgens even in de barbecue en sluit de deksel tot dat de kaas gesmolten is. Maak het af door er een beetje verse koriander overheen te strooien.
1. Serveertip: Serveer de kip fajita met witte rijst en je maaltijd is compleet.
