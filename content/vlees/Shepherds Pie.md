Sheperd's Pie is een eeuwenoud Engels recept. Van oorsprong werd het gemaakt met restjes vlees en aardappel. Traditioneel wordt lamsgehakt gebruikt. In plaats daarvan is rundergehakt heel goed te gebruiken, al heet het gerecht dan officieel Cottage Pie.

## Ingrediënten

Voor de vulling:

- 675 gr gehakt (lam of rund)
- 1 grote winterpeen, geraspt
- 1 grote ui, gesnipperd
- olijfolie
- klein blikje tomatenpuree
- scheutje rode wijn
- Worcestershiresauce
- 4 tenen knoflook of 1 tl knoflookpoeder
- 1 el verse of gedroogde rozemarijn
- 1 el verse of gedroogde tijm
- versgemalen peper en zout naar smaak

Voor de aardappelpuree:

- 675 gr aardappelen
- scheutje slagroom
- 3½ tl boter
- 2 eigelen
- geraspte Parmezaanse (of belegen Goudse) kaas
- versgemalen peper en zout naar smaak


## Bereidingswijze

1. Kook de aardappels in een ruime hoeveelheid water tot ze gaar zijn. Giet ze af, voeg alle ingrediënten voor de aardappelpuree toe en maak er een mooie, zachte puree van. Gebruik zo nodig wat extra slagroom, melk of boter tot de puree de consistentie heeft die gewenst is.
2. Verwarm de oven voor op 200 graden.
3. Verhit de olijfolie in een flinke pan. Doe het gehakt in de pan en blijf erin roeren tot het gehakt bruin is en verdeeld in kleine stukjes. Voeg fijngesneden knoflook, tijm en rozemarijn toe en roer even door. Voeg dan ook de wortel en ui toe en blijf nog even roeren.
4. Voeg Worcestershiresaus naar smaak toe, tomatenpuree en een flinke scheut rode wijn en laat een minuut of 5 doorpruttelen.
5. Schep het gehaktmengsel in een ovenschaal. Doe de aardappelpuree er overheen en strijk glad met de bolle kant van een lepel. Verdeel lekker veel kaas over de aardappelpuree.
6. Zet de Sheperd's Pie ongeveer 20 minuten in de oven, tot de kaas gesmolten is en de aardappelpuree een bruin korstje heeft.
