Ingrediënten
------------

Voor 4 personen

-  1 cm         verse gember (fijngehakt)
-  2            bosuien (fijngesnipperd)
-  2 teentjes   knoflook (fijngehakt)
-  250 g        snijboon (in schuine stukken gesneden)
-  250 g        shii-take (in schuine stukken gesneden)
-  1 bakje      tofugehakt
-  1 blik       bamboescheuten
-  125 g        sojascheut
-               olie

Saus

-  2 el   sojasaus
-  1 tl   suiker
-  2      druppels sesamolie
-  1 el   zoete chilisaus
-  1 tl   maizena

Bereidingswijze
---------------

1. Meng alle ingrediënten voor de saus in een kom.Verwarm in een wok een scheutje olie. Voeg de gember, bosui en knoflook toe, en roerbak 30 seconden.
2. Voeg de snijbonen en shii-takes toe met een half glas water.
2. Voeg als de snijbonen gaar zijn het tofugehakt, bamboe en taugé toe.
2. Kook het geheel 2 minuten.
2. Schenk de saus erbij, roer nog even goed door en serveer.

 
