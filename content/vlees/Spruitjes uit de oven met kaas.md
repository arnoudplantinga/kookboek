---
tags: []
---



-   *Bereidingstijd:* <span style="font-size:100%;" title="PT35M">35
    minuten</span>
-   *Aantal personen:* <span style="font-size:100%;">2</span>
-   *Beoordeling:* <span style="font-size:100%;"><span
    style="font-size:100%;">3.75</span> sterren - gebaseerd op<span
    style="font-size:100%;"> 20</span> review(s)</span>

#### Ingrediënten:

-   Spruitjes
-   Ui
-   Spekjes
-   Champignons
-   Knoflook
-   Geraspte kaas

####  

#### Bereidingswijze:

1.  Vul de waterkoker en zet aan
2.  Snij de kapjes van de spruitjes en
    snij de spruitjes doormidden
3.  Doe de spruitjes in een pan, giet het kokende water er overheen en
    laat 8 minuten koken, totdat de spruitjes beetgaar zijn
4.  Zet de oven aan op 175 graden
5.  Snipper terwijl de spruitjes koken de ui fijn, snij de knoflook
    klein en de champignons in blokjes
6.  Bak de ui, knoflook en spekjes drie minuten in een wok
7.  Wok de champignons nog even mee en giet de spruitjes af
8.  Bak ook de spruitjes even mee, strooi er flink wat paprikapoeder,
    een beetje nootmuskaat en peper naar smaak bij
9.  Vet een ovenschaal in en leg het spruitenmengsel in de schaal
10. Roer de crème fraiche er doorheen
11. Strooi de geraspte kaas er overheen en zet de schaal 15 minuten in
    de oven
12. Eet smakelijk!

 
