---
tags: []
---



- 4 personen
- 600 kcal [voedingswaarden](http://www.ah.nl/allerhande/recept/R-R767224/kip-gestoofd-in-witte-wijn#)
-   30 min. bereiden
-   30 min. wachten


-   [50 g boter]( "Verwijder dit ingrediënt uit selectie")
-   [1 kg kippenpoot zonder
    rugstuk]( "Verwijder dit ingrediënt uit selectie")
-   [3 preien]( "Verwijder dit ingrediënt uit selectie")
-   [4 tenen knoflook]( "Verwijder dit ingrediënt uit selectie")
-   [2 takjes tijm]( "Verwijder dit ingrediënt uit selectie")
-   [250 ml droge witte wijn]( "Verwijder dit ingrediënt uit selectie")
-   [2
    kippenbouillontabletten]( "Verwijder dit ingrediënt uit selectie")
-   [500 ml water]( "Verwijder dit ingrediënt uit selectie")
-   [200 g crème fraîche]( "Verwijder dit ingrediënt uit selectie")
-   [20 g platte peterselie]( "Verwijder dit ingrediënt uit selectie")


1.  Snijd de kippenpoten een paar keer in tot op het bot. Verhit de
    boter in een braadpan en braad de kippenpoten in 4 min.
    rondom bruin.
2.  Was ondertussen de prei en snijd in halve ringen. Snijd de knoflook
    in plakjes. Neem de kip uit de pan en laat het bakvet zitten. Bak de
    prei en knoflook 4 min. in het achtergebleven bakvet.
3.  Ris de blaadjes van de takjes tijm. Voeg samen met de wijn,
    bouillontabletten en het water toe aan de prei. Leg de kip terug in
    de pan en breng aan de kook. Zet het vuur laag en stoof de kip
    zonder deksel op de pan in ca. 30 min. gaar.
4.  Neem de kip uit de pan en laat iets afkoelen. Zet het vuur hoger en
    laat het kookvocht 10 min. inkoken. Verwijder ondertussen het vel
    van de kip en haal het vlees van het bot.
5.  Meng de crème fraîche door het kookvocht en breng op smaak met peper
    en eventueel zout. Doe het kippenvlees erbij en warm nog even door
    zonder te koken. Snijd de peterselie fijn, strooi erover en
    serveer direct.

#### Bewaartip:

Je kunt dit gerecht 1 dag van tevoren bereiden. Bewaar afgedekt in de
koelkast. Verwarm 10 min. op laag vuur.

 
