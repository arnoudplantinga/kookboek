## Ingredients

- 1 varkenshaas  van ca. 300 gram
- 100–150 gr ontbijtspek
- versgemalen peper
- 1 tak rozemarijn
- 2 takjes tijm
- 2 rode uien
- 400 gram (kastanje) champignons,
- olijfolie of boter, om in te bakken.

## Instructions

- Verwarm de oven voor op 180 graden / 160 hete lucht. Vet de ovenschaal licht in met olie.
- Verhit ondertussen de koeken- of hapjes pan op middelhoog vuur en bak de varkenshaas heel kort (!) rondom aan. Het gaat hierbij puur om het dichtschroeien van de buitenkant – zo blijven de sappen het beste bewaard namelijk. Zet daarna het vuur uit en laat iets afkoelen.
- Leg de plakken spek uit op het aanrecht (of een snijplank), het liefst in een V-vorm en waarbij de uiteindes een beetje overlappen. Bestrooi dit met versgemalen peper.
- Ris de tijm en rozemarijn van de takjes en hak dit grof. Verdeel dit ook over de spek.
- Leg nu de gebakken varkenshaas in het midden van de spek lap en vouw de plakken spek om de varkenshaas heen:  je pakt hem dus als het ware in. Plaats hierna in de ovenschaal.
- Pel de ui en snijd in parten. Boen de champignons schoon en snijd in kwarten. Hussel deze om met een scheutje olijolie, peper en (beetje) zout, en rangschik om de varkenshaas heen.
- Bak de varkenshaas in de voorverwarmde oven gedurende 20-25 minuten. Snijd de varkenshaas uit de oven daarna in plakken en serveer direct.
