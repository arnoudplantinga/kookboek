
## Ingrediënten 4 personen

- 750 g rib-runderlappen
- 2 uien
- 250 g winterpeen
- 3 tenen knoflook
- 750 ml volle rode wijn
- 180 g ontbijtspek aan een stuk
- 70 g tomatenpuree (blikje)
- 2 takjes verse tijm
- 2 gedroogde laurierblaadjes
- 30 g peterselie
- 250 g champignons (bakje)

## Bereiden

1. Snijd de riblappen in stukken van 2 x 2 cm. Snijd de uien in ringen, winterpeen en knoflook in plakjes. Meng de stukken rundvlees, uien, wortel en knoflook in een kom en schenk de wijn erover. Laat minimaal 2 uur afgedekt in de koelkast marineren. Haal 30 min. voor gebruik uit de koelkast. Schep het vlees uit de marinade en dep droog met keukenpapier.
2. Bak het in reepjes gesneden ontbijtspek in een braadpan zonder olie of boter op laag vuur in 10 min. uit. Schep uit de pan. Doe het rundvlees in de pan en bak goudbruin. Schep uit de pan. Haal de groenten uit de marinade, doe in dezelfde pan en bak zachtjes 5 min. Bak de tomatenpuree 1 min. mee. Voeg rundvlees, spek en marinade toe en breng zachtjes aan de kook.
3. Bind de tijm, laurierblaadjes en de helft van de peterselie bij elkaar met een stukje keukentouw. Voeg dit bouquet toe aan het vlees. Laat 2 uur en 30 min. zonder deksel op de pan zachtjes stoven.
4. Veeg de champignons schoon en doe ze in de pan. Laat de zilveruitjes uitlekken en voeg toe. Laat het vlees en de groenten nog 30 min. sudderen.
5. Proef of het vlees mals is en breng eventueel op smaak met peper en zout. Snijd de blaadjes van de rest van de peterselie fijn en strooi over de boeuf bourguignon.
