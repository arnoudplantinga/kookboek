Kip à la nor­man­de
===================

Gestoofd in calvados en appelcider en geserveerd met gebakken appel. Kip ik houd je!

- hoofdgerecht
- 4 personen
- 590 kcal [voedingswaarden](http://www.ah.nl/allerhande/recept/R-R665328/kip-a-la-normande#)
-   35 min. bereiden
-   20 min. wachten


-   [4 kippenpoten zonder rugstuk]( "zet ingrediënt op mijn lijst")
-   [100 g koude boter]( "zet ingrediënt op mijn lijst")
-   [30 ml calvados (slijter)]( "zet ingrediënt op mijn lijst")
-   [4 sjalotten]( "zet ingrediënt op mijn lijst")
-   [2 tenen knoflook]( "zet ingrediënt op mijn lijst")
-   [250 g champignons (bakje)]( "zet ingrediënt op mijn lijst")
-   [550 ml appelcider]( "zet ingrediënt op mijn lijst")
-   [125 ml crème fraîche (bekertje)]( "zet ingrediënt op mijn lijst")
-   [1 kippenbouillontablet]( "zet ingrediënt op mijn lijst")
-   [1 appel]( "zet ingrediënt op mijn lijst")


1.  Bestrooi de kippenpoten met peper en zout. Verhit een kwart van de
    boter in een braadpan en bak de kippenpoten in ca. 10 min.
    goudbruin. Schenk de calvados over de kip. Haal de kip uit de pan.
2.  Snijd de sjalotten in kwarten. Fruit ze daarna in dezelfde pan in
    3 min. Snijd de knoflook fijn. Maak de champignons schoon met
    keukenpapier en snijd in kwarten. Voeg de knoflook en de champignons
    toe en bak 5 min. Voeg de cider, crème fraîche, het bouillontablet
    en de kip toe. Laat met de deksel op de pan 20 min. stoven.
3.  Haal de kip opnieuw uit de pan en laat het vocht op hoog vuur tot
    ⅓ inkoken. Schil ondertussen de appel en snijd in kwarten. Verwijder
    het klokhuis en snijd elk partje in 3 stukken. Verhit nog een kwart
    van de boter in een koekenpan en bak de appel goudbruin.
4.  Snijd de rest van de boter in blokjes. Haal de saus van het vuur en
    voeg, al kloppend met een garde, beetje bij beetje de boter toe tot
    de saus vol en glanzend is. Breng de saus op smaak met peper
    en zout. Voeg de kip en de appel toe en serveer direct.

 

#### combinatietip:

Lekker met gekookte worteltjes en gebakken aardappelen.

 
