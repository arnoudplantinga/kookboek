#### Ingrediënten:

-   [Kipfilet](http://www.voedzaamensnel.nl/tag/kipfilet/) - 275 gram
-   [Ui](http://www.voedzaamensnel.nl/tag/ui/) - 1
-   [Peper](http://www.voedzaamensnel.nl/tag/peper/) - snuf
-   [Zout](http://www.voedzaamensnel.nl/tag/zout/) - snuf
-   [Olijfolie](http://www.voedzaamensnel.nl/tag/olijfolie/) -
-   [Appel](http://www.voedzaamensnel.nl/tag/appel/) - 1
-   [Blauwe
    druiven](http://www.voedzaamensnel.nl/tag/blauwe-druiven/) - 150 gram
-   [Walnoten](http://www.voedzaamensnel.nl/tag/walnoten/) - 50 gram
-   [Verse
    peterselie](http://www.voedzaamensnel.nl/tag/verse-peterselie/) - 3 gram

 

Voor de dressing:

-   [Yoghurt](http://www.voedzaamensnel.nl/tag/yoghurt/) - 4 eetlepels
-   [Kerriepoeder](http://www.voedzaamensnel.nl/tag/kerriepoeder/) - 1 theelepel
-   [Knoflookpoeder](http://www.voedzaamensnel.nl/tag/knoflookpoeder/) - 1/2 theelepel
-   [Gemberpoeder](http://www.voedzaamensnel.nl/tag/gemberpoeder/) - 1 theelepel

 

#### Bereidingswijze:

1.  Snijd de kipfilet in blokjes en de ui in ringen
2.  Bak de kipfilet met de ui, wat peper en zout en een beetje olijfolie
    lichtbruin in een pan
3.  Schil ondertussen de appel, verwijder het klokhuis en snijd de appel
    in blokjes
4.  Was de druiven en hak de walnoten in grove stukken
5.  Was en snijd de sla en verdeel de sla over 2 borden
6.  Meng de kipfilet en uit met de appel, druiven en walnoten en schep
    dit bovenop de sla
7.  Meng de ingrediënten voor de dressing en druppel de dressing over de
    salade
8.  Snij de peterselie fijn en strooi over de salade

 
