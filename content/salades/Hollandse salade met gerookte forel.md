
## Ingrediënten

Salade 

- Krieltjes 500 g 
- Sperziebonen 300 g 
- Radijs 10 stuks 
- Verse dille 4 takjes 
- Gerookte forel 140 g 

Dressing

- Extra vierge olijfolie 4 el
- Mosterd 2 el
- Witte balsamicoazijn 2 tl
- Peper en zout

## Bereiding

1. Kook 400 ml water per persoon met een snufje zout in een pan met deksel voor de krieltjes. Was de krieltjes grondig en snijd in kwarten. Kook de krieltjes, afgedekt, in 15 minuten gaar. Giet daarna af en laat zonder deksel uitstomen.
2. Verwijder ondertussen de steelaanzet van de sperziebonen en snijd de sperziebonen in 3 gelijke stukken. Kook de sperziebonen na 5 minuten mee met de krieltjes.
3. Snijd of schaaf de radijs in dunne plakken. Snijd de helft van het radijsblad klein. Hak de dille heel fijn.
4. Maak een dressing van de extra vierge olijfolie, mosterd, witte balsamicoazijn, het grootste deel van de dille, peper en zout.
5. Verdeel de gerookte forel met een vork in kleine stukken en meng ¾ van de forel, met de krieltjes, sperziebonen, radijs en radijsblad in een saladekom. Voeg de dressing toe en breng verder op smaak met peper en zout.
6. Verdeel de aardappelsalade over de borden en garneer met de overige forel en dille.

