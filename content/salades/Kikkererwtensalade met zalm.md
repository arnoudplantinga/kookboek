
(4 personen)

- 300 g winterpeen
- 3 el zonnebloemolie
- 1 rode ui
- 400 g sperziebonen
- 1 citroen (schoongeboend)
- 3 el tahin (sesampasta)
- 2 el milde yoghurt
- 700 g kikkererwten
- 300 g zalmfilet
- 40 g rucola


1. Schil de winterpeen en snijd in plakjes van ½ cm. Verhit 2 el olie in een hapjespan en bak de peen 5 min. op middelhoog vuur. Voeg peper en eventueel zout toe. Schep regelmatig om.
2. Snijd ondertussen de ui in flinterdunne halve ringen. Doe in een kommetje, schenk er heet water over en laat staan tot gebruik.
3. Maak de sperziebonen schoon en halveer ze. Voeg de sperziebonen toe aan de peen en bak 3 min. mee.
4. Rasp de gele schil van de citroen en pers de vrucht uit. Meng het citroenrasp en 4 el -sap met de tahin en yoghurt tot een dressing. Breng op smaak met peper en eventueel zout.
5. Spoel de kikkererwten af onder koud water en laat ze goed uitlekken in een vergiet. Meng vervolgens in een grote schaal met de dressing. Schep de wortel en sperziebonen erdoor.
6. Bestrijk de zalmfilet met de rest van de olie en bestrooi met peper en eventueel zout. Verhit de grillpan zonder olie of boter en gril de zalm 4 min. Keer halverwege. Laat de ui uitlekken en schep met de rucola door de kikkererwtensalade. Verdeel de zalm in stukken over de salade en serveer.
