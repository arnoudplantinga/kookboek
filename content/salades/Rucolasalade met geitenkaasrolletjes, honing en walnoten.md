Met walnoten smaakt het herecht het lekkerst, maar je kunt natuurlijk
ook pijnboompitten gebruiken. Gebruik bij voorkeur ingekookte balsamico
azijn; die is zachter en zoeter van smaak. Ook is magere, rauwe spek het
lekkerst.

 

**Ingrediënten**

-   2 el[ ingekookte balsamico
    azijn](http://www.cookingblondes.nl/recipes/ingekookte-balsamico-azijn "Balsamico stroop")
-   plakken spek
-   geitenkaas
-   rucola
-   romatomaatjes
-   walnoten
-   olijfolie
-   honing

 

**Bereiding**

1.  Snijd de tomaatjes in helften en verdeel ze samen met de rucola over
    de borden.
2.   Rol vervolgens een flinke mespunt kaas in iedere plak spek.
3.  Bak de rolletjes in ongeveer twee minuten op hoog vuur rondom bruin.
    De kaas mag warm en zacht worden, maar het is niet de bedoeling dat
    het helemaal smelt. Staat het vuur te laag, duurt het bakken te lang
    en smelt de kaas.
4.  Leg de rolletjes op de bedjes salade en strooi de walnoten
    er overheen. Besprenkel het geheel vervolgens naar smaak met
    olijfolie, balsamico azijn en honing (ongeveer één eetlepel honing
    per bord).

 

 
