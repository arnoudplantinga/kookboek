## Ingrediënten 4 personen

- 4 kipdijfilets
- 2 teentjes knoflook
- 10 el volle Griekse yoghurt
- 2 tl gemberpoeder
- 1 tl garam masala
- 2 tl komijnpoeder
- 2 tl chilipoeder
- 1 tl kurkumapoeder
- 300 gr kerstomaten
- 4 el olijfolie
- 2 tl kerriepoeder
- 4 el ongezouten pinda's
- 4 stengels bosui
- 6 takjes munt
- 2 el mangochutney
- 200 gr slamix 

## Bereiding

1. Snijd de kippendijen in hapklare reepjes. Pel en pers de knoflook uit. Doe 3/5 van de Griekse yoghurt in een kom en voeg de gember, knoflook, garam masala, komijn, chilipoeder en kurkuma toe. Roer door elkaar en schep de reepjes kip door de tandoori marinade. Zet koel om te marineren.

2. Zet de oven op de grillstand. Leg de kerstomaten in een ovenbestendige schaal en druppel de olijfolie eroverheen. Voeg het kerriepoeder toe en plaats 15 min. onder de grill.

3. Hak ondertussen de pinda’s fijn, snijd de bosui in sliertjes en ris de blaadjes van de takjes munt. Roer de mangochutney door de overige Griekse yoghurt en zet apart.

4. Verhit een koekenpan op middelhoog vuur zonder olie of boter en bak de kipreepjes in 5 min. gaar en krokant.

5. Verdeel de slamix over kommen en voeg de tandoori kip, gepofte tomaatjes, pinda’s en de mangochutney-yoghurt toe. Maak af met de munt en de bosui. 
