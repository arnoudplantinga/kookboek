**Ingrediënten**

1 bol mozzarella (kaas, 125 g)

4 rijpe vleestomaten

4 eetlepels olijfolie extra vierge met
basilicum (fles a 250 ml)

1/2 zakje verse basilicum (a 15 g)

1 ciabatta (brood)

 

**Bereiden**

Mozzarella minimaal 10 min. in vriezer
leggen. Tomaten schoonmaken, in flinterdunne plakken snijden en
dakpansgewijs over vier borden verdelen. Tomaat bestrooien met zout en
peper en besprenkelen met helft van olie. Basilicum grof snijden.
Mozzarella grof raspen en in bergjes midden op tomaat leggen. Basilicum
erover strooien en besprenkelen met rest van olie. Serveren met plakjes
ciabatta.

 

 
