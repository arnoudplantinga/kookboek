## Ingrediënten
- 1/2 galiameloen
- 200 g gemarineerde kipfilet
- 1 el traditionele olijfolie
- 2 el amandelschaafsel (zakje)
- 1 rode paprika
- 75 g veldsla
- 1 el citroensap
- 2 el yoghurtmayonaise
- ¾ el milde kerriepoeder

## Bereiden

1. Halveer de meloen en verwijder de pitjes. Steek met een meloenbolletjeslepel zoveel mogelijk meloenbolletjes uit de helften (zorg ervoor dat de schilhelften heel blijven). Laat de meloenhelften omgekeerd uitlekken.
1. Snijd de kip in blokjes. Verhit de olie in een pan en bak de kipblokjes in ca. 5 min. gaar, schep regelmatig om. Rooster ondertussen in een droge koekenpan het amandelschaafsel goudbruin.
1. Snijd de paprika in kleine blokjes. Schep de veldsla en de citroensap in een kom door elkaar. Roer in een kommetje de mayonaise en de kerriepoeder door elkaar. Breng op smaak met peper, zout en enkele druppels citroensap. Schep de meloenbolletjes, kip en paprika door de veldsla.
1. Schep de salade in de meloenhelften (rest van de salade in de schaal op tafel zetten). Schep de kerriesaus erover en bestrooi met het amandelschaafsel.
