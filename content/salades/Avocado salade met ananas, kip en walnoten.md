#### Ingrediënten:

-   [Ananas](http://www.voedzaamensnel.nl/tag/ananas/) - 1/4
-   [Kipfilet](http://www.voedzaamensnel.nl/tag/kipfilet/) - 250 gram
-   [Avocado](http://www.voedzaamensnel.nl/tag/avocado/) - 1
-   [Rucola](http://www.voedzaamensnel.nl/tag/rucola/) - 50 gram
-   [Walnoten](http://www.voedzaamensnel.nl/tag/walnoten/) - 50 gram
-   [Preischeutjes](http://www.voedzaamensnel.nl/tag/preischeutjes/) - 2 dotjes
-   Voor de dressing:
-   [Olijfolie](http://www.voedzaamensnel.nl/tag/olijfolie/) - 3 eetlepels
-   [Knoflookpoeder](http://www.voedzaamensnel.nl/tag/knoflookpoeder/) - 1/2 theelepel
-   [Gemberpoeder](http://www.voedzaamensnel.nl/tag/gemberpoeder/) - 1/2 theelepel
-   [Peper](http://www.voedzaamensnel.nl/tag/peper/) - 1/4 theelepel
-   [Zout](http://www.voedzaamensnel.nl/tag/zout/) - snufje

 

#### Bereidingswijze:

1.  Vul de waterkoker, zet aan
2.  Leg de rauwe kipfilet in een steelpan, giet het kokende water er
    overheen en laat 15 minuten koken
3.  Snij de kroon van een ananas, snij door tweeën en 1 stuk weer
    in tweeën. Bewaar 3/4 in folie in de koelkast
4.  Snij de kern en de schil van de ananas en snij het vruchtvlees in
    blokjes
5.  Snij de avocado doormidden tot de pit, lepel de pit eruit en lepel
    vervolgens het vruchtvlees eruit
6.  Snij in blokjes en leg samen met ananasblokjes in een kom
7.  Was de rucola en verdeel over 2 borden
8.  Hak de walnoten grof
9.  Meng de ingrediënten voor de dressing in een schaaltje
10. Haal de kip met een vork uit het water en controleer of hij gaar is
11. Snij de kip in blokjes en laat even afkoelen
12. Voeg de kip toe aan de ananas en avocado, hussel door elkaar en
    verdeel over de rucola
13. Strooi de walnoten er overheen en druppel daarna de dressing er
    overheen
14. Leg nu op beide salades een dotje preischeutjes

Tip: gebruik 1 teen verse knoflook en 2 gram verse gember i.p.v. de
knoflookpoeder en gemberpoeder in de dressing voor een pittigere smaak.

 
