**The Man Salad**

[![IMG\_0919](http://content.artofmanliness.com/uploads//2013/01/IMG_0919.jpg){width="500"
height="375"}](http://content.artofmanliness.com/uploads//2013/01/IMG_0919.jpg)

-   **Spinach/Spring Salad Mix.** This was the base of my salad. I used
    Organic Girl Greens from Whole Foods. Yeah, I know. The base of my
    Man Salad came from a company called Organic Girl. Spinach and other
    leafy green vegetables contain minerals like magnesium and zinc,
    which have been shown to aid in testosterone production ([study
    on magnesium](http://www.ncbi.nlm.nih.gov/pubmed/20352370), [and
    another](http://www.ergo-log.com/magnesiumtest.html); [study on
    zinc](http://www.ncbi.nlm.nih.gov/pubmed/21744023))
-   **Meat.** Meat, particularly beef, provides our bodies with the
    protein it needs to create muscle (more muscle = more T) and the
    fats and cholesterol to make testosterone. My meat topping of choice
    was sliced up chuck steak. I grilled two of them on Monday and it
    lasted me until the next Monday. Every now and then I’d slow-cook
    some ribs or brisket to use as my meat topping. My philosophy was
    the fattier, the better.
-   **Nuts**. Usually a handful of Brazil nuts or walnuts. Nuts are
    little fat bombs that provide the cholesterol that Leydig cells need
    for T production. [One study suggest that the selenium in Brazil
    nuts boosts
    testosterone](http://www.ncbi.nlm.nih.gov/pubmed/19091331). Just
    don’t go crazy with them. Too much selenium is no bueno.
-   **Avocado/Olives. **Avocados and olives are a great source of the
    good fats we need for healthy testosterone production.
-   **Broccoli. **Every now and then I’d throw some broccoli into the
    salad. Broccoli contains high levels of [indoles, a food compound
    that has been shown to reduce the bad estrogen in our bodies that
    sap testosterone
    levels](http://jn.nutrition.org/content/133/7/2470S.full).
-   **Olive Oil.** I topped my Man Salad off with lots of olive
    oil. [Research suggests that olive oil helps your Leydig cells
    (which produce testosterone) absorb cholesterol
    better. ](http://www.ergo-log.com/olivetest.html)And as I’ve
    mentioned a few times, our Leydig cells need cholesterol to make T.
    More cholesterol absorption = more testosterone.
-   **Balsamic Vinegar.** Mostly for taste. It’s also supposed to help
    keep your insulin in check.

 
