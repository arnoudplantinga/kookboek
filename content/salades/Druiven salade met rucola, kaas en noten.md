
## Ingrediënten

- 4 el extra vierge olijfolie
- 1 el witte wijnazijn
- 1 tl Franse mosterd
- 150 g oude kaas 48+
- 250 g pitloze witte druiven
- 2 el ongebrande walnoten
- 75 g rucola (zak)

## Bereiden


1. Klop de olijfolie, wittewijnazijn, mosterd, peper en eventueel zout tot een dressing. 
2. Snijd de kaas in blokjes van een halve cm. Halveer de druiven. Hak de walnoten grof. 
3. Meng de kaas met de druiven, walnoten en rucola. Schep de dressing erdoor en serveer.

Variatietip:
Lekker met meergranenrijst en rosbieflapjes à la minute (schaal 280 g).

