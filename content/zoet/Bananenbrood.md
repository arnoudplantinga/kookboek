## Ingrediënten 10 personen

- 50 g ongezouten roomboter
- 3 bananen
- 2 middelgrote eieren
- 170 g fijne havermout
- 1 tl gemalen kaneel
- 100 g kristalsuiker

## Bereiding

1. Verwarm de oven voor op 180 °C. Smelt de boter in een hittebestendig kommetje in de magnetron. Prak de bananen fijn. Voeg een voor een de eieren toe en roer met een garde erdoor. Voeg de gesmolten boter, 150 g fijne havermout (per bananenbrood), kaneel, suiker en een snufje zout toe en roer goed door tot een homogeen beslag.

2. Verdeel het beslag over het cakeblik en verdeel de rest van de havermout over het beslag. Bak het bananenbrood in 45 min. in het midden van de voorverwarmde oven gaar. Laat het brood minimaal 1 uur afkoelen.
