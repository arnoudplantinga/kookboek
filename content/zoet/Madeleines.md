**Ingrediënten**

200 g boter

60 ml volle melk

1 el honing

3 eieren

130 g fijne kristalsuiker

200 g bloem

8 g bakpoeder (zakje 16 g, pakje 5 zakjes)

1 citroen (schoongeboend)

 

**Keukenspullen**

madeleinevorm met 12 holtes

 

**Bereiden **

1.  Smelt de boter in een steelpan. Roer er de melk en honing door.
    Breek de eieren boven een kom en voeg de suiker toe. Klop met een
    mixer 5 min. Meng de bloem en het bakpoeder in een kom. Rasp de gele
    schil van de citroen en voeg toe. Doe het botermengsel bij het
    eierschuim en spatel de bloem er in delen door. Zet dit afgedekt
    minimaal 1 uur in de koelkast.
2.  Verwarm de oven voor op 190 °C. Vet de bakvorm met de boter in en
    verdeel de helft van het beslag met 2 eetlepels over de 12 holtes.
    Bak de madeleines ca. 6 min. in het midden van de oven tot ze
    goudbruin en bol zijn. Laat ze op een bord glijden en bak er zo
    nog 12. Serveer direct.

 

Bron: 

 
