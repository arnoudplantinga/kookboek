
Voor 6 tot 8 personen

- 500 gr. mascarpone
- 1,25 dl sterke koffie
- 4 eierdooiers
- 2 eiwitten
- 100 gr poedersuiker
- 1 likeurglaasje amaretto
- 300 gr boudoirkoekjes (lange vingers)
- 3 eetlepels cacaopoeder



1. Bereid een mokkatasje sterke koffie.
2. Klopt eierdooiers, poedersuiker en likeur tot een schuimig deeg met de mixer.
2. Voeg de mascarpone toe.
2. Klopt het eiwit tot schuim en meng voorzichtig onder het beslag.
2. Besprenkel de boudoirkoekjes met sterke koffie.
2. Schik de koekjes op een schotel in 3 lagen. Breng tussen iedere laag koekjes een laagje creme aan.
2. Laat de tiramisu minstens 1 uur in de koelkast rusten.
Bestrooi met cacaopoeder juist voor het opdienen.
