
Time: 40 minutes

This large, fluffy pancake is excellent for breakfast, brunch, lunch and dessert any time of year. And it comes together in about five blessed minutes. Just dump all of the ingredients into a blender, give it a good whirl, pour it into a heated skillet sizzling with butter, and pop it into the oven. Twenty-five minutes later? Bliss.

## Ingredients
- 3 eggs
- ½ cup flour
- ½ cup milk
- 1 tablespoon sugar
 - Pinch of nutmeg
- 4 tablespoons unsalted butter
 - Syrup, preserves, confectioners' sugar or cinnamon sugar

## Preparation

1. Preheat oven to 425 degrees.
1. Combine eggs, flour, milk, sugar and nutmeg in a blender jar and blend until smooth. Batter may also be mixed by hand.
1. Place butter in a heavy 10-inch skillet or baking dish and place in the oven. As soon as the butter has melted (watch it so it does not burn) add the batter to the pan, return pan to the oven and bake for 20 minutes, until the pancake is puffed and golden. Lower oven temperature to 300 degrees and bake five minutes longer.
1. Remove pancake from oven, cut into wedges and serve at once topped with syrup, preserves, confectioners' sugar or cinnamon sugar.
