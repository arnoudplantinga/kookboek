
## Ingrediënten

- 50 gram gedroogde dadels, 
- 50 gram vijgen, 
- 50 gram abrikozen, 
- 100 gram rauwe gemengde noten naar keuze (geen pinda’s), 
- rasp van een biologische (gewassen) citroen, 
- 1 theelepel kaneel, 
- 50 ml sinaasappelsap, 
- geschaafde kokos of geraspte pure chocolade 

## Bereiding

Meng het sinaasappelsap met de gedroogde vruchten en kaneel. Doe alle ingrediënten, behalve de laatste, in een keukenmachine. Als alles fijngehakt is, draai er dan balletjes van en rol ze door de kokos of chocoladerasp. 
