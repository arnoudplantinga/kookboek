
Ideaal voor kinderen: zonder bakken, zonder oven een lekkere petit beurre of creme au beurre taart maken met koekjes, boter, poedersuiker, koffie en hagelslag.

Dit recept is zowat de Vlaamse tiramisu waar de mascarpone vervangen wordt door echte boter. De onderste laag koekjes echter niet in de koffie dompelen als je een stevige basis aan de taart wil geven.

Voor de zoetebekken gebruik je evenveel poedersuiker als boter. Om zeer makkelijk te werken, maak je gewoon kleine taartjes ter grootte van het koekje.

Er wordt geen alcohol gebruikt, wat dit recept ideaal maakt om met kinderen te maken en op te eten.

## Ingrediënten

- ¼ kilogram echte boter
- een pak petit beurre koekjes
- 4 eetlepels poedersuiker
- zeer sterke, koude koffie of espresso
- chocolade hagelslag

## Bereiding

1. Maak creme au beurre door de boter romig te roeren en er dan beetje bij beetje de poedersuiker onder te roeren.
2. Leg een laag petit beurre koekjes naast elkaar en smeer er een laagje creme au beurre over.
3. Dompel dan de petit beurre koekjes snel in koude koffie en leg bovenop de creme au beurre.
4. Herhaal met een laagje creme au beurre en koekjes en eindig met een laag creme au beurre.
5. Strooi er chocolade hagelslag over en laat de petit beurre taart opstijven in de ijskast.
