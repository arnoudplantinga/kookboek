Apple crumble recept op twee verschillende manieren

Filmpje IKEA:  

 

**Ingrediënten:**

- 70 gram havermout
- 70 gram bloem
- 60 gram basterdsuiker
- 110 gram boter
- 5-6 appels ( ongeveer 700 gram)

**Voor de crumble:**

1. De oven voorverwarmen op 190°c.
2. In een grote kom, meng je de bloem, havermout en suiker goed
doorelkaar.
3. Snijd de boter in kleine blokjes en voeg deze toe in de grote kom.
4. Mix het geheel met je vingertoppen of een vork tot een kruimelig
textuur.
5. Zet je kruimeldeeg even opzij, om je appels voor te bereiden.
6. Verwijder de klokhuizen en snijd de apples in dunne plakken.
7. Vet 2 ovenvaste schalen in ( diameter 14 cm) en plaats de plakjes
appel in de schalen.
8. Verdeel de crumble gelijkmatig over de appels.

**Gevulde appels:**

1. Snijd de bovenkant van twee appels af en verwijder de klokhuizen,
snijd ook tegelijk iets meer appel weg zodat je genoeg ruimte hebt om de
appel te vullen met crumble.
2. De extra wegsneden appel kan je mixen met de crumble.
3. Vul de appels met de crumble en plaats de afgesneden bovenkant terug
op de appel.

Plaats beide desserts voor ongeveer 20 minuten in de oven of totdat de
crumble goudbruin is.
