**Dit heb je nodig**
--------------------

- 240 gram fijne patentbloem
- 180 gram roomboter
- 120 gram fijne suiker
- snufje zeezout

**Zo maak je de koekjes**
-------------------------

1. Verwarm de oven voor op 170 C.
2. Doe alle ingrediënten in een wijde kom en snijdt de boter met twee messen in stukjes door de bloem. Kneed alles snel tot een mooie deegbal, wikkel in folie en leg een half uurtje in de koelkast.
3. Rol het deeg uit tot een lap van een paar milimeter dik en steek met een rond vormpje de koekjes uit. In de helft van de koekjes maak je (met appelboor b.v.) een klein gat ik het midden.
4. Bak de koekjes in ca 20-25 minuten gaar en lichtbruin. Laat ze helemaal afkoelen op een rooster.
5. Doe een flinke theelepel jam op een koekje en druk er een koekje met gat bovenop.

 
