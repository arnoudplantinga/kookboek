Ingrediënten
------------

-   4 [appels](http://www.mijnreceptenboek.nl/ingredienten/hardfruit/appels.html "Ingrediëntinformatie over appels")
-   4
    eetlepels [rozijnen](http://www.mijnreceptenboek.nl/ingredienten/fruit/rozijnen.html "Ingrediëntinformatie over rozijnen")
-   2
    eetlepels [boter](http://www.mijnreceptenboek.nl/ingredienten/zuivel/boter.html "Ingrediëntinformatie over boter")
-   2
    eetlepels [kaneel](http://www.mijnreceptenboek.nl/ingredienten/specerijen/kaneel.html "Ingrediëntinformatie over kaneel")
-   magnetronfolie

 

1. Verwijder de klokhuizen met een appelboor.
2. Zet de appels in een magnetronschaal met een bodempje water. Vul elk gat met een ½ eetlepel boter en 1 eetlepel rozijnen. Strooi 2 eetlepels kaneel over de appels en dek de schaal af met de folie.
3. Verwarm de appels ca. 5 minuten op vol vermogen in de magnetron tot ze zacht zijn. Eet smakelijk!

 

Bron: [http://www.mijnreceptenboek.nl/recept/nagerechten/appels-uit-de-magnetron-3793.html](http://www.mijnreceptenboek.nl/recept/nagerechten/appels-uit-de-magnetron-3793.html)
 
