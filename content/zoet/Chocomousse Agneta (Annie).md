
## Ingrediënten

- 200 gr. chocolade
- 5 eieren
- 30 gr. boter of planta (hard)
- 150 gr. bloemsuiker
- 2 dl room

## Bereiding

1. De chocolade, de helft van de suiker en de boter smelten au bain marie. (Warmwaterbad)
2. een beetje room toevoegen.
3. Eieren breken, eiwit opkloppen met de rest van de suiker,
4. dooiers bij de lauwe chocolade doen, opgeklopt eiwit lichtjes bij de chocolademengeling doen.
5. Room lichtjes opkloppen en er ook bij doen.
