Voor 8 pannenkoeken


![Foto: Amerikaanse
Pannenkoeken](http://www.tastyweb.nl/tastyweb/Images/pancakes.jpg)

-   7 eieren
-   250 gr (zelfrijzende) Bloem
-   120 gr (fijne) suiker
-   250 ml melk
-   snufje zout
-   indien gewenst: 1 zakje vanillesuiker


1. Scheidt als eerste de eieren en doet ze in twee kommetjes. Voeg de
bloem, suiker, bakpoeder en de melk bij de eigelen mengt deze
ingrediënten tot een zachte dikke massa. Klop de eiwitten met het zout
stijf op en voeg ze bij het overige. Het deeg is klaar voor bereiding. 
2. Verhit een tefalpan op middelhoog vuur. Doe wat arachide of
zonnebloemolie op een stukje huishoudpapier en verspreid het over de
pan. 
3. Schud wat pannenkoekdeeg in de verhitte pan en laat het enkele
minuten totdat het goudgeel en stevig zijn aan beide kanten. 

## Gevulde pannenkoeken 
1. Indien je gevulde pancakes wilt maken kan ze vullen vlak voordat je
ze omdraait. 

2. De vulling kan bestaan uit: ontbijtspek, pancetta, bosbessen,
bananen, gestoofde appelen, chocolade, siroop of eender wat je denkt wat
goed gaat met pannenkoeken.

 
