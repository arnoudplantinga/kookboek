---
tags: []
---


- Bladerdeeg
- Bokaal ontpitte krieken (sap houden)
- 1/4 liter room
- 5 soeplepels suiker
- 1 soeplepel vanillebloem
- 3 eieren


1. Bladerdeeg in vorm leggen en prikken.
2. Krieken opschikken (sap houden).
3. Beleg maken: room + suiker + eieren en vanillebloem samen kloppen en over de krieken gieten.
4. Bakken in voorverwarmde over op 210 graden. Ongeveer 30 minuten.
5. Saus: sap van krieken + 3 soeplepels suiker laten koken,
6. 2 eetlepels vanillebloem oplossen in wat kriekensap en bij het kokende sap roeren en laten doorkoken en over de taart gieten.
