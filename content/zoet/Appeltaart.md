Ingrediënten
------------

-   11 friszoete en stevige appels (bijv. jonagold)
-   125 g boter, op kamertemperatuur + extra om in te vetten
-   125 g plantaardige margarine, op kamertemperatuur
-   200 g suiker
-   1 zakje vanillesuiker
-   2 eieren
-   400 g zelfrijzend bakmeel, gezeefd
-   2 el kaneel

 

Materialen
----------

-   deegroller
-   springvorm doorsnede 28 cm, ingevet

 

Bereiden
--------

1. Verwarm de oven voor op 180 °C.
2. Schil de appels, verwijder de klokhuizen, snijd de appels in 4 parten en deze vervolgens in plakjes.
3. Klop in een beslagkom de 125 g boter, de margarine, suiker, vanillesuiker en een mespunt zout met een mixer tot een luchtige massa. Voeg 1 ei toe en mix tot een egaal beslag. Voeg het bakmeel toe en mix tot het deeg een bal vormt. Vervang de gardes van de mixer eventueel halverwege door deeghaken.
4. Bekleed de springvorm met de helft van het deeg en houd de rest achter. Doe de helft van de appels in de springvorm en strooi er 1 el kaneel over. Leg de andere helft van de appels erop en strooi de rest van de kaneel erover. Rol het achtergehouden deeg met een deegroller uit tot een lap van een 1/2 cm dik en snijd er 8 stroken uit van 1 1/2 cm breed en 1 lange strook voor langs de rand van de springvorm. Leg de deegstroken in een geruit patroon over de appelvulling. Leg de lange strook langs de rand van de springvorm.
5. Klop het andere ei los en bestrijk met het kwastje de stroken en de rand.
6. Laat de taart 1 1/2 uur in het midden van de oven goudbruin en gaar worden. Controleer regelmatig of de taart niet te snel bruin wordt; zet in dat geval de oven op 150 °C.

 
