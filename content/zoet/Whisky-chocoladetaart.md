---
tags: []
---


- 3 stickjes espressopoeder
- 200 ml kraanwater (heet)
- 250 ml volle melk
- 300 g bloem
- 400 g witte basterdsuiker
- 100 g cacaopoeder
- 3 tl bakpoeder
- 8 g vanillesuiker (zakje à 8 g)
- 1 tl zout
- 5 eieren
- 125 ml zonnebloemolie
- 400 g chocolade extra puur 72%
- 1 el zeezout
- 750 ml slagroom
- 2 el donkerbruine basterdsuiker
- 70 ml whisky (Gall&Gall)

## Keukenspullen

* bakpapier
* 2 vierkante springvormen (24 x24 cm)
* mixer
* satéprikker


1.  Vet de springvormen in met wat boter en bekleed de bodem met bakpapier. Verwarm de oven voor op 180 °C. Los 2 sticks espressopoeder op in 200 ml heet water.
2.  Verwarm de melk tot lauwwarm. Meng in een grote kom de bloem, witte basterdsuiker, cacaopoeder, bakpoeder, vanillesuiker en het zout. Voeg de eieren, lauwwarme melk en olie toe en klop 5 min. met een mixer tot een stevig beslag. Voeg al kloppend de koffie toe. Verdeel het beslag meteen over de springvormen.
2. Bak de taartlagen in het midden van de oven in ca. 35 min. gaar. Controleer met de satéprikker of ze gaar zijn. Dit is het geval als je de prikker in de cake steekt en hij er schoon en droog uit komt.
2. Laat de taart­lagen volledig afkoelen op een rooster. Breng ondertussen een pan met water aan de kook.
3.  Hak de chocolade grof. Hang in de pan met kokend water een iets kleinere kom of pan, die het water niet mag raken. Laat daarin de chocolade smelten (au bain-marie). Schenk de helft van de chocolade op een stuk bakpapier en spreid uit met een paletmes of spatel. Bestrooi met het zeezout en laat in 1 uur en 30 min. hard worden.
4.  Klop de slagroom stijf met de laatste stick espressopoeder en donkerbruine basterdsuiker. Klop de whisky erdoor.
1. Halveer de taartlagen overlangs. Leg 1 taartlaag op een bord en bestrijk dun met de rest van de gesmolten chocolade en 1/3 van de whiskyroom. Leg er een taartlaag op. Herhaal totdat alle taartlagen op zijn. Bestrijk de bovenkant van de laatste laag en de taart rondom met de rest van de whiskyroom. Breek de hard geworden chocolade in stukken en plak deze tegen de zijkanten van de taart.
