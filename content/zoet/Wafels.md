
## Ingrediënten wafels (voor ongeveer 10 stuks)

- 380 gram bloem
- 1 el bakpoeder
- snuf zout
- 2 el suiker
- 2 eieren
- 380 ml melk
- 6 el olie
- 1 tl vanille extract


## BEREIDING

Doe de droge ingrediënten -bloem, bakpoeder, zout en suiker- in een kom en roer met een garde door elkaar.

Voeg dan de eieren, melk, olie en vanille extract toe. Klop met de garde tot een egaal beslag.

Verhit je wafelijzer en vet deze in. Ik gebruik hiervoor een bakspray.

Afhankelijk van de grootte van je ijzer, vul je de platen met 1,5-2 ijslepels beslag. Sluit het wafelijzer en bak tot ze goudbruin beginnen te kleuren. Hoe lang dat duurt verschilt per ijzer. Bij mij hadden ze ongeveer 5-7 minuten nodig.

Eenmaal uit het ijzer leg je ze op een rooster om af te koelen. Of je eet ze gelijk warm op 😉

Goed om te weten: deze wafels zijn heel neutraal van smaak. Ze zijn dus het lekkerst met een beetje beleg. Poedersuiker doet het altijd goed, maar ook met fruit zijn ze heerlijk. Als je liever wilt dat ze van zichzelf al zoet zijn, kun je naar smaak meer suiker aan het beslag toevoegen.

Omdat ze zo neutraal van smaak zijn, kun je ze ook heel goed hartig beleggen. Lekker met een beetje kaas, of pak uit met bacon en eieren. Ja, dat is de volgende wafel die ik ga proberen.

