Frisse citroen kwarktaart. Snel en makkelijk te maken.

 

**INGREDIENTEN**

100 gram boter

200 gram bastognekoeken =&gt; of zakje koekkruimels van dr. Oetker

7 blaadjes witte gelatine (5 of 6 kan ook)

2 dl slagroom

2 el suiker (iets minder kan ook)

500 gram citroenkwark (bv. Optimel)

3 el citroensap

evt. aardbeien ter garnering

 

**HULPMIDDELEN**

Springvorm

 

**Bereidingswijze**

-   Boter smelten.
    Koekjes verkruimelen. Boter er door mengen. Over bodem springvorm
    verdelen en aandrukken. Ca. 30 min. in koelkast hard
    laten worden.
-   Gelatine 5 min. in
    water laten weken. 
-   Slagroom met
    suiker stijfkloppen. Kwark en slagroom door elkaar mengen. 
-   In pannetje
    citroensap met 2 el water aan de kook brengen en van vuur halen.
    Gelatine al roerend in hete citroensap oplossen en iets
    laten afkoelen. 
-   Gelatine door
    kwarkmengsel roeren en de massa op de koekjesbodem gieten. 
-   Taart in koelkast
    stijf laten worden.

 

**Serveertips**

Eventueel aardbeien in plakjes er op sorteren. Of enkele bastogne
koekjes verkruimelen en eroverheen strooien.

 

Bron: 

 

 
