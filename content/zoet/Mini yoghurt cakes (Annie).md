Benodigdheden:

1 potje natuuryoghurt

2 potjes suiker

3 potjes bloem

3 hele eieren

1 pakje vanillesuiker

1 zakje bakpoeder

1/2 potje maisolie

(telkens het lege yoghurtpotje gebruiken)

Papieren cakevormpjes

 

Bereiding:

Alles mengen, goed kloppen en de papieren vormpjes tot de helft vullen
met het mengsel.

Bakken gedurende 15 à 20 minuten in een warme oven van 180 à 200
graden.\

