## Ingrediënten

- 15 gram roomboter
- 15 gram bloem
- 0,5 liter bouillon
- Snuf peper
- Aanbaksels van gebraden vlees of gevogelte
- Optioneel: verse kruiden
- Optioneel: witte of rode wijn

## Bereiding

Zet je pan of braadslee op laag vuur zodat de aanbaksels zacht beginnen te worden. Blus af met bouillon of een combinatie van bouillon met een scheutje rode of witte wijn. Hierdoor komen de baksels los en krijgt je uiteindelijke jus extra veel smaak.

Smelt de boter in een koekenpan op laag vuur en voeg de bloem in één keer toe. Roer met een garde totdat de bloem is opgenomen door de boter en je geen klontjes meer ziet. Je hebt nu een ‘roux’.

Kook de roux nog even door zodat ook de bloem gaar wordt (doe je dit niet? Dan krijgt je jus een lijmachtige structuur) en blus af met een scheutje bouillon. Blijf roeren met een garde en het mengsel begint vanzelf dikker te worden.

Voeg wanneer er geen klontjes meer zijn nog een scheut bouillon toe en ga zo door tot je de gewenste smaak en dikte hebt. Breng eventueel nog op smaak met versgemalen peper (zout is waarschijnlijk niet nodig door de bouillon) en verse kruiden.
