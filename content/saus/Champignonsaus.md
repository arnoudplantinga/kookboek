
## Ingrediënten (voor 4 personen)

- 250 ml slagroom
- 200 ml runderbouillon (of de foto staat 500 ml bouillon, maar ik heb maar 200 ml gebruikt)
- 1 sjalotje, fijngesnipperd
- 1 teen knoflook, uitgeperst
- 400 g champignons
- 1 flinke el bloem
- Bosje platte peterselie
- Olie om in te bakken
- Zout en peper


## Bereiding

1. Verhit de koekenpan of sauspan met wat olie en fruit hierin het sjalotje en de knoflook tot ze zacht maar niet gekleurd zijn.
2. Snijd ondertussen de champignons in plakken en voeg toe aan het sjalotje. Breng op smaak met peper en zout.
3. Bak op hoog vuur verder, totdat de champignons zacht zijn. Voeg nog wat extra olie toe en doe de bloem erbij. Zet het vuur wat zachter en bak nog enkele minuten verder om de bloem te laten garen. Blus dan af met de bouillon en slagroom.
4. Laat inkoken tot de gewenste sausdikte.
5. Was de peterselie, dep droog met een theedoek en hak fijn. Voeg op het laatste moment 1-2 eetlepels peterselie toe aan de saus en breng eventueel nog verder op smaak met zout en peper.

