
![Plaatje](../../images/Almost%20No-Knead%20Bread%20With%20Olives,%20Rosemary,%20and%20Parmesan.jpg)

## Ingrediënten

* 500g bloem (volkoren of gewoon)
* gist
* zout
* 100g fijn geraspte parmezaanse kaas
* 1 eetleepel rozemarijn
* 210 ml lauw water
* 110 g grof gehakte groene olijven
* 90 ml pils
* 1 eetleepel azijn

## Bereiding

Mocht je het brood op een specifiek nodig hebben, begin dan zo’n 15 uur van tevoren met het recept. Ikzelf begon ‘s avonds voordat ik naar bed ging en ik kon aan het begin van de middag van mijn zelfgebakken brood proeven.

1. Meng de bloem, gist, zout, parmezaanse kaas, en rozemarijn in een beslagkom. Voeg daarna water, olijven, bier en azijn toe.
1. Vouw het deeg met een rubberen spatel naar binnen. Schraap droge bloem uit de bodem van de kom tot je een nattige bal krijg.
1.  Dek de kom af met een schone theedoek of folie en laat het ongeveer 12 uur rijzen tot het in volume is verdubbeld.
1. Op warme dagen kan dit wat sneller gaan en op koude dagen duurt het misschien wat langer. Het komt in ieder geval niet heel erg nauw, want over het algemeen zie je een rijstijd van 8-18 uur voor dit soort broden.
1. Na de eerste lange rijs bestrooi je je werkblad rijkelijk met bloem, stort hier het deeg op. Neem twee spatels of deegstekers die je ook even bestrooit met bloem en vouw het deeg een aantal keren dubbel. Doe dan wat bloem op je handen en vorm het brood vlot tot een bol.
1. Leg het deeg daarna gelijk op een vel bakpapier, bestrooi met bloem en dek af met een schone doek. Zo laat je het deeg weer ongeveer 2 uur rijzen.
1. Verwarm je oven voor op 250 °C (boven- en onderwarmte), tijdens het voorverwarmen zet je een gietijzeren pan of andere ovenbestendige pan (met deksel!) in de oven.
1. Als de oven heet genoeg is en het brood lang genoeg heeft gerezen kun je eindelijk gaan bakken!
1. Haal de pan uit de oven en verplaats het brood door middel van het bakpapier in de pan (het bakpapier gaat dus ook in de pan). Plaats de deksel er weer op en zet terug in de oven.
1. Bak het brood eerst 25-30 minuten met deksel. Daarna verwijder je het deksel en bak je het brood 10-15 minuten zonder deksel.
1. Haal het brood uit de pan en leg op een rooster om het af te laten koelen.from pot; transfer to wire rack and cool to room temperature, about 2 hours.
