## Ingrediënten
- 450 gram bloem
- 8 gram zout
- 3 gram gedroogde gist
- 350 ml water (op kamertemperatuur)

## Instructies
1. Mocht je het brood op een specifiek nodig hebben, begin dan zo’n 15 uur van tevoren met het recept. Ikzelf begon ‘s avonds voordat ik naar bed ging en ik kon aan het begin van de middag van mijn zelfgebakken brood proeven.
1. Doe alle ingrediënten in een kom en roer met een spatel door elkaar, zorg dat het bloem helemaal is opgenomen. Dek de kom af met een schone theedoek of folie en laat het ongeveer 12 uur rijzen tot het in volume is verdubbeld.
1. Op warme dagen kan dit wat sneller gaan en op koude dagen duurt het misschien wat langer. Het komt in ieder geval niet heel erg nauw, want over het algemeen zie je een rijstijd van 8-18 uur voor dit soort broden.
1. Na de eerste lange rijs bestrooi je je werkblad rijkelijk met bloem, stort hier het deeg op. Neem twee spatels of deegstekers die je ook even bestrooit met bloem en vouw het deeg een aantal keren dubbel. Doe dan wat bloem op je handen en vorm het brood vlot tot een bol.
1. Leg het deeg daarna gelijk op een vel bakpapier, bestrooi met bloem en dek af met een schone doek. Zo laat je het deeg weer ongeveer 2 uur rijzen.
1. Verwarm je oven voor op 250 °C (boven- en onderwarmte), tijdens het voorverwarmen zet je een gietijzeren pan of andere ovenbestendige pan (met deksel!) in de oven.
1. Als de oven heet genoeg is en het brood lang genoeg heeft gerezen kun je eindelijk gaan bakken!
1. Haal de pan uit de oven en verplaats het brood door middel van het bakpapier in de pan (het bakpapier gaat dus ook in de pan). Plaats de deksel er weer op en zet terug in de oven.
1. Bak het brood eerst 25-30 minuten met deksel. Daarna verwijder je het deksel en bak je het brood 10-15 minuten zonder deksel.
1. Haal het brood uit de pan en leg op een rooster om het af te laten koelen.

## Tips
Tip: Houd vooral bij het bakken zonder deksel je brood in de gaten. Toen ging het bij mij heel hard en je wilt natuurlijk geen droog en zwart brood. Bewaren: In een zak 3-4 dagen houdbaar, houd er rekening mee dat de korst na verpakken niet meer knapperig is. Invriezen kan ook, dan kun je het tot 3 maanden bewaren.
